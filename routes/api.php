<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1', 'middleware' => ['cors']], function() {
	// Rutas de usuario
	//Route::post('login', 'LoginController@login');
	Route::post('login', 'UsuarioController@login');
	Route::post('loginweb', 'UsuarioController@loginweb');
	Route::post('distribuidor', 'UsuarioController@storeDistribuidor');
	Route::post('callcenter', 'UsuarioController@storeAtencionCliente');
	Route::post('supervisor', 'UsuarioController@storeSupervisor');
	Route::post('planta', 'UsuarioController@storePlanta');
	Route::post('rrhh', 'UsuarioController@storeRrhh');
	Route::post('administrador', 'UsuarioController@storeAdministrador');
	Route::post('cliente', 'UsuarioController@storeCliente');
	Route::post('cliente_moral', 'UsuarioController@storeClienteMoral');
    Route::post('operador', 'UsuarioController@storeOperador');
    Route::post('staff', 'UsuarioController@storeStaff');
	Route::post('usuario/recuperar_password', 'UsuarioController@recuperarPassword');
	Route::post('usuario/nuevo_password', 'UsuarioController@nuevoPassword');
	Route::post('estatus/{id}', 'UsuarioController@cambiarEstado');
	Route::get('registrar', 'UsuarioController@registrar');
	Route::get('clientes_qr', 'UsuarioController@clientesQr');
	Route::post('clientes_qr', 'UsuarioController@qrImpreso');
	Route::resource('usuario', 'UsuarioController', 
		['only' => ['index', 'update', 'show']]);
	Route::post('usuario/update', 'UsuarioController@change');
	// Rutas de direccion
	Route::resource('direccion', 'DireccionController', 
		['only' => ['index', 'store', 'update', 'show']	]);
	Route::post('direccion/cliente', 'DireccionController@storeDireccionCliente');
	Route::post('direccion/update', 'DireccionController@updateDireccion');
	Route::get('direccion/territorio', 'DireccionController@territorio');
	Route::get('direccion/cliente/zona/{id}', 'DireccionController@clientesPorZona');
	Route::get('direccion/cliente/{id}', 'DireccionController@direccionUsuario');
	Route::get('direccion_sin_sucursal', 'DireccionController@direccionSinSucursal');
	Route::post('asignar_direccion_sucursal', 'DireccionController@asignarSucursalDireccion');

	// Rutas de articulo
	Route::resource('articulo', 'ArticuloController', 
		['only' => ['index', 'store', 'update', 'show']	]);
	Route::get('articulo/categoria/{id}', 'ArticuloController@getArticulosCategoria');

	// Rutas de pedidos
	Route::resource('pedido', 'PedidoController', 
		['only' => ['index', 'store', 'update', 'show']	]);
	Route::get('pedido/cliente/{id}', 'PedidoController@pedidoCliente');
	Route::post('pedido/historial', 'PedidoController@historial');
	Route::post('pedidos_status', 'PedidoController@pedidosEstatus');
	Route::post('pedidos_by_status', 'PedidoController@pedidoByEstatus');
	Route::get('all_status_pedidos', 'PedidoController@getAllEstatus');
	Route::get('pedido_all', 'PedidoController@pedidoAll');
	Route::get('pedido_details/{id}', 'PedidoController@pedidoDetailsById');
	Route::get('pedido_por_sucursal', 'PedidoController@pedidosPorSucursal');
	Route::get('pedido_por_canal', 'PedidoController@pedidosPorCanal');

	// Rutas de ruta
	Route::resource('ruta', 'RutaController', 
		['only' => ['index', 'store', 'update', 'show']	]);
	Route::post('ruta/relacionar', 'RutaController@asociarClienteRuta');

	// Rutas especiales
	Route::post('dashboard', 'PedidoController@dashboard');
	Route::post('sucursal/create', 'RutaController@storeSucursal');
    Route::get('sucursal', 'RutaController@getSucursal');

	Route::post('compra_proveedor', 'PedidoController@compraInsumos');
	Route::post('recepcion_compra', 'PedidoController@recepcionInsumos');
	Route::get('hitorial_compra_proveedores', 'PedidoController@allBuyProvider');
	Route::post('hitorial_compra_proveedores', 'PedidoController@allBuyProvider');
	Route::post('monitoreo', 'ApiController@monitoreo');
	Route::get('monitoreo', 'ApiController@getMonitoreo');

	// Rutas notificaciones
	Route::post('notificacion', 'NotificacionController@store');
	Route::post('notificacion/create', 'NotificacionController@storeNotificacionMensaje');
	Route::get('notificacion_mensaje', 'NotificacionController@getMensajeAll');
	Route::get('notificacion_mensaje_by_rol', 'NotificacionController@getMensajeRolAll');
	Route::get('novedades', 'NotificacionController@getNovedades');
	Route::get('novedades/{id}', 'NotificacionController@notificacionUsuario'); 
	Route::get('notificacion_by_rol/{id}', 'NotificacionController@notificacionReportadasPor'); 

	// Rutas pagos
	Route::post('payment', 'ApiController@pagar');

	// Rutas proveedor
    Route::post('proveedor', 'ProveedorController@storeProveedor');
    Route::get('proveedor', 'ProveedorController@getAll');

    // Rutas viaticos
    Route::post('viaticos', 'ViaticosController@store');
    Route::get('viaticos', 'ViaticosController@getAll');

    // Rutas rol
    Route::get('rol', 'UsuarioController@getRolAll');
    Route::get('usuario/rol/{id}', 'UsuarioController@getUsuarioByRol');

    // Vehiculo
    Route::post('vehiculo/create', 'RutaController@storeVehiculo');
    Route::get('vehiculo', 'RutaController@getAllVehiculos');
    Route::post('tipo_vehiculo', 'RutaController@storeTipoVehiculo');
    Route::get('tipo_vehiculo', 'RutaController@getAllTipoVehiculos');

    Route::post('unidad_metrica', 'UnidadMetricaController@storeUnidad');
    Route::get('unidad_metrica', 'UnidadMetricaController@getUnidad');

    Route::post('equivalencia', 'UnidadMetricaController@storeEquivalencia');
    Route::get('equivalencia', 'UnidadMetricaController@getEquivalencia');

    Route::post('pedido_planificacion', 'PedidoController@replanificarPedido');
    Route::get('pedido_planificacion', 'PedidoController@pedidoParaReplanificar');

    Route::get('pedido_no_entregado', 'PedidoController@pedidosNoEntregados');

    Route::get('motivos_no_entrega', 'PedidoController@motivosNoEntregados');

    Route::get('canales_venta', 'PedidoController@canalesVenta');
});
