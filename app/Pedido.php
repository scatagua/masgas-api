<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $table = "pedido";

    protected $fillable = ["id_pedido_grupo", "id_articulo", "nu_cantidad", "nu_subtotal"];

    public $timestamps = false;


}
