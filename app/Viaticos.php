<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Viaticos extends Model
{
    protected $table = "viaticos";

    protected $fillable = ["id_usuario", "nu_monto", "tx_factura", "dt_fecha"];

    public $timestamps = false;
}
