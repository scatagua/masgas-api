<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    protected $table = "rol";

    protected $fillable = ["tx_nombre"];

    public $timestamps = false;

    public function mensaje()
    {
        return $this->belongsToMany(NotificacionMensaje::class, 'rel_notificacion_rol', 'id', 'id_rol');
    }
}
