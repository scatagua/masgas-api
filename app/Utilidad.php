<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Utilidad extends Model
{
    //

    public static function is_json($string,$return_data = false,$list = false) {
	    $data = json_decode($string, $list);
	    return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : TRUE) : FALSE;
	}
}
