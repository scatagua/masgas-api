<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificacionMensaje extends Model
{
    protected $table = "notificacion_mensaje";

    protected $fillable = ["tx_mensaje"];

    public $timestamps = false;

    public function rols()
    {
        return $this->belongsToMany(Rol::class, 'rel_notificacion_rol', 'id', 'id_notificacion_mensaje');
    }
}
