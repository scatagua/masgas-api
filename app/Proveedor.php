<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    protected $table = "proveedor";

    protected $fillable = ["tx_nombre", "tx_numero_documento", "tx_direccion", "tx_telefono", "tx_correo"];

    public $timestamps = false;
}
