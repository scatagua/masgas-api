<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model
{
    protected $table = "vehiculo";

    protected $fillable = ["id_tipo_vehiculo", "tx_numero_identificacion", "id_usuario", "tx_marca", "tx_modelo"];
    //protected $fillable = ["id_tipo_vehiculo", "tx_numero_identificacion", "id_usuario"];

    public $timestamps = false;
}
