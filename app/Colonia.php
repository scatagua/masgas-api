<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colonia extends Model
{
    protected $table = "colonia";

    protected $fillable = ["nombre"];
}
