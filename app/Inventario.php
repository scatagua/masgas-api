<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventario extends Model
{
    protected $table = "historial_inventario";

    protected $fillable = ["id_articulo","tx_nombre", "nu_cantidad", "id_usuario", "dt_fecha", "id_proveedor", "nu_solicitado", "nu_merma", "nu_monto", "nu_flete","nu_precio_unitario", "tx_numero_orden"];

    public $timestamps = false;
}
