<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{
    protected $table = "articulo";

    protected $fillable = ["tx_nombre", "nu_precio", "id_articulo_categoria", "nu_inventario"];

    public $timestamps = false;
}
