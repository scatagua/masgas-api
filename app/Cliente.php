<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = "cliente";

    protected $fillable = ["tx_nombre1", "tx_apellido1", "tx_nombre2", "tx_apellido2", "dt_fecha_nacimiento", "id_tipo_identificacion", "nu_identificacion", "tx_sexo", "bo_activado", "id_tipo_documento"];

}
