<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelRutaDistribuccion extends Model
{
    protected $table = "rel_ruta_distribuccion";

    protected $fillable = ["id_ruta", "id_distribuidor", "id_colonia"];

}
