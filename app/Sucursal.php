<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    protected $table = "sucursal";

    protected $fillable = ["tx_nombre", "tx_numero", "tx_colonia", "tx_codigo_postal"];

    public $timestamps = false;
}
