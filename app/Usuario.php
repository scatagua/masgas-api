<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = "usuario";

    protected $fillable = ["tx_nombre1", "tx_apellido1", "tx_nombre2", "tx_apellido2", "dt_fecha_nacimiento",
        "id_tipo_identificacion", "nu_identificacion", "tx_sexo", "bo_activo", "id_rol",
        "tx_correo", "tx_telefono_celular", "tx_telefono_local", "tx_password", "tx_codigo",
        "tx_cargo", "dt_fecha_ingreso", "bo_impreso"];

    public $timestamps = false;

    public function randomString($length) {
	    $salt = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
	    
	    $rand = '';
	    $i = 0;
	    while ($i < $length) {
	        //Loop hasta que el string aleatorio contenga la longitud ingresada.
	        $num = rand() % strlen($salt);
	        $tmp = substr($salt, $num, 1);	
	        $rand = $rand . $tmp;
	        $i++;
	    }
	    //Retorno del string aleatorio.
	    return $rand;
	}
}
