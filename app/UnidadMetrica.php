<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnidadMetrica extends Model
{
    protected $table = "unidad_metrica";

    protected $fillable = ["tx_nombre", "tx_simbolo"];

    public $timestamps = false;
}
