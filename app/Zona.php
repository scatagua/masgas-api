<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zona extends Model
{
    protected $table = "zona";

    protected $fillable = ["tx_nombre", "id_ciudad"];

    public $timestamps = false;
}
