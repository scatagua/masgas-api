<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notificacion extends Model
{
    protected $table = "notificacion";

    protected $fillable = ["id_usuario_receptor", "id_usuario_emisor", "id_mensaje", "dt_fecha", "tx_cuerpo"];

    public $timestamps = false;
}
