<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelNotificacionRol extends Model
{
    
    protected $table = "rel_notificacion_rol";

    protected $fillable = ["id_rol", "id_notificacion_mensaje"];

    public $timestamps = false;
}
