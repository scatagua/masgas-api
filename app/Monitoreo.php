<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Monitoreo extends Model
{
    protected $table = "monitoreo";

    protected $fillable = ["id_usuario", "nu_latitud", "nu_longitud", "id_vehiculo"];

}
