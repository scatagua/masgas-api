<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Auditoria extends Model
{
    protected $table = "auditoria";

    protected $fillable = ["nu_modulo", "nu_accion", "nu_identificador", "dt_fecha_accion", "id_usuario_accion"];

    public $timestamps = false;

    /*
    	Explicacion de modulos:

    	1 - Cliente Fisico
    	2 - Distribuidor
    	3 - Articulo
    	4 - 
    	5 - Direccion
    	6 - Ruta
        7 - Administrador
        8 - RRHH
        9 - Planta
        10 - Supervisor
        11 - Atencion al cliente o call center
    */
}
