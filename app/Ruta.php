<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ruta extends Model
{
    protected $table = "ruta";

    protected $fillable = ["alias"];

    public $timestamps = false;
}
