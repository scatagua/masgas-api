<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equivalencia extends Model
{
    protected $table = "equivalencia";

    protected $fillable = ["nu_valor", "id_unidad_from", "id_unidad_to"];

    public $timestamps = false;
}
