<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehiculoTipo extends Model
{
    protected $table = "tipo_vehiculo";

    protected $fillable = ["tx_nombre"];

    public $timestamps = false;
}
