<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    protected $table = "ciudad";

    protected $fillable = ["tx_nombre", "id_estado"];

    public $timestamps = false;
}
