<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MotivoNoEntrega extends Model
{
    protected $table = "motivo_no_entrega";

    protected $fillable = ["nombre"];
}
