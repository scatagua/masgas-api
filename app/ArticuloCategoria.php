<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticuloCategoria extends Model
{
    protected $table = "articulo_categoria";

    protected $fillable = ["tx_nombre"];

    public $timestamps = false;
}
