<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'v1/usuario*',
        'v1/tipo_identificacion*',
        'v1/cliente*',
        'v1/cliente_moral*',
        'v1/direccion*',
        'v1/login*',
        'v1/distribuidor*'
    ];
}
