<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'  => 'required',
            'nombre1'  => 'required',
            'apellido1'  => 'required',
            'telefono_celular'  => 'required',
            'telefono_local'  => 'required',
            'correo'  => 'required|email',
            'sexo'  => 'required',
            'fecha_nacimiento'  => 'required',
            'identificacion'  => 'required',
            'tipo_identificacion'  => 'required',
            'direccion' => 'required',
            'direccion_longitud' => 'required',
            'direccion_latitud' => 'required'
        ];
    }
}
