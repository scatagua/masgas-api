<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PedidoStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_direccion' => ['required', 'integer'],
            'bo_pagada' => ['required', 'boolean'],
            'id_articulo' => ['required', 'array'],
            'nu_cantidad' => ['required', 'array'],
            'nu_subtotal' => ['required', 'array']
        ];
    }
}
