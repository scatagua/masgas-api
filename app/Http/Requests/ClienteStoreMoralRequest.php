<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteStoreMoralRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'  => 'required',
            'empresa'  => 'required',
            'telefono_celular'  => 'required',
            'telefono_local'  => 'required',
            'correo'  => 'required|email',
            'identificacion'  => 'required',
            'direccion' => 'required',
            'direccion_longitud' => 'required',
            'direccion_latitud' => 'required'
        ];
    }
}
