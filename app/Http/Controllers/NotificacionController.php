<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Notificacion;
use App\NotificacionMensaje;
use App\RelNotificacionRol;
use App\Rol;

class NotificacionController extends Controller
{
    
    /**
     * Crear una nueva direccion para un usuario.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_usuario' => 'required',
            'id_mensaje' => 'required',
            'tx_cuerpo' => 'required'
        ]);
        $data = $request->all();

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Los parametros enviados no son validos, por favor verifique',
                'errors' => $validator->errors()
            ];
        }

        $user = DB::select('
                SELECT *
                FROM usuario 
                WHERE id = '.$data['id_usuario'].'
        ');

        if (count($user) == 0) {
            return [
                'status' => false,
                'mensaje' => 'El id de usuario enviado no existe',
                'errors' => []
            ];
        }

        if (isset($data['id_receptor'])) {
            $id_receptor = $data['id_receptor'];
        }else{
            $id_receptor = 1;
        }
        
        Notificacion::create([
            'id_usuario_emisor' => $data['id_usuario'],
            'id_usuario_receptor' => $id_receptor,
            'id_mensaje' => $data['id_mensaje'],
            'dt_fecha' => date('Y-m-d'),
            'tx_cuerpo' => $data['tx_cuerpo']
        ]);

        $notificacion = DB::select('
                SELECT n.id AS id_notificacion, n.id_usuario_emisor AS usuario_emisor, n.id_usuario_receptor AS usuario_receptor, n.dt_fecha As fecha, n.id_mensaje AS id_mensaje
                FROM notificacion AS n
                WHERE n.id_usuario_emisor = '.$data['id_usuario'].' OR n.id_usuario_receptor = '.$data['id_usuario'].' 
            '); 

        return [
            'status' => true,
            'mensaje' => 'Se registro con exito',
            'data' => [
                'notificacion' => $notificacion
            ]
        ];
    }

    /**
     * Crear una nueva direccion para un usuario.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeNotificacionMensaje(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tx_mensaje' => 'required',
            'id_rol' => 'required'
        ]);
        $data = $request->all();

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Los parametros enviados no son validos, por favor verifique',
                'errors' => $validator->errors()
            ];
        }

        $nm = new NotificacionMensaje([
            "tx_mensaje" => "tx_mensaje"
        ]);
        $nm->save();

        if (is_array($data['id_rol'])) {
            while(list($key,$value) = each($data['id_rol']))
            {
                $rnr = new RelNotificacionRol([
                    "id_rol" => $data["id_rol"][$key],
                    "id_notificacion_mensaje" => $nm->id
                ]);
                $rnr->save();
            }
        }

        return [
            'status' => true,
            'mensaje' => 'Se registro con exito'
        ];
    }


    public function getMensajeAll()
    {
        $notificacionMensaje = NotificacionMensaje::with(['rols'])->get();

        return [
            "status" => true,
            "mensaje" => "Registro exitoso",
            "data" => [
                "mensajes" => $notificacionMensaje
            ]
        ];
    }


    public function getMensajeRolAll()
    {
        $notificacionMensaje = Rol::with(['mensaje'])->get();

        return [
            "status" => true,
            "mensaje" => "Registro exitoso",
            "data" => [
                "mensajes" => $notificacionMensaje
            ]
        ];
    }

    public function getNovedades()
    {
        $notificacionMensaje = DB::select(
            'SELECT n.tx_cuerpo AS cuerpo_mensaje, nm.tx_mensaje AS mensaje, concat_ws(\' \', u1.tx_nombre1, u1.tx_apellido1) AS emisor, 
                concat_ws(\' \', u2.tx_nombre1, u2.tx_apellido1) AS receptor, n.dt_fecha AS fecha 
                FROM notificacion AS n
                JOIN usuario AS u1 ON n.id_usuario_emisor = u1.id 
                JOIN usuario AS u2 ON n.id_usuario_receptor = u2.id 
                JOIN notificacion_mensaje AS nm ON n.id_mensaje = nm.id 
                ');

        return [
            "status" => true,
            "mensaje" => "Consulta exitosa",
            "data" => [
                "mensajes" => $notificacionMensaje
            ]
        ];
    }

    public function notificacionUsuario($id)
    {
        $notificacionMensaje = DB::select(
            'SELECT n.tx_cuerpo AS cuerpo_mensaje, nm.tx_mensaje AS mensaje, concat_ws(\' \', u1.tx_nombre1, u1.tx_apellido1) AS emisor, 
                concat_ws(\' \', u2.tx_nombre1, u2.tx_apellido1) AS receptor, n.dt_fecha AS fecha 
                FROM notificacion AS n
                JOIN usuario AS u1 ON n.id_usuario_emisor = u1.id 
                JOIN usuario AS u2 ON n.id_usuario_receptor = u2.id 
                JOIN notificacion_mensaje AS nm ON n.id_mensaje = nm.id 
                WHERE n.id_usuario_emisor = '.$id.' OR n.id_usuario_receptor = '.$id.'
                ');

        return [
            "status" => true,
            "mensaje" => "Consulta exitosa",
            "data" => [
                "mensajes" => $notificacionMensaje
            ]
        ];
    }

    public function notificacionReportadasPor($id)
    {
        $notificacionMensaje = DB::select(
            'SELECT n.tx_cuerpo AS cuerpo_mensaje, nm.tx_mensaje AS mensaje, concat_ws(\' \', u1.tx_nombre1, u1.tx_apellido1) AS emisor, 
                concat_ws(\' \', u2.tx_nombre1, u2.tx_apellido1) AS receptor, n.dt_fecha AS fecha 
                FROM notificacion AS n
                JOIN usuario AS u1 ON n.id_usuario_emisor = u1.id 
                JOIN usuario AS u2 ON n.id_usuario_receptor = u2.id 
                JOIN notificacion_mensaje AS nm ON n.id_mensaje = nm.id 
                WHERE u1.id_rol = '.$id.'
                ');

        return [
            "status" => true,
            "mensaje" => "Consulta exitosa",
            "data" => [
                "mensajes" => $notificacionMensaje
            ]
        ];
    }

    public function direccionUsuario($id)
    {
        $obj = Direccion::where('id_usuario', '=', $id)
            ->get();

        if ($obj->count() == 0) {
            return [
                'detalle' => false,
                'error' => 'No se consigieron direcciones con el id de usuario suministrado'
            ];
        }

        return [
            'detalle' => true,
            'direccion' => $obj
        ];
    }
}
