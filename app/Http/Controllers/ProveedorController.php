<?php

namespace App\Http\Controllers;

use Validator;

use App\Proveedor;
use Illuminate\Http\Request;

class ProveedorController extends Controller
{
    public function storeProveedor(Request $request) {
        $validator = Validator::make($request->all(), [
            'nombre'  => 'required',
            'numero_identificacion'  => 'required',
            'direccion'  => 'required',
            'telefono'  => 'required',
            'correo'  => 'required'
        ]);

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Verifique que no haya dejado ningún campo vacio o que su correo o número de identificación no este en uso',
                'errors' => $validator->errors()
            ];
        }

        $data = $request->all();

        $obj = new Proveedor([
            'tx_nombre' => $data['nombre'],
            'tx_telefono' => $data['telefono'],
            'tx_correo' => $data['correo'],
            'tx_numero_documento' => $data['numero_identificacion'],
            'tx_direccion' => $data['direccion']
        ]);
        $obj->save();

        return [
            'status' => true,
            'mensaje'  => 'Se creo con exito el proveedor'
        ];
    }

    public function getAll()
    {
        return Proveedor::all();
    }
}
