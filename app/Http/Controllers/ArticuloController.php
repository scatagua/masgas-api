<?php

namespace App\Http\Controllers;

use Validator;

use App\Articulo;
use App\Inventario;
use App\Auditoria;

use App\Http\Requests\ArticuloStoreRequest;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

/**
 * @resource Articulo
 *
 * Modulo de Articulo
 */
class ArticuloController extends Controller
{
    /**
     * Muestra todos los articulos
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd(QrCode::encoding('UTF-8')->generate('Puta Madre'));
        return Articulo::all();
    }

    /**
     * Muestra todos los articulos
     *
     * @return \Illuminate\Http\Response
     */
    public function getArticulosCategoria($id)
    {
        //dd(QrCode::encoding('UTF-8')->generate('Puta Madre'));
        return DB::select('
                SELECT a.id AS id_articulo, a.tx_nombre AS articulo, a.nu_precio AS precio, 
                (
                    SELECT sum(hi.nu_cantidad)
                    FROM historial_inventario AS hi 
                    WHERE hi.id_articulo = a.id
                ) AS cantidad, a.id_articulo_categoria AS id_articulo_categoria
                FROM articulo AS a
                WHERE a.id_articulo_categoria = '.$id.'
            ');
    }

    /**
     * Crear nuevo articulo
     *
     * @param  ArticuloStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticuloStoreRequest $request)
    {
        $data = $request->validated();

        $obj = new Articulo([
            'tx_nombre' => $data['tx_nombre'],
            'nu_precio' => $data['nu_precio'],
            'nu_cantidad' => $data['nu_cantidad']
        ]);
        $obj->save();

        Auditoria::create([
            'nu_modulo' => 3,
            'nu_accion' => 1,
            'nu_identificador' => $obj->id,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => $data['id_usuario']
        ]);

        $inv = new Inventario([
            'id_articulo' => $obj->id,
            'nu_cantidad' => $data['nu_cantidad'],
            'id_usuario' => $data['id_usuario'],
            'dt_fecha' => date('Y-m-d')
        ]);
        $inv->save();

        return [
        	"crear" => true
        ];
    }

    /**
     * Mostrar un articulo especifico.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $obj = Articulo::findOrFail($id);

        if ($obj == null) {
            return [
                'detalle' => false,
                'error' => 'No se consigio el articulo con el id suministrado'
            ];
        }

        return [
            'detalle' => true,
            'articulo' => $obj
        ];
    }


    /**
     * Modificar un articulo.
     *
     * 
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $obj = Articulo::find($id);

        if ($obj == null) {
            return [
                'modificar' => false,
                'error' => 'No se consigio el articulo con el id suministrado'
            ];
        }

        $obj->save();

        return [
            'modificar' => true
        ];
    }
}
