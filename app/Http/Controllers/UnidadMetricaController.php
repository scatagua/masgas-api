<?php

namespace App\Http\Controllers;

use Validator;

use App\Equivalencia;
use App\UnidadMetrica;
use Illuminate\Http\Request;

class UnidadMetricaController extends Controller
{
    public function getUnidad() {
        return [
            'status' => true,
            'mensaje' => 'Consulta exitosa',
            'data' => UnidadMetrica::all()
        ];
    }

    public function storeUnidad(Request $request) {
        $validator = Validator::make($request->all(), [
            'tx_nombre'  => 'required',
            'tx_simbolo'  => 'required'
        ]);

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Verifique que no haya dejado ningún campo vacio',
                'errors' => $validator->errors()
            ];
        }

        $data = $request->all();

        UnidadMetrica::create($data);

        return [
            'status' => true,
            'mensaje' => 'Creacion exitosa'
        ];
    }

    public function getEquivalencia() {
        return [
            'status' => true,
            'mensaje' => 'Consulta exitosa',
            'data' => DB::select('
                SELECT um1.tx_nombre AS unidad_desde, um1.tx_simbolo AS simbolo_desde, um2.tx_nombre AS unidad_hasta, 
                um2.tx_simbolo AS simbol_hasta, e.nu_valor AS conversion 
                FROM equivalencia AS e 
                JOIN unidad_metrica AS um1 ON e.id_unidad_from = um1.id 
                JOIN unidad_metrica AS um2 ON e.id_unidad_to = um2.id                 
            ')
        ];

    }

    public function storeEquivalencia(Request $request) {
        $validator = Validator::make($request->all(), [
            'id_unidad_from'  => 'required',
            'id_unidad_to'  => 'required',
            'nu_valor'  => 'required'
        ]);

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Verifique que no haya dejado ningún campo vacio',
                'errors' => $validator->errors()
            ];
        }

        $data = $request->all();

        Equivalencia::create($data);

        return [
            'status' => true,
            'mensaje' => 'Creacion exitosa'
        ];
    }
}
