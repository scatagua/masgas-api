<?php

namespace App\Http\Controllers;

use Validator;

use App\Usuario;
use App\Rol;
use App\TipoIdentificacion;
use App\Direccion;
use App\Auditoria;
use App\Notificacion;
use App\NotificacionMensaje;
use App\RelNotificacionRol;
use App\Vehiculo;
use App\MotivoNoEntrega;

use App\Http\Requests\DistribuidorStoreRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\ClienteStoreRequest;
use App\Http\Requests\ClienteStoreMoralRequest;
use App\Http\Requests\UsuarioActivoUpdateRequest;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

/**
 * @resource Usuario
 *
 * Modulo de Usuario
 */
class UsuarioController extends Controller
{

    /**
     * Muestra todos los usuarios
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return DB::select('
                SELECT u.id AS id_usuario, u.tx_nombre1 AS nombre, u.tx_apellido1 AS apellido, 
                u.tx_correo AS correo, u.tx_telefono_celular AS telefono, u.id_rol AS id_rol, r.tx_nombre AS perfil, 
                u.tx_codigo AS codigo, u.bo_activo AS activo 
                FROM usuario AS u
                JOIN rol AS r ON u.id_rol = r.id
            ');
    }

    /**
     * Muestra los usuarios por rol
     *
     * @return \Illuminate\Http\Response
     */
    public function getUsuarios($id)
    {
        return Usuario::where('id_rol', '=', $id)->get();
    }

    /**
     * Inicio de Sesión
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request) {
        // Creamos las reglas de validación
        $this->validate(request(), [
            'tx_correo' => ['required'],
            'tx_password' => ['required']
        ]);
        $data = $request->all();

        if (filter_var($data['tx_correo'], FILTER_VALIDATE_EMAIL)) {
            $objects = DB::select('
                SELECT u.bo_activo AS bo_activo, u.id AS id_usuario, u.tx_nombre1 AS nombre1, u.tx_apellido1 AS apellido1, ti.tx_nombre AS tipo_identificacion, u.nu_identificacion AS identificacion, u.tx_nombre2 AS nombre2, u.tx_apellido2 AS apellido2, u.tx_sexo AS sexo, u.dt_fecha_nacimiento AS fecha_nacimiento, r.tx_nombre AS rol, u.id_rol AS id_rol, u.tx_telefono_celular AS telefono_celular, u.tx_telefono_local AS telefono_local, u.tx_codigo AS codigo, u.tx_correo AS correo
                FROM usuario AS u 
                JOIN tipo_identificacion AS ti ON u.id_tipo_identificacion = ti.id
                JOIN rol AS r ON u.id_rol = r.id 
                WHERE u.tx_correo = "'.$data['tx_correo'].'" AND u.tx_password = "'.$data['tx_password'].'" 
            ');
        }
        else if (is_numeric($data['tx_correo'])) {
            $objects = DB::select('
                SELECT u.bo_activo AS bo_activo, u.id AS id_usuario, u.tx_nombre1 AS nombre1, u.tx_apellido1 AS apellido1, ti.tx_nombre AS tipo_identificacion, u.nu_identificacion AS identificacion, u.tx_nombre2 AS nombre2, u.tx_apellido2 AS apellido2, u.tx_sexo AS sexo, u.dt_fecha_nacimiento AS fecha_nacimiento, r.tx_nombre AS rol, u.id_rol AS id_rol, u.tx_telefono_celular AS telefono_celular, u.tx_telefono_local AS telefono_local, u.tx_codigo AS codigo, u.tx_correo AS correo
                FROM usuario AS u 
                JOIN tipo_identificacion AS ti ON u.id_tipo_identificacion = ti.id
                JOIN rol AS r ON u.id_rol = r.id 
                WHERE (u.tx_telefono_celular = "'.$data['tx_correo'].'" OR u.tx_telefono_local = "'.$data['tx_correo'].'") AND u.tx_password = "'.$data['tx_password'].'" 
            ');
            /*
            DB::table('usuario')
                    ->select(DB::raw('usuario.id as id_usuario, usuario.tx_nombre1 as nombre1, usuario.tx_apellido1 as apellido1, tipo_identificacion.tx_nombre as identificacion, usuario.nu_identificacion as nu_identificacion, usuario.tx_nombre2 as nombre2, usuario.tx_apellido2 as apellido2, usuario.tx_sexo as sexo, usuario.dt_fecha_nacimiento as fecha_nacimiento, rol.tx_nombre as rol, rol.id as id_rol, usuario.tx_telefono_celular as telefono_celular, usuario.tx_telefono_local as telefono_local, usuario.tx_codigo as codigo'))
                    ->join('tipo_identificacion','usuario.id_tipo_identificacion','=','tipo_identificacion.id')
                    ->join('rol','usuario.id_rol','=','rol.id')
                    ->where('usuario.tx_telefono_celular', '=', $data['tx_correo'])
                    ->where('usuario.tx_password', '=', $data['tx_password'])
                    ->get();
                    */
        } 
        else {
            $objects = array();
        }

        if (count($objects) > 0 and $objects[0]->bo_activo == 1 ) {

            $notificacionMensaje = NotificacionMensaje::all(); /*DB::select('
                    SELECT nm.id AS id_mensaje, nm.tx_mensaje AS mensaje
                    FROM rel_notificacion_rol AS rnr
                    JOIN notificacion_mensaje AS nm ON rnr.id_notificacion_mensaje = nm.id
                    WHERE rnr.id_rol = '.$objects[0]->id_rol.'
                ');*/

            $relNotMen = RelNotificacionRol::all();

            $notificacion = DB::select('
                SELECT n.id AS id_notificacion, n.id_usuario_emisor AS usuario_emisor, n.id_usuario_receptor AS usuario_receptor, n.dt_fecha As fecha, n.id_mensaje AS id_mensaje, tx_cuerpo AS cuerpo
                FROM notificacion AS n
                WHERE n.id_usuario_emisor = '.$objects[0]->id_usuario.' OR n.id_usuario_receptor = '.$objects[0]->id_usuario.' 
            '); 

            if ($objects[0]->id_rol == 1 || $objects[0]->id_rol == 5) {
                $direcciones = DB::table('direccion')
                            ->select(DB::raw('direccion.id as id_direccion, direccion.tx_nombre as direccion, direccion.nu_latitud as latitud, direccion.nu_longitud as longitud, direccion.tx_direccion as denominacion, direccion.tx_referencia as referencia, direccion.tx_numero_ext as num_ext, direccion.tx_numero_int as num_int, direccion.tx_codigo_postal as codigo_postal, tx_colonia as colonia'))
                            ->where('direccion.id_usuario', '=', $objects[0]->id_usuario)
                            ->get();
                foreach ($direcciones as $key => $value) {
                    $direcciones[$key]->latitud = number_format($value->latitud, 2, '.', '');
                    $direcciones[$key]->longitud = number_format($value->longitud, 2, '.', '');
                }

                $articulos = DB::select('
                    SELECT a.id AS id_articulo, a.tx_nombre AS articulo, a.nu_precio AS precio, 
                    (
                        SELECT sum(hi.nu_cantidad)
                        FROM historial_inventario AS hi 
                        WHERE hi.id_articulo = a.id
                    ) AS cantidad, a.id_articulo_categoria AS id_articulo_categoria
                    FROM articulo AS a
                ');

                $articulosCategoria = DB::select('
                    SELECT ac.id AS id_articulo_categoria, ac.tx_nombre AS categoria
                    FROM articulo_categoria AS ac
                ');

                $pedidos = DB::select(' SELECT pg.id AS id_pedido_grupo, pg.dt_fecha AS fecha, pg.id_direccion AS id_direccion, pg.bo_pagada AS pagado, 
                    pg.tx_estatus AS estatus, pg.tx_codigo AS codigo
                    FROM pedido_grupo AS pg
                    JOIN direccion AS d ON pg.id_direccion = d.id 
                    WHERE d.id_usuario = '.$objects[0]->id_usuario.'
					');

                $items = DB::select('
                    SELECT p.id AS id_item, p.id_pedido_grupo AS id_pedido_grupo, p.id_articulo AS id_producto, p.nu_cantidad AS cantidad, 
                    p.nu_subtotal AS subtotal
                    FROM pedido AS p
                    JOIN pedido_grupo AS pg ON p.id_pedido_grupo = pg.id
                    JOIN direccion AS d ON pg.id_direccion = d.id 
                    WHERE d.id_usuario = '.$objects[0]->id_usuario.'
                ');

                return [
                    'status' => true,
                    'data' => [
                        'usuario'  => $objects,
                        'direccion' => $direcciones,
                        'articulos' => $articulos,
                        'articulosCategoria' => $articulosCategoria,
                        'pedidos' => $pedidos,
                        'items' => $items,
                        'mensajes' => $notificacionMensaje,
                        'notificacion' => $notificacion,
                        'relNotRol' => $relNotMen
                    ]
                ]; 
            }
            else if ($objects[0]->id_rol == 2) {
                $pedidos = DB::select('
                    SELECT pg.id AS id_pedido_grupo, pg.dt_fecha AS fecha, pg.id_direccion AS id_direccion, pg.bo_pagada AS pagado, 
                    pg.tx_estatus AS estatus, u.tx_codigo AS codigo, d.id_usuario AS id_usuario
                    FROM pedido_grupo AS pg
                    JOIN direccion AS d ON  d.id = pg.id_direccion
                    JOIN usuario AS u ON u.id = id_usuario
                    WHERE pg.id_distribuidor = '.$objects[0]->id_usuario.'
                ');   

                $items = DB::select('
                    SELECT p.id AS id_item, p.id_pedido_grupo AS id_pedido_grupo, p.id_articulo AS id_producto, p.nu_cantidad AS cantidad, p.nu_subtotal AS subtotal
                    FROM pedido_grupo AS pg
                    JOIN pedido AS p ON p.id_pedido_grupo = pg.id
                    WHERE pg.id_distribuidor = '.$objects[0]->id_usuario.'
                ');      

                $direcciones = DB::select('
                    SELECT d.id AS id_direccion, d.tx_nombre AS direccion, d.nu_latitud AS latitud, d.nu_longitud AS longitud, d.id_usuario AS id_usuario, d.tx_direccion AS denominacion, d.tx_referencia AS referencia
                    FROM pedido_grupo AS pg
                    JOIN direccion AS d ON pg.id_direccion = d.id
                    WHERE pg.id_distribuidor = '.$objects[0]->id_usuario.'
                ');    

                $articulos = DB::select('
                    SELECT a.id AS id_articulo, a.tx_nombre AS articulo, a.nu_precio AS precio, 
                    (
                        SELECT sum(hi.nu_cantidad)
                        FROM historial_inventario AS hi 
                        WHERE hi.id_articulo = a.id
                    ) AS cantidad, a.id_articulo_categoria AS id_articulo_categoria
                    FROM articulo AS a
                ');

                $articulosCategoria = DB::select('
                    SELECT ac.id AS id_articulo_categoria, ac.tx_nombre AS categoria
                    FROM articulo_categoria AS ac
                ');   

                $clientes = DB::select('
                    SELECT u.id AS id_usuario, u.tx_nombre1 AS nombre1, u.tx_nombre2 AS nombre2, u.tx_apellido1 AS apellido1, u.tx_apellido2 AS apellido2, u.tx_sexo AS sexo, u.dt_fecha_nacimiento AS fecha_nacimiento, u.tx_telefono_celular AS telefono_celular, u.tx_telefono_local AS telefono_local
                    FROM pedido_grupo AS pg
                    JOIN direccion AS d ON pg.id_direccion = d.id
                    JOIN usuario AS u ON d.id_usuario = u.id
                    WHERE pg.id_distribuidor = '.$objects[0]->id_usuario.'
                ');   

                return [
                    'status' => true,
                    'data' => [
                        'usuario'  => $objects,
                        'direccion' => $direcciones,
                        'articulos' => $articulos,
                        'articulosCategoria' => $articulosCategoria,
                        'pedidos' => $pedidos,
                        'items' => $items, 
                        'clientes' => $clientes,
                        'mensajes' => $notificacionMensaje,
                        'notificacion' => $notificacion,
                        'relNotRol' => $relNotMen,
                        'motivosNoEntrega' => MotivoNoEntrega::all()
                    ]
                ]; 
            }      
        }
        
        return [
            'status' => false,
            'mensaje'  => 'Las credenciales no corresponden a ninguno de nuestros usuarios'
        ];
        
        //User::create($request->all());
    }


     /**
     * Inicio de Sesión
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function loginweb(Request $request) {
        // Creamos las reglas de validación
        $this->validate(request(), [
            'tx_correo' => ['required'],
            'tx_password' => ['required']
        ]);
        $data = $request->all();

        if (filter_var($data['tx_correo'], FILTER_VALIDATE_EMAIL)) {
            $objects = DB::select('
                SELECT u.id AS id_usuario, u.tx_nombre1 AS nombre1, u.tx_apellido1 AS apellido1, ti.tx_nombre AS tipo_identificacion, u.nu_identificacion AS identificacion, u.tx_nombre2 AS nombre2, u.tx_apellido2 AS apellido2, u.tx_sexo AS sexo, u.dt_fecha_nacimiento AS fecha_nacimiento, r.tx_nombre AS rol, u.id_rol AS id_rol, u.tx_telefono_celular AS telefono_celular, u.tx_telefono_local AS telefono_local, u.tx_codigo AS codigo, u.tx_correo AS correo
                FROM usuario AS u 
                JOIN tipo_identificacion AS ti ON u.id_tipo_identificacion = ti.id
                JOIN rol AS r ON u.id_rol = r.id 
                WHERE u.tx_correo = "'.$data['tx_correo'].'" AND u.tx_password = "'.$data['tx_password'].'" 
            ');
        }
        else if (is_numeric($data['tx_correo'])) {
            $objects = DB::select('
                SELECT u.id AS id_usuario, u.tx_nombre1 AS nombre1, u.tx_apellido1 AS apellido1, ti.tx_nombre AS tipo_identificacion, u.nu_identificacion AS identificacion, u.tx_nombre2 AS nombre2, u.tx_apellido2 AS apellido2, u.tx_sexo AS sexo, u.dt_fecha_nacimiento AS fecha_nacimiento, r.tx_nombre AS rol, u.id_rol AS id_rol, u.tx_telefono_celular AS telefono_celular, u.tx_telefono_local AS telefono_local, u.tx_codigo AS codigo, u.tx_correo AS correo
                FROM usuario AS u 
                JOIN tipo_identificacion AS ti ON u.id_tipo_identificacion = ti.id
                JOIN rol AS r ON u.id_rol = r.id 
                WHERE (u.tx_telefono_celular = "'.$data['tx_correo'].'" OR u.tx_telefono_local = "'.$data['tx_correo'].'") AND u.tx_password = "'.$data['tx_password'].'" 
            ');
            /*
            DB::table('usuario')
                    ->select(DB::raw('usuario.id as id_usuario, usuario.tx_nombre1 as nombre1, usuario.tx_apellido1 as apellido1, tipo_identificacion.tx_nombre as identificacion, usuario.nu_identificacion as nu_identificacion, usuario.tx_nombre2 as nombre2, usuario.tx_apellido2 as apellido2, usuario.tx_sexo as sexo, usuario.dt_fecha_nacimiento as fecha_nacimiento, rol.tx_nombre as rol, rol.id as id_rol, usuario.tx_telefono_celular as telefono_celular, usuario.tx_telefono_local as telefono_local, usuario.tx_codigo as codigo'))
                    ->join('tipo_identificacion','usuario.id_tipo_identificacion','=','tipo_identificacion.id')
                    ->join('rol','usuario.id_rol','=','rol.id')
                    ->where('usuario.tx_telefono_celular', '=', $data['tx_correo'])
                    ->where('usuario.tx_password', '=', $data['tx_password'])
                    ->get();
                    */
        } 
        else {
            $objects = array();
        }


        if (count($objects) > 0) {

            $notificacionMensaje = NotificacionMensaje::all(); /*DB::select('
                    SELECT nm.id AS id_mensaje, nm.tx_mensaje AS mensaje
                    FROM rel_notificacion_rol AS rnr
                    JOIN notificacion_mensaje AS nm ON rnr.id_notificacion_mensaje = nm.id
                    WHERE rnr.id_rol = '.$objects[0]->id_rol.'
                ');*/

            $relNotMen = RelNotificacionRol::all();

            $notificacion = DB::select('
                SELECT n.id AS id_notificacion, n.id_usuario_emisor AS usuario_emisor, n.id_usuario_receptor AS usuario_receptor, n.dt_fecha As fecha, n.id_mensaje AS id_mensaje, tx_cuerpo AS cuerpo
                FROM notificacion AS n
                WHERE n.id_usuario_emisor = '.$objects[0]->id_usuario.' OR n.id_usuario_receptor = '.$objects[0]->id_usuario.' 
            '); 

            if (1==1) {

                 if ($objects[0]->id_rol == 1 or $objects[0]->id_rol == 5 or $objects[0]->id_rol == 2 or $objects[0]->id_rol == 10) {
                      return [
                        'status' => false,
                        'mensaje'  => 'Las credenciales no corresponden a ninguno de nuestros usuarios'
                          ];
                 }
                $direcciones = DB::table('direccion')
                            ->select(DB::raw('direccion.id as id_direccion, direccion.tx_nombre as direccion, direccion.nu_latitud as latitud, direccion.nu_longitud as longitud, direccion.tx_direccion as denominacion, direccion.tx_referencia as referencia, direccion.tx_numero_ext as num_ext, direccion.tx_numero_int as num_int, direccion.tx_codigo_postal as codigo_postal, tx_colonia as colonia'))
                            ->where('direccion.id_usuario', '=', $objects[0]->id_usuario)
                            ->get();
                foreach ($direcciones as $key => $value) {
                    $direcciones[$key]->latitud = number_format($value->latitud, 2, '.', '');
                    $direcciones[$key]->longitud = number_format($value->longitud, 2, '.', '');
                }

                $articulos = DB::select('
                    SELECT a.id AS id_articulo, a.tx_nombre AS articulo, a.nu_precio AS precio, 
                    (
                        SELECT sum(hi.nu_cantidad)
                        FROM historial_inventario AS hi 
                        WHERE hi.id_articulo = a.id
                    ) AS cantidad, a.id_articulo_categoria AS id_articulo_categoria
                    FROM articulo AS a
                ');

                $articulosCategoria = DB::select('
                    SELECT ac.id AS id_articulo_categoria, ac.tx_nombre AS categoria
                    FROM articulo_categoria AS ac
                ');

                $pedidos = DB::select('
                    SELECT pg.id AS id_pedido_grupo, pg.dt_fecha AS fecha, pg.id_direccion AS id_direccion, pg.bo_pagada AS pagado, 
                    pg.tx_estatus AS estatus, pg.tx_codigo AS codigo
                    FROM pedido_grupo AS pg
                    JOIN direccion AS d ON pg.id_direccion = d.id 
                    WHERE d.id_usuario = '.$objects[0]->id_usuario.'
                ');

                $items = DB::select('
                    SELECT p.id AS id_item, p.id_pedido_grupo AS id_pedido_grupo, p.id_articulo AS id_producto, p.nu_cantidad AS cantidad, 
                    p.nu_subtotal AS subtotal
                    FROM pedido AS p
                    JOIN pedido_grupo AS pg ON p.id_pedido_grupo = pg.id
                    JOIN direccion AS d ON pg.id_direccion = d.id 
                    WHERE d.id_usuario = '.$objects[0]->id_usuario.'
                ');

                return [
                    'status' => true,
                    'data' => [
                        'usuario'  => $objects,
                        'direccion' => $direcciones,
                        'articulos' => $articulos,
                        'articulosCategoria' => $articulosCategoria,
                        'pedidos' => $pedidos,
                        'items' => $items,
                        'mensajes' => $notificacionMensaje,
                        'notificacion' => $notificacion,
                        'relNotRol' => $relNotMen
                    ]
                ]; 
            }
            else if ($objects[0]->id_rol == 2) {
                $pedidos = DB::select('
                    SELECT pg.id AS id_pedido_grupo, pg.dt_fecha AS fecha, pg.id_direccion AS id_direccion, pg.bo_pagada AS pagado, 
                    pg.tx_estatus AS estatus, pg.tx_codigo AS codigo
                    FROM pedido_grupo AS pg
                    WHERE pg.id_distribuidor = '.$objects[0]->id_usuario.'
                ');   

                $items = DB::select('
                    SELECT p.id AS id_item, p.id_pedido_grupo AS id_pedido_grupo, p.id_articulo AS id_producto, p.nu_cantidad AS cantidad, p.nu_subtotal AS subtotal
                    FROM pedido_grupo AS pg
                    JOIN pedido AS p ON p.id_pedido_grupo = pg.id
                    WHERE pg.id_distribuidor = '.$objects[0]->id_usuario.'
                ');      

                $direcciones = DB::select('
                    SELECT d.id AS id_direccion, d.tx_nombre AS direccion, d.nu_latitud AS latitud, d.nu_longitud AS longitud, d.id_usuario AS id_usuario, d.tx_direccion AS denominacion, d.tx_referencia AS referencia
                    FROM pedido_grupo AS pg
                    JOIN direccion AS d ON pg.id_direccion = d.id
                    WHERE pg.id_distribuidor = '.$objects[0]->id_usuario.'
                ');    

                $articulos = DB::select('
                    SELECT a.id AS id_articulo, a.tx_nombre AS articulo, a.nu_precio AS precio, 
                    (
                        SELECT sum(hi.nu_cantidad)
                        FROM historial_inventario AS hi 
                        WHERE hi.id_articulo = a.id
                    ) AS cantidad, a.id_articulo_categoria AS id_articulo_categoria
                    FROM articulo AS a
                ');

                $articulosCategoria = DB::select('
                    SELECT ac.id AS id_articulo_categoria, ac.tx_nombre AS categoria
                    FROM articulo_categoria AS ac
                ');   

                $clientes = DB::select('
                    SELECT u.id AS id_usuario, u.tx_nombre1 AS nombre1, u.tx_nombre2 AS nombre2, u.tx_apellido1 AS apellido1, u.tx_apellido2 AS apellido2, u.tx_sexo AS sexo, u.dt_fecha_nacimiento AS fecha_nacimiento, u.tx_telefono_celular AS telefono_celular, u.tx_telefono_local AS telefono_local
                    FROM pedido_grupo AS pg
                    JOIN direccion AS d ON pg.id_direccion = d.id
                    JOIN usuario AS u ON d.id_usuario = u.id
                    WHERE pg.id_distribuidor = '.$objects[0]->id_usuario.'
                ');   

                return [
                    'status' => true,
                    'data' => [
                        'usuario'  => $objects,
                        'direccion' => $direcciones,
                        'articulos' => $articulos,
                        'articulosCategoria' => $articulosCategoria,
                        'pedidos' => $pedidos,
                        'items' => $items, 
                        'clientes' => $clientes,
                        'mensajes' => $notificacionMensaje,
                        'notificacion' => $notificacion,
                        'relNotRol' => $relNotMen,
                        'motivosNoEntrega' => MotivoNoEntrega::all()
                    ]
                ]; 
            }      
        }else{
             return [
            'status' => false,
            'mensaje'  => 'Las credenciales no corresponden a ninguno de nuestros usuarios'
              ];
        }
        
       
        
        //User::create($request->all());
    }






    /**
     * Pre Registro
     *
     * Debe ser consumido antes de registrar un cliente o despachador, ya que tiene los datos necesarios para dicho registro
     * 
     * @return \Illuminate\Http\Response
     */
    public function registrar() {
        $roles = Rol::all();
        $identificacion = TipoIdentificacion::all();

        return [
            'rol' => $roles,
            'identificacion' => $identificacion
        ];
    }

    /**
     * Pre Registro
     *
     * Debe ser consumido antes de registrar un cliente o despachador, ya que tiene los datos necesarios para dicho registro
     * 
     * @return \Illuminate\Http\Response
     */
    public function clientesQr() {
        $clientes = DB::select('
            SELECT u.id AS id_usuario, u.tx_nombre1 AS nombre1, u.tx_nombre2 AS nombre2, u.tx_apellido1 AS apellido1, u.tx_apellido2 AS apellido2, u.tx_sexo AS sexo, u.dt_fecha_nacimiento AS fecha_nacimiento, u.tx_telefono_celular AS telefono_celular, u.tx_telefono_local AS telefono_local, u.tx_codigo AS qr
            FROM usuario AS u
            WHERE (u.id_rol = 1 OR u.id_rol = 5) AND u.bo_impreso = 0
        ');  

        return [
            'clientes' => $clientes
        ];
    }

    public function qrImpreso(Request $request) {

        $validator = Validator::make($request->all(), [
            'id_usuario' => 'required'            
        ]);
        $data = $request->all();

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Los parametros enviados no son validos, por favor verifique',
                'errors' => $validator->errors()
            ];
        }

       $id_usuarios = explode(",", $data['id_usuario']);
        if (is_array($id_usuarios)) {
              echo "entro en el if";
            foreach ($id_usuarios as $key => $value) {
                $pedido = Usuario::findOrFail($value);     
                if ($pedido != null) {
                    $pedido->bo_impreso = true;
                    $pedido->save();
                }   
            }
            return [
                'status' => true,
                'mensaje' => 'Se actualizaron los clientes con el QR impreso'
            ];
        }
        else {
            echo "entro en el else";
            $pedido = Usuario::findOrFail($id_usuarios);     
            if ($pedido != null) {
                $pedido->bo_impreso = true;
                $pedido->save();
                return [
                    'status' => true,
                    'mensaje' => 'Se actualizo el cliente con el QR impreso'
                ];
            }
            else {
                return [
                    'status' => true,
                    'mensaje' => 'El cliente no existe'
                ];
            }    
        }
        die("murio");
        return [
                    'status' => false,
                    'mensaje' => 'falla'
                ];

    }

    /**
     * Crear nuevo distribuidor
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeDistribuidor(Request $request) {
        $validator = Validator::make($request->all(), [
            'password'  => 'required',
            'nombre1'  => 'required',
            'apellido1'  => 'required',
            'telefono_celular'  => 'required',
            //'telefono_local'  => 'required',
            'correo'  => 'required|email|unique:usuario,tx_correo',
            'sexo'  => 'required',
            'fecha_nacimiento'  => 'required',
            'nu_identificacion'  => 'required|unique:usuario',
            'tipo_identificacion'  => 'required',
            'direccion' => 'required',
            'direccion_longitud' => 'required',
            'direccion_latitud' => 'required',
            'colonia' => 'required',
            'codigo_postal' => 'required',
            'id_vehiculo' => 'required'
        ]);

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Verifique que no haya dejado ningún campo vacio o que su correo o número de identificación no este en uso',
                'errors' => $validator->errors()
            ];
        }

        $data = $request->all();

        $usuario = new Usuario();

        $obj = new Usuario([
            //'tx_alias' => $data['tx_alias'],
            'tx_password' => $data['password'],
            'tx_nombre1' => $data['nombre1'],
            'tx_nombre2' => '',
            'tx_apellido1' => $data['apellido1'],   
            'tx_apellido2' => '',
            'tx_telefono_local' => isset($data['telefono_local']) ? $data['telefono_local'] : '',
            'tx_telefono_celular' => $data['telefono_celular'],
            'tx_correo' => $data['correo'],
            'dt_fecha_nacimiento' => $data['fecha_nacimiento'],
            'tx_sexo' => $data['sexo'],
            'id_tipo_identificacion' => $data['tipo_identificacion'],
            'nu_identificacion' => $data['nu_identificacion'],
            'id_rol' => 2,
            'bo_activo' => true,
            'bo_impreso' => false,
            'tx_codigo' => $usuario->randomString(8),
        ]);
        
        if (isset($data['nombre2'])) {
            $obj->tx_nombre2 = $data['nombre2'];
        }
        else {
            $obj->tx_nombre2 = '';
        }
        if (isset($data['apellido2'])) {
            $obj->tx_apellido2 = $data['apellido2'];
        }
        else {
            $obj->tx_apellido2 = '';
        }
        $obj->save();

        Auditoria::create([
            'nu_modulo' => 1,
            'nu_accion' => 1,
            'nu_identificador' => $obj->id,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        $dir = new Direccion([
            'tx_nombre' => 'Principal',
            'id_usuario' => $obj->id,
            'nu_longitud' => $data['direccion_longitud'],
            'nu_latitud' => $data['direccion_latitud'],
            'tx_direccion' => $data['direccion'],
            'tx_referencia' => isset($data['punto_referencia']) ? $data['punto_referencia'] : '',
            'tx_colonia' => $data['colonia'],
            'tx_codigo_postal' => $data['codigo_postal'],
            'tx_numero_int' => $data['numero_int'],
            'tx_numero_ext' => $data['numero_ext']
        ]);
        $dir->save();

        Auditoria::create([
            'nu_modulo' => 5,
            'nu_accion' => 1,
            'nu_identificador' => $dir->id,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        $vehiculo = Vehiculo::findOrFail($data['id_vehiculo']);
        if (isset($vehiculo) && $vehiculo != null) {
            $vehiculo->id_usuario = $obj->id;
            $vehiculo->save();
        }

        return [
            'status' => true,
            'mensaje'  => 'Se creo con exito el distribuidor'
        ];
    }

    /**
     * Crear nuevo distribuidor
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAtencionCliente(Request $request) {
        $validator = Validator::make($request->all(), [
            'password'  => 'required',
            'nombre1'  => 'required',
            'apellido1'  => 'required',
            'telefono_celular'  => 'required',
            //'telefono_local'  => 'required',
            'correo'  => 'required|email|unique:usuario,tx_correo',
            'sexo'  => 'required',
            'fecha_nacimiento'  => 'required',
            'nu_identificacion'  => 'required|unique:usuario',
            'tipo_identificacion'  => 'required',
            'direccion' => 'required',
            'direccion_longitud' => 'required',
            'direccion_latitud' => 'required',
            'cargo' => 'required',
            'fecha_ingreso' => 'required',
            'colonia' => 'required',
            'codigo_postal' => 'required'
        ]);

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Verifique que no haya dejado ningún campo vacio o que su correo o número de identificación no este en uso',
                'errors' => $validator->errors()
            ];
        }

        $data = $request->all();

        $usuario = new Usuario();

        $obj = new Usuario([
            //'tx_alias' => $data['tx_alias'],
            'tx_password' => $data['password'],
            'tx_nombre1' => $data['nombre1'],
            'tx_nombre2' => '',
            'tx_apellido1' => $data['apellido1'],   
            'tx_apellido2' => '',
            'tx_telefono_local' => isset($data['telefono_local']) ? $data['telefono_local'] : '',
            'tx_telefono_celular' => $data['telefono_celular'],
            'tx_correo' => $data['correo'],
            'dt_fecha_nacimiento' => $data['fecha_nacimiento'],
            'tx_sexo' => $data['sexo'],
            'id_tipo_identificacion' => $data['tipo_identificacion'],
            'nu_identificacion' => $data['nu_identificacion'],
            'id_rol' => 6,
            'bo_activo' => true,
            'dt_fecha_ingreso' => $data['fecha_ingreso'],
            'tx_cargo' => $data['cargo'],
            'bo_impreso' => false,
            'tx_codigo' => $usuario->randomString(8),
        ]);
        
        if (isset($data['nombre2'])) {
            $obj->tx_nombre2 = $data['nombre2'];
        }
        else {
            $obj->tx_nombre2 = '';
        }
        if (isset($data['apellido2'])) {
            $obj->tx_apellido2 = $data['apellido2'];
        }
        else {
            $obj->tx_apellido2 = '';
        }
        $obj->save();

        Auditoria::create([
            'nu_modulo' => 1,
            'nu_accion' => 1,
            'nu_identificador' => $obj->id,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        $dir = new Direccion([
            'tx_nombre' => 'Principal',
            'id_usuario' => $obj->id,
            'nu_longitud' => $data['direccion_longitud'],
            'nu_latitud' => $data['direccion_latitud'],
            'tx_direccion' => $data['direccion'],
            'tx_referencia' => isset($data['punto_referencia']) ? $data['punto_referencia'] : '',
            'tx_colonia' => $data['colonia'],
            'tx_codigo_postal' => $data['codigo_postal'],
            'tx_numero_int' => $data['numero_int'],
            'tx_numero_ext' => $data['numero_ext']
        ]);
        $dir->save();

        Auditoria::create([
            'nu_modulo' => 5,
            'nu_accion' => 1,
            'nu_identificador' => $dir->id,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        return [
            'status' => true,
            'mensaje'  => 'Se creo con exito el perfil de atención al cliente'
        ];
    }

    /**
     * Crear nuevo distribuidor
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeSupervisor(Request $request) {
        $validator = Validator::make($request->all(), [
            'password'  => 'required',
            'nombre1'  => 'required',
            'apellido1'  => 'required',
            'telefono_celular'  => 'required',
            //'telefono_local'  => 'required',
            'correo'  => 'required|email|unique:usuario,tx_correo',
            'sexo'  => 'required',
            'fecha_nacimiento'  => 'required',
            'nu_identificacion'  => 'required|unique:usuario',
            'tipo_identificacion'  => 'required',
            'direccion' => 'required',
            'direccion_longitud' => 'required',
            'direccion_latitud' => 'required',
            'cargo' => 'required',
            'fecha_ingreso' => 'required',
            'colonia' => 'required',
            'codigo_postal' => 'required'
        ]);

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Verifique que no haya dejado ningún campo vacio o que su correo o número de identificación no este en uso',
                'errors' => $validator->errors()
            ];
        }

        $data = $request->all();

        $usuario = new Usuario();

        $obj = new Usuario([
            //'tx_alias' => $data['tx_alias'],
            'tx_password' => $data['password'],
            'tx_nombre1' => $data['nombre1'],
            'tx_nombre2' => '',
            'tx_apellido1' => $data['apellido1'],   
            'tx_apellido2' => '',
            'tx_telefono_local' => isset($data['telefono_local']) ? $data['telefono_local'] : '',
            'tx_telefono_celular' => $data['telefono_celular'],
            'tx_correo' => $data['correo'],
            'dt_fecha_nacimiento' => $data['fecha_nacimiento'],
            'tx_sexo' => $data['sexo'],
            'id_tipo_identificacion' => $data['tipo_identificacion'],
            'nu_identificacion' => $data['nu_identificacion'],
            'id_rol' => 7,
            'bo_activo' => true,
            'dt_fecha_ingreso' => $data['fecha_ingreso'],
            'tx_cargo' => $data['cargo'],
            'bo_impreso' => false,
            'tx_codigo' => $usuario->randomString(8),
        ]);
        
        if (isset($data['nombre2'])) {
            $obj->tx_nombre2 = $data['nombre2'];
        }
        else {
            $obj->tx_nombre2 = '';
        }
        if (isset($data['apellido2'])) {
            $obj->tx_apellido2 = $data['apellido2'];
        }
        else {
            $obj->tx_apellido2 = '';
        }
        $obj->save();

        Auditoria::create([
            'nu_modulo' => 10,
            'nu_accion' => 1,
            'nu_identificador' => $obj->id,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        $dir = new Direccion([
            'tx_nombre' => 'Principal',
            'id_usuario' => $obj->id,
            'nu_longitud' => $data['direccion_longitud'],
            'nu_latitud' => $data['direccion_latitud'],
            'tx_direccion' => $data['direccion'],
            'tx_referencia' => isset($data['punto_referencia']) ? $data['punto_referencia'] : '',
            'tx_colonia' => $data['colonia'],
            'tx_codigo_postal' => $data['codigo_postal'],
            'tx_numero_int' => $data['numero_int'],
            'tx_numero_ext' => $data['numero_ext']
        ]);
        $dir->save();

        Auditoria::create([
            'nu_modulo' => 5,
            'nu_accion' => 1,
            'nu_identificador' => $dir->id,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        return [
            'status' => true,
            'mensaje'  => 'Se creo con exito el perfil de supervisor'
        ];
    }

    /**
     * Crear nuevo distribuidor
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePlanta(Request $request) {
        $validator = Validator::make($request->all(), [
            'password'  => 'required',
            'nombre1'  => 'required',
            'apellido1'  => 'required',
            'telefono_celular'  => 'required',
            //'telefono_local'  => 'required',
            'correo'  => 'required|email|unique:usuario,tx_correo',
            'sexo'  => 'required',
            'fecha_nacimiento'  => 'required',
            'nu_identificacion'  => 'required|unique:usuario',
            'tipo_identificacion'  => 'required',
            'direccion' => 'required',
            'direccion_longitud' => 'required',
            'direccion_latitud' => 'required',
            'cargo' => 'required',
            'fecha_ingreso' => 'required',
            'colonia' => 'required',
            'codigo_postal' => 'required'
        ]);

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Verifique que no haya dejado ningún campo vacio o que su correo o número de identificación no este en uso',
                'errors' => $validator->errors()
            ];
        }

        $data = $request->all();

        $usuario = new Usuario();

        $obj = new Usuario([
            //'tx_alias' => $data['tx_alias'],
            'tx_password' => $data['password'],
            'tx_nombre1' => $data['nombre1'],
            'tx_nombre2' => '',
            'tx_apellido1' => $data['apellido1'],   
            'tx_apellido2' => '',
            'tx_telefono_local' => isset($data['telefono_local']) ? $data['telefono_local'] : '',
            'tx_telefono_celular' => $data['telefono_celular'],
            'tx_correo' => $data['correo'],
            'dt_fecha_nacimiento' => $data['fecha_nacimiento'],
            'tx_sexo' => $data['sexo'],
            'id_tipo_identificacion' => $data['tipo_identificacion'],
            'nu_identificacion' => $data['nu_identificacion'],
            'id_rol' => 8,
            'bo_activo' => true,
            'dt_fecha_ingreso' => $data['fecha_ingreso'],
            'tx_cargo' => $data['cargo'],
            'bo_impreso' => false,
            'tx_codigo' => $usuario->randomString(8),
        ]);
        
        if (isset($data['nombre2'])) {
            $obj->tx_nombre2 = $data['nombre2'];
        }
        else {
            $obj->tx_nombre2 = '';
        }
        if (isset($data['apellido2'])) {
            $obj->tx_apellido2 = $data['apellido2'];
        }
        else {
            $obj->tx_apellido2 = '';
        }
        $obj->save();

        Auditoria::create([
            'nu_modulo' => 1,
            'nu_accion' => 1,
            'nu_identificador' => $obj->id,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        $dir = new Direccion([
            'tx_nombre' => 'Principal',
            'id_usuario' => $obj->id,
            'nu_longitud' => $data['direccion_longitud'],
            'nu_latitud' => $data['direccion_latitud'],
            'tx_direccion' => $data['direccion'],
            'tx_referencia' => isset($data['punto_referencia']) ? $data['punto_referencia'] : '',
            'tx_colonia' => $data['colonia'],
            'tx_codigo_postal' => $data['codigo_postal'],
            'tx_numero_int' => $data['numero_int'],
            'tx_numero_ext' => $data['numero_ext']
        ]);
        $dir->save();

        Auditoria::create([
            'nu_modulo' => 5,
            'nu_accion' => 1,
            'nu_identificador' => $dir->id,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        return [
            'status' => true,
            'mensaje'  => 'Se creo con exito el perfil de planta'
        ];
    }

    /**
     * Crear nuevo distribuidor
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeRrhh(Request $request) {
        $validator = Validator::make($request->all(), [
            'password'  => 'required',
            'nombre1'  => 'required',
            'apellido1'  => 'required',
            'telefono_celular'  => 'required',
            //'telefono_local'  => 'required',
            'correo'  => 'required|email|unique:usuario,tx_correo',
            'sexo'  => 'required',
            'fecha_nacimiento'  => 'required',
            'nu_identificacion'  => 'required|unique:usuario',
            'tipo_identificacion'  => 'required',
            'direccion' => 'required',
            'direccion_longitud' => 'required',
            'direccion_latitud' => 'required',
            'cargo' => 'required',
            'fecha_ingreso' => 'required',
            'colonia' => 'required',
            'codigo_postal' => 'required'
        ]);

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Verifique que no haya dejado ningún campo vacio o que su correo o número de identificación no este en uso',
                'errors' => $validator->errors()
            ];
        }

        $data = $request->all();

        $usuario = new Usuario();

        $obj = new Usuario([
            //'tx_alias' => $data['tx_alias'],
            'tx_password' => $data['password'],
            'tx_nombre1' => $data['nombre1'],
            'tx_nombre2' => '',
            'tx_apellido1' => $data['apellido1'],   
            'tx_apellido2' => '',
            'tx_telefono_local' => isset($data['telefono_local']) ? $data['telefono_local'] : '',
            'tx_telefono_celular' => $data['telefono_celular'],
            'tx_correo' => $data['correo'],
            'dt_fecha_nacimiento' => $data['fecha_nacimiento'],
            'tx_sexo' => $data['sexo'],
            'id_tipo_identificacion' => $data['tipo_identificacion'],
            'nu_identificacion' => $data['nu_identificacion'],
            'id_rol' => 9,
            'bo_activo' => true,
            'dt_fecha_ingreso' => $data['fecha_ingreso'],
            'tx_cargo' => $data['cargo'],
            'bo_impreso' => false,
            'tx_codigo' => $usuario->randomString(8),
        ]);
        
        if (isset($data['nombre2'])) {
            $obj->tx_nombre2 = $data['nombre2'];
        }
        else {
            $obj->tx_nombre2 = '';
        }
        if (isset($data['apellido2'])) {
            $obj->tx_apellido2 = $data['apellido2'];
        }
        else {
            $obj->tx_apellido2 = '';
        }
        $obj->save();

        Auditoria::create([
            'nu_modulo' => 1,
            'nu_accion' => 1,
            'nu_identificador' => $obj->id,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        $dir = new Direccion([
            'tx_nombre' => 'Principal',
            'id_usuario' => $obj->id,
            'nu_longitud' => $data['direccion_longitud'],
            'nu_latitud' => $data['direccion_latitud'],
            'tx_direccion' => $data['direccion'],
            'tx_referencia' => isset($data['punto_referencia']) ? $data['punto_referencia'] : '',
            'tx_colonia' => $data['colonia'],
            'tx_codigo_postal' => $data['codigo_postal'],
            'tx_numero_int' => $data['numero_int'],
            'tx_numero_ext' => $data['numero_ext']
        ]);
        $dir->save();

        Auditoria::create([
            'nu_modulo' => 5,
            'nu_accion' => 1,
            'nu_identificador' => $dir->id,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        return [
            'status' => true,
            'mensaje'  => 'Se creo con exito el perfil de RRHH'
        ];
    }

    /**
     * Crear nuevo distribuidor
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAdministrador(Request $request) {
        $validator = Validator::make($request->all(), [
            'password'  => 'required',
            'nombre1'  => 'required',
            'apellido1'  => 'required',
            'telefono_celular'  => 'required',
            //'telefono_local'  => 'required',
            'correo'  => 'required|email|unique:usuario,tx_correo',
            'sexo'  => 'required',
            'fecha_nacimiento'  => 'required',
            'nu_identificacion'  => 'required|unique:usuario',
            'tipo_identificacion'  => 'required',
            'direccion' => 'required',
            'direccion_longitud' => 'required',
            'direccion_latitud' => 'required',
            'colonia' => 'required',
            'codigo_postal' => 'required'
        ]);

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Verifique que no haya dejado ningún campo vacio o que su correo o número de identificación no este en uso',
                'errors' => $validator->errors()
            ];
        }

        $data = $request->all();

        $usuario = new Usuario();

        $obj = new Usuario([
            //'tx_alias' => $data['tx_alias'],
            'tx_password' => $data['password'],
            'tx_nombre1' => $data['nombre1'],
            'tx_nombre2' => '',
            'tx_apellido1' => $data['apellido1'],   
            'tx_apellido2' => '',
            'tx_telefono_local' => isset($data['telefono_local']) ? $data['telefono_local'] : '',
            'tx_telefono_celular' => $data['telefono_celular'],
            'tx_correo' => $data['correo'],
            'dt_fecha_nacimiento' => $data['fecha_nacimiento'],
            'tx_sexo' => $data['sexo'],
            'id_tipo_identificacion' => $data['tipo_identificacion'],
            'nu_identificacion' => $data['nu_identificacion'],
            'id_rol' => 4,
            'bo_activo' => true,
            'bo_impreso' => false,
            'tx_codigo' => $usuario->randomString(8),
        ]);

        if (isset($data['nombre2'])) {
            $obj->tx_nombre2 = $data['nombre2'];
        }
        else {
            $obj->tx_nombre2 = '';
        }
        if (isset($data['apellido2'])) {
            $obj->tx_apellido2 = $data['apellido2'];
        }
        else {
            $obj->tx_apellido2 = '';
        }
        $obj->save();

        Auditoria::create([
            'nu_modulo' => 7,
            'nu_accion' => 1,
            'nu_identificador' => $obj->id,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        $dir = new Direccion([
            'tx_nombre' => 'Principal',
            'id_usuario' => $obj->id,
            'nu_longitud' => $data['direccion_longitud'],
            'nu_latitud' => $data['direccion_latitud'],
            'tx_direccion' => $data['direccion'],
            'tx_referencia' => isset($data['punto_referencia']) ? $data['punto_referencia'] : '',
            'tx_colonia' => $data['colonia'],
            'tx_codigo_postal' => $data['codigo_postal'],
            'tx_numero_int' => $data['numero_int'],
            'tx_numero_ext' => $data['numero_ext']
        ]);
        $dir->save();

        Auditoria::create([
            'nu_modulo' => 5,
            'nu_accion' => 1,
            'nu_identificador' => $dir->id,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        return [
            'status' => true,
            'mensaje'  => 'Se creo con exito'
        ];
    }

    public function storeOperador(Request $request) {
        $validator = Validator::make($request->all(), [
            'password'  => 'required',
            'nombre1'  => 'required',
            'apellido1'  => 'required',
            'telefono_celular'  => 'required',
            //'telefono_local'  => 'required',
            'correo'  => 'required|email|unique:usuario,tx_correo',
            'sexo'  => 'required',
            'fecha_nacimiento'  => 'required',
            'nu_identificacion'  => 'required|unique:usuario',
            'tipo_identificacion'  => 'required',
            'direccion' => 'required',
            'direccion_longitud' => 'required',
            'direccion_latitud' => 'required',
            'colonia' => 'required',
            'codigo_postal' => 'required'
        ]);

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Verifique que no haya dejado ningún campo vacio o que su correo o número de identificación no este en uso',
                'errors' => $validator->errors()
            ];
        }

        $data = $request->all();

        $usuario = new Usuario();

        $obj = new Usuario([
            //'tx_alias' => $data['tx_alias'],
            'tx_password' => $data['password'],
            'tx_nombre1' => $data['nombre1'],
            'tx_nombre2' => '',
            'tx_apellido1' => $data['apellido1'],
            'tx_apellido2' => '',
            'tx_telefono_local' => isset($data['telefono_local']) ? $data['telefono_local'] : '',
            'tx_telefono_celular' => $data['telefono_celular'],
            'tx_correo' => $data['correo'],
            'dt_fecha_nacimiento' => $data['fecha_nacimiento'],
            'tx_sexo' => $data['sexo'],
            'id_tipo_identificacion' => $data['tipo_identificacion'],
            'nu_identificacion' => $data['nu_identificacion'],
            'id_rol' => 10,
            'bo_activo' => true,
            'bo_impreso' => false,
            'tx_codigo' => $usuario->randomString(8),
        ]);

        if (isset($data['nombre2'])) {
            $obj->tx_nombre2 = $data['nombre2'];
        }
        else {
            $obj->tx_nombre2 = '';
        }
        if (isset($data['apellido2'])) {
            $obj->tx_apellido2 = $data['apellido2'];
        }
        else {
            $obj->tx_apellido2 = '';
        }
        $obj->save();

        Auditoria::create([
            'nu_modulo' => 11,
            'nu_accion' => 1,
            'nu_identificador' => $obj->id,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        $dir = new Direccion([
            'tx_nombre' => 'Principal',
            'id_usuario' => $obj->id,
            'nu_longitud' => $data['direccion_longitud'],
            'nu_latitud' => $data['direccion_latitud'],
            'tx_direccion' => $data['direccion'],
            'tx_referencia' => isset($data['punto_referencia']) ? $data['punto_referencia'] : '',
            'tx_colonia' => $data['colonia'],
            'tx_codigo_postal' => $data['codigo_postal'],
            'tx_numero_int' => $data['numero_int'],
            'tx_numero_ext' => $data['numero_ext']
        ]);
        $dir->save();

        Auditoria::create([
            'nu_modulo' => 5,
            'nu_accion' => 1,
            'nu_identificador' => $dir->id,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        return [
            'status' => true,
            'mensaje'  => 'Se creo con exito'
        ];
    }

    public function storeStaff(Request $request) {
        $validator = Validator::make($request->all(), [
            'password'  => 'required',
            'nombre1'  => 'required',
            'apellido1'  => 'required',
            'telefono_celular'  => 'required',
            //'telefono_local'  => 'required',
            'correo'  => 'required|email|unique:usuario,tx_correo',
            'sexo'  => 'required',
            'fecha_nacimiento'  => 'required',
            'nu_identificacion'  => 'required|unique:usuario',
            'tipo_identificacion'  => 'required',
            'direccion' => 'required',
            'direccion_longitud' => 'required',
            'direccion_latitud' => 'required',
            'colonia' => 'required',
            'codigo_postal' => 'required'
        ]);

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Verifique que no haya dejado ningún campo vacio o que su correo o número de identificación no este en uso',
                'errors' => $validator->errors()
            ];
        }

        $data = $request->all();

        $usuario = new Usuario();

        $obj = new Usuario([
            //'tx_alias' => $data['tx_alias'],
            'tx_password' => $data['password'],
            'tx_nombre1' => $data['nombre1'],
            'tx_nombre2' => '',
            'tx_apellido1' => $data['apellido1'],
            'tx_apellido2' => '',
            'tx_telefono_local' => isset($data['telefono_local']) ? $data['telefono_local'] : '',
            'tx_telefono_celular' => $data['telefono_celular'],
            'tx_correo' => $data['correo'],
            'dt_fecha_nacimiento' => $data['fecha_nacimiento'],
            'tx_sexo' => $data['sexo'],
            'id_tipo_identificacion' => $data['tipo_identificacion'],
            'nu_identificacion' => $data['nu_identificacion'],
            'id_rol' => 11,
            'bo_activo' => true,
            'bo_impreso' => false,
            'tx_codigo' => $usuario->randomString(8),
        ]);

        if (isset($data['nombre2'])) {
            $obj->tx_nombre2 = $data['nombre2'];
        }
        else {
            $obj->tx_nombre2 = '';
        }
        if (isset($data['apellido2'])) {
            $obj->tx_apellido2 = $data['apellido2'];
        }
        else {
            $obj->tx_apellido2 = '';
        }
        $obj->save();

        Auditoria::create([
            'nu_modulo' => 12,
            'nu_accion' => 1,
            'nu_identificador' => $obj->id,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        $dir = new Direccion([
            'tx_nombre' => 'Principal',
            'id_usuario' => $obj->id,
            'nu_longitud' => $data['direccion_longitud'],
            'nu_latitud' => $data['direccion_latitud'],
            'tx_direccion' => $data['direccion'],
            'tx_referencia' => isset($data['punto_referencia']) ? $data['punto_referencia'] : '',
            'tx_colonia' => $data['colonia'],
            'tx_codigo_postal' => $data['codigo_postal'],
            'tx_numero_int' => $data['numero_int'],
            'tx_numero_ext' => $data['numero_ext']
        ]);
        $dir->save();

        Auditoria::create([
            'nu_modulo' => 5,
            'nu_accion' => 1,
            'nu_identificador' => $dir->id,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        return [
            'status' => true,
            'mensaje'  => 'Se creo con exito'
        ];
    }

    /**
     * Crear nuevo cliente
     *
     * @param  ClienteStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCliente(Request $request) {
        $validator = Validator::make($request->all(), [
            //'password'  => 'required',
            'nombre1'  => 'required',
            'apellido1'  => 'required',
            //'telefono_celular'  => 'required',
            //'telefono_local'  => 'required',
            'correo'  => 'required|email|unique:usuario,tx_correo',
            'sexo'  => 'required',
            'fecha_nacimiento'  => 'required',
            //'nu_identificacion'  => 'required|unique:usuario',
            'tipo_identificacion'  => 'required',
            'direccion' => 'required',
            'direccion_longitud' => 'required',
            'direccion_latitud' => 'required',
            //'punto_referencia' => 'required',
            'colonia' => 'required',
            //'codigo_postal' => 'required'
        ]);

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Verifique que no haya dejado ningún campo vacio o que su correo o número de identificación no este en uso',
                'errors' => $validator->errors()
            ];
        }

        $data = $request->all();

        $us = Usuario::where('nu_identificacion', $data['nu_identificacion'])->get();
        if ($us->count() > 0) {
            return [
                'status' => false,
                'mensaje' => 'El numero de identificación ya existe',
                'errors' => ['El numero de identificación ya existe']
            ];
        }

        if (isset($data['telefono_celular'])) {
            $passTel = Usuario::where('tx_telefono_celular', $data['telefono_celular'])
                                ->orWhere('tx_telefono_local', $data['telefono_celular'])
                                ->get();
            if ($passTel->count() > 0) {
                return [
                    'status' => false,
                    'mensaje' => 'El numero telefonico ya existe por favor intente con otro'
                ];    
            }
        }

        if (isset($data['telefono_local'])) {
            $passTel = Usuario::where('tx_telefono_celular', $data['telefono_local'])
                                ->orWhere('tx_telefono_local', $data['telefono_local'])
                                ->get();
            if ($passTel->count() > 0) {
                return [
                    'status' => false,
                    'mensaje' => 'El numero telefonico ya existe por favor intente con otro'
                ];    
            }      
        }

        if (isset($data['password'])) {
            $password = $data['password'];
        }
        else {
            $password = '';
        }

        $usuario = new Usuario();

        $obj = new Usuario([
            'tx_password' => $password,
            'tx_nombre1' => $data['nombre1'],
            'tx_nombre2' => '',
            'tx_apellido1' => $data['apellido1'],
            'tx_apellido2' => '',
            'tx_telefono_local' => isset($data['telefono_local']) ? $data['telefono_local'] : '',
            'tx_telefono_celular' => isset($data['telefono_celular']) ? $data['telefono_celular'] : '',
            'tx_correo' => $data['correo'],
            'dt_fecha_nacimiento' => $data['fecha_nacimiento'],
            'tx_sexo' => $data['sexo'],
            'id_tipo_identificacion' => $data['tipo_identificacion'],
            'nu_identificacion' => isset($data['nu_identificacion']) ? $data['nu_identificacion'] : '',
            'id_rol' => 1,
            'bo_activo' => true,
            'tx_codigo' => $usuario->randomString(8),
            'bo_impreso' => false
        ]);
        
        if (isset($data['nombre2'])) {
            $obj->tx_nombre2 = $data['nombre2'];
        }
        else {
            $obj->tx_nombre2 = '';
        }
        if (isset($data['apellido2'])) {
            $obj->tx_apellido2 = $data['apellido2'];
        }
        else {
            $obj->tx_apellido2 = '';
        }
        $obj->save();

        //$obj->tx_codigo = "MG-CF-".$obj->id."ID-".$obj->nuevo_identificacion."DT-".date('Ymd');
        //$obj->save();

        Auditoria::create([
            'nu_modulo' => 1,
            'nu_accion' => 1,
            'nu_identificador' => $obj->id,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        $dir = new Direccion([
            'tx_nombre' => 'Principal',
            'id_usuario' => $obj->id,
            'nu_longitud' => $data['direccion_longitud'],
            'nu_latitud' => $data['direccion_latitud'],
            'tx_direccion' => $data['direccion'],
            'tx_referencia' => isset($data['punto_referencia']) ? $data['punto_referencia'] : '',
            'tx_colonia' => $data['colonia'],
            'tx_codigo_postal' => $data['codigo_postal'],
            'tx_numero_int' => $data['numero_int'],
            'tx_numero_ext' => $data['numero_ext'],
            'tx_calle' => $data['tx_calle'],
            'tx_municipio' => $data['tx_municipio'], 
            'telefono' => isset($data['telefono_celular']) ? $data['telefono_celular'] : ''
        ]);
        $dir->save();

        Auditoria::create([
            'nu_modulo' => 5,
            'nu_accion' => 1,
            'nu_identificador' => $dir->id,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 2
        ]);

        return [
            'status' => true,
            'mensaje'  => 'Se creo con exito el cliente',
            'data' => [
                'codigo' => $obj->tx_codigo
            ]
        ];
    }

    /**
     * Crear nuevo cliente moral
     *
     * @param  ClienteStoreMoralRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function storeClienteMoral(Request $request) {
         $validator = Validator::make($request->all(), [
            //'password'  => 'required',
            'empresa'  => 'required',
            //'telefono_celular'  => 'required',
            //'telefono_local'  => 'required',
            'correo'  => 'required|email|unique:usuario,tx_correo',
            //'nu_identificacion'  => 'required|unique:usuario',
            'direccion' => 'required',
            'direccion_longitud' => 'required',
            'direccion_latitud' => 'required',
            //'punto_referencia' => 'required',
            'colonia' => 'required',
            //'codigo_postal' => 'required'
        ]);

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Verifique que no haya dejado ningún campo vacio o que su correo o número de identificación no este en uso',
                'errors' => $validator->errors()
            ];
        }

        $data = $request->all();

        if (isset($data['telefono_celular'])) {
            $passTel = Usuario::where('tx_telefono_celular', $data['telefono_celular'])
                                ->orWhere('tx_telefono_local', $data['telefono_celular'])
                                ->get();
            if ($passTel->count() > 0) {
                return [
                    'status' => false,
                    'mensaje' => 'El numero telefonico ya existe por favor intente con otro'
                ];    
            }
        }

        if (isset($data['telefono_local'])) {
            $passTel = Usuario::where('tx_telefono_celular', $data['telefono_local'])
                                ->orWhere('tx_telefono_local', $data['telefono_local'])
                                ->get();
            if ($passTel->count() > 0) {
                return [
                    'status' => false,
                    'mensaje' => 'El numero telefonico ya existe por favor intente con otro'
                ];    
            }      
        }  

        if (isset($data['password'])) {
            $password = $data['password'];
        }
        else {
            $password = '';
        }

        $usuario = new Usuario();

        $obj = new Usuario([
            'tx_password' => $password,
            'tx_nombre1' => $data['empresa'],
            'tx_nombre2' => '',
            'tx_apellido1' => '',
            'tx_apellido2' => '',
            'tx_telefono_local' => isset($data['telefono_local']) ? $data['telefono_local'] : '',
            'tx_telefono_celular' => isset($data['telefono_celular']) ? $data['telefono_celular'] : '',
            'tx_correo' => $data['correo'],
            'dt_fecha_nacimiento' => date('Y-m-d'),
            'tx_sexo' => '',
            'id_tipo_identificacion' => 4,
            'nu_identificacion' => $data['nu_identificacion'],
            'id_rol' => 5,
            'bo_activo' => true,
            'tx_codigo' => $usuario->randomString(8),
            'bo_impreso' => false
        ]);
        $obj->save();

        //$obj->tx_codigo = "MG-CF-".$obj->id."ID-".$obj->nu_identificacion."DT-".date('Ymd');
        //$obj->save();

        Auditoria::create([
            'nu_modulo' => 1,
            'nu_accion' => 1,
            'nu_identificador' => $obj->id,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        $dir = new Direccion([
            'tx_nombre' => 'Principal',
            'id_usuario' => $obj->id,
            'nu_longitud' => $data['direccion_longitud'],
            'nu_latitud' => $data['direccion_latitud'],
            'tx_direccion' => $data['direccion'],
            'tx_referencia' => isset($data['punto_referencia']) ? $data['punto_referencia'] : '',
            'tx_colonia' => $data['colonia'],
            'tx_codigo_postal' => $data['codigo_postal'],
            'tx_numero_int' => $data['numero_int'],
            'tx_numero_ext' => $data['numero_ext'],
            'telefono' => isset($data['telefono_celular']) ? $data['telefono_celular'] : ''
        ]);
        $dir->save();

        Auditoria::create([
            'nu_modulo' => 5,
            'nu_accion' => 1,
            'nu_identificador' => $dir->id,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 2
        ]);

        return [
            'status' => true,
            'mensaje'  => 'Se creo con exito el cliente',
            'data' => [
                'codigo' => $obj->tx_codigo
            ]
        ];
    }

    /**
     * Cambia el estado de un usuario.
     *
     * Activa o desactiva un usuario
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @response {
     *  data: [],  
     * }   
     * @return \Illuminate\Http\Response
     */
    public function cambiarEstado(Request $request, $id)
    {

        $data = $request;

        $obj = Usuario::find($id);

        if ($obj == null) {
            return [
                'status' => false,
                'mensaje' => 'No se consigio el usuario con el id suministrado'
            ];
        }

        $obj->bo_activo = $data['estado'];
        $obj->save();

        Auditoria::create([
            'nu_modulo' => 1,
            'nu_accion' => 2,
            'nu_identificador' => $obj->id,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => $data['id_usuario']
        ]);

        return [
            'status' => true,
            'mensaje'  => 'Se cambio el estatus con exito del usuario'
        ];
    }

    public function recuperarPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'correo'  => 'required|email'
        ]);

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Problemas para procesar solicitud',
                'errors' => $validator->errors()
            ];
        }

        $data = $request->all();

        $user = DB::select('
                SELECT id, dt_fecha_nacimiento, nu_identificacion, id_rol
                FROM usuario 
                WHERE tx_correo = "'.$data['correo'].'"
        ');
    
        if (count($user) == 0) {
            return [
                'status' => false,
                'mensaje' => 'El correo no corresponde a ningun usuario',
                'errors' => []
            ];
        }

        return [
            'status' => true,
            'mensaje'  => 'Se completo la petición para la recuperación',
            'data' => [
                'usuario' => $user
            ]
        ];
    }

    public function nuevoPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password'  => 'required',
            'id_usuario' => 'required'
        ]);

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Problemas para procesar solicitud',
                'errors' => $validator->errors()
            ];
        }

        $data = $request->all();

        $obj = Usuario::findOrFail($data['id_usuario']);
        $obj->tx_password = $data['password'];
        $obj->save();

        return [
            'status' => true,
            'mensaje'  => 'Se cambio la contraseña con exito'
        ];
    }

    public function change(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_usuario'  => 'required',
            'tx_correo' => 'required|unique:usuario',
            'dt_fecha_nacimiento' => 'required',
            'tx_telefono_celular' => 'required',
            'tx_telefono_local' => 'required',
            'nu_identificacion' => 'required|unique:usuario'
        ]);

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Problemas para procesar solicitud',
                'errors' => $validator->errors()
            ];
        }

        $data = $request->all();

        $obj = Usuario::findOrFail($data['id_usuario']);
        $obj->tx_correo = $data['tx_correo'];
        $obj->dt_fecha_nacimiento = $data['dt_fecha_nacimiento'];
        $obj->tx_telefono_celular = $data['tx_telefono_celular'];
        $obj->tx_telefono_local = $data['tx_telefono_local'];
        $obj->nu_identificacion = $data['nu_identificacion'];
        $obj->save();

        return [
            'status' => true,
            'mensaje'  => 'Se modificaron los datos con exito',
            'data' => [
                'usuario' => $obj
            ]
        ];
    }

    /**
     * Mostrar un usuario especifico.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $obj = Usuario::find($id);

        if ($obj == null) {
            return [
                'detalle' => false,
                'error' => 'No se consigio el usuario con el id suministrado'
            ];
        }

        return [
            'detalle' => true,
            'usuario' => $obj
        ];
    }


    /**
     * Modificar un usuario.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $obj = Usuario::findOrFail($id);

        if ($obj == null) {
            return [
                'modificar' => false,
                'error' => 'No se consigio el usuario con el id suministrado'
            ];
        }

        $obj->save();

        return [
            'status' => true,
            'mensaje'  => 'Consulta exitosa',
        ];

    }

    public function getRolAll()
    {
        return [
            'status' => true,
            'mensaje'  => 'Consulta exitosa',
            'data' => [
                'rol' => Rol::all()
            ]
        ];
    }

    public function getUsuarioByRol($id)
    {
        return [
            'status' => true,
            'mensaje'  => 'Consulta exitosa',
            'data' => [
                'usuario' => Usuario::where('id_rol', $id)->get()
            ]
        ];
    }

}
