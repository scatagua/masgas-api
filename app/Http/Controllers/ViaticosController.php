<?php

namespace App\Http\Controllers;

use Validator;

use App\Viaticos;
use Illuminate\Http\Request;

class ViaticosController extends Controller
{
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'fecha'  => 'required',
            'id_usuario'  => 'required',
            'monto'  => 'required'
        ]);

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Verifique que no haya dejado ningún campo vacio o que su correo o número de identificación no este en uso',
                'errors' => $validator->errors()
            ];
        }

        $data = $request->all();

        $factura = '';
        if (isset($data['factura'])) {
            $factura = $data['factura'];
        }

        $obj = new Viaticos([
            'dt_fecha' => $data['fecha'],
            'id_usuario' => $data['id_usuario'],
            'nu_monto' => $data['monto'],
            'tx_factura' => $factura
        ]);
        $obj->save();

        return [
            'status' => true,
            'mensaje'  => 'Se cargaron los viaticos'
        ];
    }

    public function getAll()
    {
        return DB::select('
                SELECT u.id AS id_usuario, u.tx_nombre1 AS nombre, u.tx_apellido1 AS apellido, v.nu_monto AS monto, 
                v.tx_factura AS factura, v.dt_fecha AS fecha, u.tx_correo AS correo, u.tx_telefono_celular AS telefono, 
                u.id_rol AS id_rol, r.tx_nombre AS perfil, u.bo_activo AS activo,  
                FROM viaticos AS v
                JOIN usuario AS u ON v.id_usuario = u.id
                JOIN rol AS r ON u.id_rol = r.id
            ');
    }
}
