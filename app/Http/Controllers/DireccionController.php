<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;

use App\Pais;
use App\Estado;
use App\Ciudad;
use App\Zona;
use App\Direccion;
use App\Sucursal;

//use App\Http\Requests\DireccionStoreRequest;

//use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

/**
 * @resource Direccion
 *
 * Modulo de Direccion
 */
class DireccionController extends Controller
{
    /**
     * Muestra todas las direcciones registrada por los clientes
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Direccion::all();
    }

    /**
     * Muestra la distribucion geopolitica
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function territorio() {
        $paises = Pais::all();
        foreach ($paises as $key1 => $pais) {
            $estados = Estado::where('id_pais', '=', $pais->id)->get();
            foreach ($estados as $key2 => $estado) {
                $ciudades = Ciudad::where('id_estado', '=', $estado->id)->get();
                foreach ($ciudades as $key3 => $ciudad) {
                    $zonas = Zona::where('id_ciudad', '=', $ciudad->id)->get();
                    $ciudades[$key3]['zonas'] = $zonas;
                }
                $estados[$key2]['ciudades'] = $ciudades;
            }
            $paises[$key1]['estados'] = $estados;
        }
        return $paises;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Crear una nueva direccion para un usuario.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeDireccionCliente(Request $request)
    {
        $validator = Validator::make($request->all(), [
            //'tx_nombre' => 'required',
            'id_usuario' => 'required',
            'nu_longitud' => 'required',
            'nu_latitud' => 'required',
            'tx_direccion' => 'required',
            'tx_municipio' => 'required',
            //'tx_referencia' => 'required',
            //'tx_codigo_postal' => 'required',
            'tx_colonia' => 'required',
            'tx_numero_ext' => 'required'
        ]);
        $data = $request->all();

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Los parametros enviados no son validos, por favor verifique',
                'errors' => $validator->errors()
            ];
        }

        $user = DB::select('
                SELECT *
                FROM usuario 
                WHERE id = '.$data['id_usuario'].'
        ');

        if (count($user) == 0) {
            return [
                'status' => false,
                'mensaje' => 'El id de usuario enviado no existe',
                'errors' => []
            ];
        }

        $dir = new Direccion([
            'tx_nombre' => $data['tx_direccion'],
            'id_usuario' => $data["id_usuario"],
            'nu_longitud' => $data['nu_longitud'],
            'nu_latitud' => $data['nu_latitud'],
            'tx_direccion' => $data['tx_direccion'],
            'tx_referencia' => isset($data['tx_referencia']) ? $data['tx_referencia'] : '',
            'tx_colonia' => $data['tx_colonia'],
            'tx_codigo_postal' => isset($data['codigo_postal']) ? $data['codigo_postal'] : '',
            'tx_numero_int' => isset($data['tx_numero_int']) ? $data['tx_numero_int'] : '',
            'tx_numero_ext' => $data['tx_numero_ext'],
            'tx_municipio' => $data['tx_municipio'],
            'telefono' => isset($data['telefono']) ? $data['telefono'] : ''
        ]);
        $dir->save();

        $direccion = DB::select('
                SELECT id AS id_direccion, tx_nombre AS direccion, tx_direccion AS denominacion, tx_referencia AS referencia, nu_latitud AS latitud, nu_longitud AS longitud, tx_colonia AS colonia, tx_codigo_postal AS codigo_postal, tx_numero_ext AS num_ext, tx_numero_int AS num_int
                FROM direccion 
                WHERE id_usuario = '.$data['id_usuario'].'
                ORDER BY id DESC LIMIT 1
        ');

        return [
            'status' => true,
            'mensaje' => 'Se registro con exito',
            'data' => [
                'direccion' => $direccion
            ]
        ];
    }

    public function updateDireccion(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tx_nombre' => 'required',
            'id_usuario' => 'required',
            'nu_longitud' => 'required',
            'nu_latitud' => 'required',
            'tx_direccion' => 'required',
            'tx_referencia' => 'required',
            'id_direccion' => 'required',
            'tx_codigo_postal' => 'required',
            'tx_colonia' => 'required'
        ]);
        $data = $request->all();

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Los parametros enviados no son validos, por favor verifique',
                'errors' => $validator->errors()
            ];
        }

        $user = DB::select('
                SELECT *
                FROM usuario 
                WHERE id = '.$data['id_usuario'].'
        ');

        if (count($user) == 0) {
            return [
                'status' => false,
                'mensaje' => 'El id de usuario enviado no existe',
                'errors' => []
            ];
        }

        $d = Direccion::findOrFail($data['id_direccion']);
        if ($d == null) { 
            return [
                'status' => false,
                'mensaje' => 'El id de direccion enviado no existe',
                'errors' => []
            ];   
        }
        $d->nu_longitud = $data['nu_longitud'];
        $d->nu_latitud = $data['nu_latitud'];
        $d->tx_direccion = $data['tx_direccion'];
        $d->tx_referencia = $data['tx_referencia'];
        $d->tx_nombre = $data['tx_nombre'];
        $d->tx_codigo_postal = $data['tx_codigo_postal'];
        $d->tx_colonia = $data['tx_colonia'];
        $d->tx_numero_ext = $data['tx_numero_ext'];
        $d->tx_numero_int = $data['tx_numero_int'];
        $d->save();
        //Direccion::create($data);

        $direccion = DB::select('
                SELECT id AS id_direccion, tx_nombre AS direccion, tx_direccion AS denominacion, tx_referencia AS referencia, nu_latitud AS latitud, nu_longitud AS longitud, tx_colonia AS colonia, tx_codigo_postal AS codigo_postal, tx_numero_ext AS num_ext, tx_numero_int AS num_int
                FROM direccion 
                WHERE id = '.$data['id_direccion'].'
        ');

        return [
            'status' => true,
            'mensaje' => 'Se actualizo con exito',
            'data' => [
                'direccion' => $direccion
            ]
        ];
    }

    /**
     * Muestra una direccion relacionada al id suministrado
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $obj = Direccion::find($id);

        if ($obj == null) {
            return [
                'detalle' => false,
                'error' => 'No se consigio la direccion con el id suministrado'
            ];
        }

        return [
            'detalle' => true,
            'direccion' => $obj
        ];
    }

    /**
     * Muestra las direcciones de un usuario
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function direccionUsuario($id)
    {
        $obj = Direccion::where('id_usuario', '=', $id)
                        ->get();

        if ($obj->count() == 0) {
            return [
                'detalle' => false,
                'error' => 'No se consigieron direcciones con el id de usuario suministrado'
            ];
        }

        return [
            'detalle' => true,
            'direccion' => $obj
        ];
    }

    /**
     * Clientes por zona
     *
     * Recibe el id de la zona para filtar los usuarios que pertenezcan a dicha zona
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function clientesPorZona($id)
    {
        $obj = DB::table('direccion')
                    ->select(DB::raw('usuario.id as id_usuario, usuario.tx_nombre1 as nombre1, usuario.tx_apellido1 as apellido1, tipo_identificacion.tx_nombre as identificacion, usuario.nu_identificacion as identificacion, usuario.tx_nombre2 as nombre2, usuario.tx_apellido2 as apellido2, usuario.tx_sexo as sexo, usuario.dt_fecha_nacimiento as fecha_nacimiento, rol.tx_nombre as rol, usuario.tx_telefono_celular as telefono_celular, usuario.tx_telefono_local as telefono_local'))
                    ->join('usuario', 'direccion.id_usuario', '=', 'usuario.id')
                    ->join('tipo_identificacion','usuario.id_tipo_identificacion','=','tipo_identificacion.id')
                    ->join('rol','usuario.id_rol','=','rol.id')
                    ->where('direccion.id_zona', '=', $id)
                    ->where('usuario.id_rol', '=', 1)
                    ->get();

        if ($obj->count() == 0) {
            return [
                'detalle' => false,
                'error' => 'No se consigieron clientes con el id de zona suministrado'
            ];
        }

        return [
            'detalle' => true,
            'usuario' => $obj
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Modifica una direccion.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( $id)
    {
        $obj = Direccion::find($id);

        if ($obj == null) {
            return [
                'modificar' => false,
                'error' => 'No se consigio la direccion con el id suministrado'
            ];
        }

        $obj->save();

        return [
            'modificar' => true
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function direccionSinSucursal()
    {        
        $pedido = DB::select('
            SELECT concat_ws(\' \', u.tx_nombre1, u.tx_apellido1) AS "cliente", d.tx_direccion AS "direccion", d.tx_referencia AS "referencia"
            FROM direccion AS d 
            JOIN usuario AS u ON d.id_usuario = u.id             
            WHERE d.id_sucursal IS NULL
            ');
        
        return [
            "status" => true,
            "mensaje" => "Consulta exitosa",
            "data" => [
                "pedidos" => $pedido,
                "sucursal" => Sucursal::all()
            ]
        ];
    }

    public function asignarSucursalDireccion(Request $request) {
        $validator = Validator::make($request->all(), [
            'id_sucursal' => 'required',
            'id_direccion' => 'required'            
        ]);
        $data = $request->all();

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Los parametros enviados no son validos, por favor verifique',
                'errors' => $validator->errors()
            ];
        }
        
        if (is_array($data['id_direccion'])) {
            foreach ($data['id_direccion'] as $key => $value) {
                $pedido = Direccion::findOrFail($value);     
                if ($pedido != null) {
                    $pedido->id_sucursal = $data['id_sucursal'];
                }   
            }
            return [
                'status' => true,
                'mensaje' => 'Se asignaron las direcciones a la sucursal'
            ];
        }
        else {
            $pedido = Direccion::findOrFail($data['id_direccion']);
            if ($pedido != null) {
                $pedido->id_sucursal = $data['id_sucursal'];

                return [
                    'status' => true,
                    'mensaje' => 'Se asigno la dirección'
                ];
            }
            else {
                return [
                    'status' => false,
                    'mensaje' => 'La dirección enviada no existe'
                ];
            }    
        }
    }
}
