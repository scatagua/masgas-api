<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use App\Pedido;
use App\PedidoGrupo;
use App\Inventario;
use App\Monitoreo;

use Illuminate\Support\Facades\DB;

//require_once("../../../vendor/conekta/conekta-php/lib/Conekta.php");

class ApiController extends Controller
{
    public function pagar(Request $request) {
        $data = $request->all();

        \Conekta\Conekta::setApiKey('key_xsm1z4B8n8rkbss51P3jYQ');
        //\Conekta\Conekta::setApiKey('key_E8gSyVk5Anp6mfgjABynGg');
        \Conekta\Conekta::setApiVersion("2.0.0");
        \Conekta\Conekta::setLocale('es');

        try{
            $customer = \Conekta\Customer::create(
                array(
                    "name" => $data['card']['nombre'],
                    "email" => $data['card']['email'],
                    "phone" => $data['card']['telefono'],
                    "payment_sources" => array(
                        array(
                            "type" => "card",
                            "token_id" => $data['token']
                        )
                    )//payment_sources
                )//customer
            );

            $lista = [];

            if(is_array($data['items']))
            {
                // realizamos el ciclo
                while(list($key,$value) = each($data['items']))
                {
                    $lista[] = [
                        'name' => $value['tx_nombre'],
                        'unit_price' => floatval($value['nu_price_unit']),
                        'quantity' => intval($value['nu_cantidad'])
                    ];
                }
            }

            //dd($customer->id);

            $order = \Conekta\Order::create(
                array(
                  "line_items" => $lista, //line_items
                  "shipping_lines" => array(
                    array(
                      "amount" => 0,
                       "carrier" => "FEDEX"
                    )
                  ), //shipping_lines - physical goods only
                  "currency" => "MXN",
                  "customer_info" => array(
                    "customer_id" => $customer->id
                  ), //customer_info
                  "shipping_contact" => array(  
                    "address" => array(
                      "street1" => "Calle 123, int 2",
                      "postal_code" => "06100",
                      "country" => "MX"
                    )//address
                  ), //shipping_contact - required only for physical goods
                  "metadata" => array("reference" => "12987324097", "more_info" => "lalalalala"),
                  "charges" => array(
                      array(
                          "payment_method" => array(
                            "type" => "card",
                            "payment_source_id" => $customer->payment_sources[0]->id
                            //"token_id" => $data['token']
                          ) //payment_method - use customer's <code>default</code> - a card
                      ) //first charge
                  ) //charges
                )//order
            );

            $pedido = null;
            if ($order->payment_status == "paid") {
                $pedido = $this->storePedido($request);
            }
            else {
                $pedido = [
                    "status" => false,
                    "mensaje" => "Pago sin exito"
                ];
            }

            return [
                "status" => true,
                "mensaje" => "Pago efectuado con exito",
                "data" => [
                    //"customer" => $customer,
                    "order" => $order,
                    "pedido" => $pedido
                ]
            ];
        } catch (\Conekta\ProccessingError $error){
            return [
                "status" => false,
                "mensaje" => $error->getMessage()
            ];
        } catch (\Conekta\ParameterValidationError $error){
            return [
                "status" => false,
                "mensaje" => $error->getMessage()
            ];
        } catch (\Conekta\Handler $error){
            return [
                "status" => false,
                "mensaje" => $error->getMessage()
            ];
        }
    }

    public function storePedido(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_direccion' => 'required',
            'bo_pagada' => 'required',
            'items' => 'required',
            'departamento_venta' => 'required',
            'tipo_pago' => 'required'
        ]);
        $data = $request->all();


        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Los parametros enviados no son validos, por favor verifique',
                'errors' => $validator->errors()
            ];
        }

        $p = new PedidoGrupo();

        $pedido = new PedidoGrupo([
            'id_direccion' => $data['id_direccion'],
            'bo_pagada' => $data['bo_pagada'],
            'dt_fecha' => date('Y-m-d'),
            'tx_codigo' => $p->randomString(8),
            'tx_estatus' => 'Asignación',
            'id_distribuidor' => 0,
            'tx_vendido_en' => $data['departamento_venta'],
            'tx_tipo_pago' => $data['tipo_pago'],
            'dt_fecha_asignado' => null
        ]);
        $pedido->save();

        if(is_array($data['items']))
        {
            // realizamos el ciclo
            while(list($key,$value) = each($data['items']))
            {
                /// Guardamos el registro en la tabla relacional para categorias
                $obj = new Pedido([
                    'id_pedido_grupo' => $pedido->id,
                    'id_articulo' => $value['id_articulo'],
                    'nu_cantidad' => $value['nu_cantidad'],
                    'nu_subtotal' => $value['nu_subtotal']
                ]);
                $obj->save();
            }
        }

        $pedidos = DB::select('
            SELECT pg.id AS id_pedido_grupo, pg.dt_fecha AS fecha, pg.id_direccion AS id_direccion, pg.bo_pagada AS pagado, pg.tx_estatus AS estatus
            FROM pedido_grupo AS pg
            ORDER BY id DESC LIMIT 1
        ');

        $items = DB::select('
            SELECT p.id AS id_item, p.id_pedido_grupo AS id_pedido_grupo, p.id_articulo AS id_producto, p.nu_cantidad AS cantidad, p.nu_subtotal AS subtotal
            FROM pedido AS p
            JOIN pedido_grupo AS pg ON p.id_pedido_grupo = pg.id
            WHERE pg.id = '.$pedidos[0]->id_pedido_grupo.'
        ');

        return [
            "status" => true,
            "mensaje" => "Registro exitoso",
            'data' => [
                'pedidos' => $pedidos,
                'items' => $items
            ]
        ];
    }

    public function monitoreo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_usuario' => 'required',
            'nu_latitud' => 'required',
            'nu_longitud' => 'required',
            'id_vehiculo' => 'required'
        ]);
        $data = $request->all();

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Los parametros enviados no son validos, por favor verifique',
                'errors' => $validator->errors()
            ];
        }

        Monitoreo::create($data);

        return [
            "status" => true,
            "mensaje" => "Registro exitoso"
        ];
    }

    public function getMonitoreo()
    {
        $fecha = "";
        if (isset($_GET['fecha']) && isset($_GET['fecha2'])) {
            $fecha = "m.created_at BETWEEN '".$_GET['fecha']."' AND '".$_GET['fecha2']."'";
        }
        elseif (isset($_GET['fecha'])) {
            $fecha = "m.created_at = '".$_GET['fecha']."'";
        }

        $opt = "";
        if (isset($_GET['id_usuario'])) {
            $opt = "u.id = ".$_GET['id_usuario'];
        }
        elseif (isset($_GET['id_vehiculo'])) {
            $opt = "v.id = ".$_GET['id_vehiculo'];
        }

        $where = "";
        $concat = "";
        if (strlen($fecha) > 0 && strlen($opt) > 0) {
            $concat = "AND";
            $where = "WHERE";
        }
        elseif (strlen($fecha) > 0 || strlen($opt) > 0) {
            $where = "WHERE";
        }

        $items = DB::select('
            SELECT m.id AS id_monitoreo, m.nu_latitud AS latitud, m.nu_longitud AS longitud, u.tx_nombre1 AS nombre, u.tx_apellido1 AS apellido, u.id AS id_usuario, v.tx_marca AS marca, v.tx_modelo AS modelo, tv.tx_nombre AS tipo_vehiculo
            FROM monitoreo AS m
            JOIN usuario AS u ON m.id_usuario = u.id
            JOIN vehiculo AS v ON u.id = v.id_usuario
            JOIN tipo_vehiculo AS tv ON v.id_tipo_vehiculo = tv.id
           
        ');        

        return [
            "status" => true,
            "mensaje" => "Registro exitoso",
            "data" => [
                "monitoreo" => $items
            ]
        ];
    }
}
