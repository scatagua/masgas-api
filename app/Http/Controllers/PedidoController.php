<?php

namespace App\Http\Controllers;

use Validator;

use App\Pedido;
use App\PedidoGrupo;
use App\Inventario;

use App\Utilidad;

use App\Http\Requests\PedidoStoreRequest;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

/**
 * @resource Pedido
 *
 * Modulo de Pedido
 */
class PedidoController extends Controller
{



    /**
     * Muestra todos los pedidos
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Pedido::all();
    }

    /**
     * Crear nuevo pedido
     *
     * @param  PedidoStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_direccion' => 'required',
            'bo_pagada' => 'required',
            'items' => 'required',
            'departamento_venta' => 'required',
            'tipo_pago' => 'required'
        ]);
        $data = $request->all();
         //print_r($data);
        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Los parametros enviados no son validos, por favor verifique',
                'errors' => $validator->errors()
            ];
        }

        $p = new PedidoGrupo();

        $pedido = new PedidoGrupo([
            'id_direccion' => $data['id_direccion'],
            'bo_pagada' => $data['bo_pagada'],
            'dt_fecha' => date('Y-m-d'),
            //'tx_codigo' => $p->randomString(8),
			'tx_codigo' =>$data['codigo'],
            'tx_estatus' => 'Sin asignar',
            'id_distribuidor' => 0,
            'tx_vendido_en' => $data['departamento_venta'],
            'tx_tipo_pago' => $data['tipo_pago'],
            'dt_fecha_asignado' => null
        ]);
        $pedido->save();
        //dd($data['items']);
        if ($data['departamento_venta'] == 'App') {
             $json = Utilidad::is_json(json_encode($data['items']), TRUE);
        }else{
            $json = Utilidad::is_json($data['items'], TRUE);
        }
       
       
        if ($json) {
          
            //dd($json);
            if(is_array($json)) {
            
                // realizamos el ciclo
                foreach ($json as $key => $value) {
                    /// Guardamos el registro en la tabla relacional para categorias
                    $obj = new Pedido([
                        'id_pedido_grupo' => $pedido->id,
                        'id_articulo' => $value->id_articulo,
                        'nu_cantidad' => $value->nu_cantidad,
                        'nu_subtotal' => $value->nu_subtotal
                    ]);
                    $obj->save();
                }
            }
        }
        else {            
            if(is_array($data['items']))
            {
               
                // realizamos el ciclo
                foreach ($data['items'] as $key => $value)
                {
                    /// Guardamos el registro en la tabla relacional para categorias
                    $obj = new Pedido([
                        'id_pedido_grupo' => $pedido->id,
                        'id_articulo' => $value['id_articulo'],
                        'nu_cantidad' => $value['nu_cantidad'],
                        'nu_subtotal' => $value['nu_subtotal']
                    ]);
                    $obj->save();
                }
            }
        }        

        $pedidos = DB::select('
            SELECT pg.id AS id_pedido_grupo, pg.dt_fecha AS fecha, pg.id_direccion AS id_direccion, pg.bo_pagada AS pagado, pg.tx_estatus AS estatus
            FROM pedido_grupo AS pg
            ORDER BY id DESC LIMIT 1
        ');

        $items = DB::select('
            SELECT p.id AS id_item, p.id_pedido_grupo AS id_pedido_grupo, p.id_articulo AS id_producto, p.nu_cantidad AS cantidad, p.nu_subtotal AS subtotal
            FROM pedido AS p
            JOIN pedido_grupo AS pg ON p.id_pedido_grupo = pg.id
            WHERE pg.id = '.$pedidos[0]->id_pedido_grupo.'
        ');

        return [
        	"status" => true,
            "mensaje" => "Registro exitoso",
            'data' => [
                'pedidos' => $pedidos,
                'items' => $items
            ]
        ];
    }

    /**
     * Dashboard
     *
     * @param  PedidoStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function dashboard(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fecha_inicio' => 'required',
            'fecha_fin' => 'required'
        ]);
        $data = $request->all();

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Los parametros enviados no son validos, por favor verifique',
                'errors' => $validator->errors()
            ];
        }

         $pedidos1 = DB::select(' SELECT p.id_articulo as articulo, p.nu_cantidad as cantidad,  a.tx_nombre AS nombre
                    FROM pedido AS p 
                    JOIN pedido_grupo AS pg ON p.id_pedido_grupo = pg.id
                    JOIN articulo AS a ON p.id_articulo = a.id 
                    WHERE pg.tx_estatus =  \'En Ruta\'
                    AND (pg.dt_fecha >= \''.$data['fecha_inicio'].'\' AND pg.dt_fecha <= \''.$data['fecha_fin'].'\')
                     ');
      
         $sumaDistribucion = 0;
         foreach ( $pedidos1 as  $value) {
           switch ($value->articulo) {
               case 1:
                   $sumaDistribucion = $sumaDistribucion + ($value->cantidad * 4);
                   break;
                case 2:
                   $sumaDistribucion = $sumaDistribucion + ($value->cantidad * 10);
                   # code...
                   break;
                case 3:
                   $sumaDistribucion = $sumaDistribucion + ($value->cantidad * 20);
                   # code...
                   break;
                case 4:
                   $sumaDistribucion = $sumaDistribucion + ($value->cantidad * 30);
                   # code...
                   break;
                case 5:
                   $sumaDistribucion = $sumaDistribucion + ($value->cantidad * 45);
                   # code...
                   break;
                case 6:
                   $sumaDistribucion = $sumaDistribucion + $value->cantidad;
                   # code...
                   break;
           }
         }
         

         $pedidos2 = DB::select(' SELECT p.id_articulo as articulo, p.nu_cantidad as cantidad,  a.tx_nombre AS nombre
                    FROM pedido AS p 
                    JOIN pedido_grupo AS pg ON p.id_pedido_grupo = pg.id
                    JOIN articulo AS a ON p.id_articulo = a.id 
                    WHERE pg.tx_estatus =  \'Entregado\' 
                    AND (pg.dt_fecha >= \''.$data['fecha_inicio'].'\' AND pg.dt_fecha <= \''.$data['fecha_fin'].'\')
                     ');

         $sumaEntregado = 0;
         foreach ( $pedidos2 as  $value2) {
           switch ($value2->articulo) {
               case 1:
                   $sumaEntregado = $sumaEntregado + ($value2->cantidad * 4);
                   break;
                case 2:
                   $sumaEntregado = $sumaEntregado + ($value2->cantidad * 10);
                   # code...
                   break;
                case 3:
                   $sumaEntregado = $sumaEntregado + ($value2->cantidad * 20);
                   # code...
                   break;
                case 4:
                   $sumaEntregado = $sumaEntregado + ($value2->cantidad * 30);
                   # code...
                   break;
                case 5:
                   $sumaEntregado = $sumaEntregado + ($value2->cantidad * 45);
                   # code...
                   break;
                case 6:
                   $sumaEntregado = $sumaEntregado + $value2->cantidad;
                   # code...
                   break;
           }
         }

       

        $disponible = DB::select('
            SELECT sum(hi.nu_cantidad) AS \'Gas Disponible\'
            FROM historial_inventario AS hi ');

       


        $venta = DB::select('
            SELECT a.tx_nombre AS \'Producto\', 
                (
                    SELECT sum(p2.nu_cantidad) 
                    FROM pedido AS p2 
                    JOIN pedido_grupo AS pg2 ON p2.id_pedido_grupo = pg2.id
                    WHERE pg2.tx_estatus = \'Entregado\' AND pg2.bo_pagada = 1 AND p2.id_articulo = a.id 
                    AND (pg2.dt_fecha >= \''.$data['fecha_inicio'].'\' AND pg2.dt_fecha <= \''.$data['fecha_fin'].'\')
                ) AS \'Cantidad Vendida\',
                (
                    SELECT sum(p2.nu_subtotal) 
                    FROM pedido AS p2 
                    JOIN pedido_grupo AS pg2 ON p2.id_pedido_grupo = pg2.id
                    WHERE pg2.tx_estatus = \'Entregado\' AND pg2.bo_pagada = 1 AND p2.id_articulo = a.id 
                    AND (pg2.dt_fecha >= \''.$data['fecha_inicio'].'\' AND pg2.dt_fecha <= \''.$data['fecha_fin'].'\')
                ) AS \'Monto\'
            FROM articulo AS a
        ');

        $pendientePago = DB::select('
            SELECT sum(p2.nu_cantidad) AS \'Cantidad Vendida\', sum(p2.nu_subtotal) AS \'Monto\'
            FROM pedido AS p2 
            JOIN pedido_grupo AS pg2 ON p2.id_pedido_grupo = pg2.id
            WHERE pg2.bo_pagada = 0 
            AND (pg2.dt_fecha >= \''.$data['fecha_inicio'].'\' AND pg2.dt_fecha <= \''.$data['fecha_fin'].'\')
        ');
        $pagado = DB::select('
            SELECT sum(p2.nu_subtotal) AS \'Monto\'
            FROM pedido AS p2 
            JOIN pedido_grupo AS pg2 ON p2.id_pedido_grupo = pg2.id
            WHERE pg2.bo_pagada = 1 
            AND (pg2.dt_fecha >= \''.$data['fecha_inicio'].'\' AND pg2.dt_fecha <= \''.$data['fecha_fin'].'\')
        ');
        
        
        $proveedor = DB::select('
            SELECT p.tx_nombre AS \'Proveedor\', sum(hi.nu_solicitado) AS \'Cantidad Solicitada\', sum(hi.nu_cantidad) AS \'Cantidad Obtenida\', sum(hi.nu_merma) AS \'Cantidad Merma\', sum(hi.nu_monto) AS \'Monto\', sum(hi.nu_flete) AS \'Flete\'
            FROM historial_inventario AS hi
            JOIN proveedor AS p ON hi.id_proveedor = p.id
            WHERE (hi.dt_fecha >= \''.$data['fecha_inicio'].'\' AND hi.dt_fecha <= \''.$data['fecha_fin'].'\')
            GROUP BY p.id, p.tx_nombre
        ');

        $ventaDepartamento = DB::select('
            SELECT pg2.tx_vendido_en, sum(p2.nu_cantidad) AS \'Cantidad Vendida\', sum(p2.nu_subtotal) AS \'Monto\'
            FROM pedido AS p2 
            JOIN pedido_grupo AS pg2 ON p2.id_pedido_grupo = pg2.id
            WHERE (pg2.dt_fecha >= \''.$data['fecha_inicio'].'\' AND pg2.dt_fecha <= \''.$data['fecha_fin'].'\')
            GROUP BY pg2.tx_vendido_en
        ');

        return [
                "status" => true,
                "mensaje" => "Consulta exitosa",
                'data' => [
                    'inventario'  => $disponible,
                    'gas_entregado'  => $sumaEntregado,
                    'gas_distribucion'  => $sumaDistribucion,
                    'total_venta' => $sumaDistribucion + $sumaEntregado ,
                    'venta' => $venta,
                    'pago_pendiente' => $pendientePago,
                    'pago_pasarela' => $pagado,
                    'proveedor' => $proveedor,
                    'ventas_departamento' => $ventaDepartamento
                ]
        ];
    }

    /**
     * Dashboard
     *
     * @param  PedidoStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function compraInsumos(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_proveedor' => 'required',
            'id_articulo' => 'required',
            'id_usuario' => 'required',
            'numero_orden' => 'required',
            'nu_cantidad' => 'required',
            'nu_precio_unitario' => 'required',
            'nu_flete' => 'required',
            'nu_total' => 'required'
        ]);
        $data = $request->all();

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Los parametros enviados no son validos, por favor verifique',
                'errors' => $validator->errors()
            ];
        }

        $obj = new Inventario([
            'id_articulo' => $data['id_articulo'],
            'id_usuario' => $data['id_usuario'],
            'nu_solicitado' => $data['nu_cantidad'],
            'id_proveedor' => $data['id_proveedor'],   
            'nu_cantidad' => 0,
            'nu_merma' => 0,
            'nu_monto' => $data['nu_total'],
            'nu_flete' => $data['nu_flete'],
            'dt_fecha' => date('Y-m-d'),
            'nu_precio_unitario' => $data['nu_precio_unitario'],
            'tx_numero_orden' => $data['numero_orden']
        ]);
        
        $obj->save();
         
        return [
            "status" => true,
            "mensaje" => "Consulta exitosa"
        ];
    }

    /**
     * Dashboard
     *
     * @param  PedidoStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function recepcionInsumos(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_compra' => 'required',
            'tx_numero_orden' => 'required',
            'nu_cantidad' => 'required',
            'nu_merma' => 'required'
        ]);
        $data = $request->all();

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Los parametros enviados no son validos, por favor verifique',
                'errors' => $validator->errors()
            ];
        }

        $obj = Inventario::findOrFail($data['id_compra']);
        if ($obj == null) {
            $obj = Inventario::where('tx_numero_orden', $data['tx_numero_orden'])->get();
        }

        if ($obj != null) {
            if (is_array($obj) && count($obj) > 0) {
                $obj = $obj[0];
                $obj->nu_cantidad = $data['nu_cantidad'];
                $obj->nu_merma = $data['nu_merma'];
            }
            else {
                $obj->nu_cantidad = $data['nu_cantidad'];
                $obj->nu_merma = $data['nu_merma'];
            }
            $obj->save();
        }
        
        return [
            "status" => true,
            "mensaje" => "Consulta exitosa"
        ];
    }

    public function allBuyProvider(Request $request)
    {        
        
        if ($request->isMethod('post')) {
            $data = $request->all();

            $pedido = DB::select('
            SELECT prv.tx_nombre AS proveedor, prv.id AS id_proveedor, hi.dt_fecha AS fecha, concat_ws(\' \', u.tx_nombre1, u.tx_apellido1) AS usuario_solicitud, u.id AS id_usuario, a.tx_nombre AS articulo, hi.nu_cantidad AS nu_cantidad, hi.id AS id_compra, hi.nu_monto AS monto_total, hi.nu_precio_unitario AS precio_unitario, hi.nu_solicitado AS cantidad_pedida, hi.nu_merma AS merma, hi.nu_flete AS flete, hi.tx_numero_orden AS orden
            FROM historial_inventario AS hi
            JOIN proveedor AS prv ON hi.id_proveedor = prv.id 
            JOIN usuario AS u ON hi.id_usuario = u.id 
            JOIN articulo AS a ON hi.id_articulo = a.id 
            WHERE  hi.dt_fecha >= \''.$data['fecha_inicio'].'\' AND hi.dt_fecha <= \''.$data['fecha_fin'].'\'
            ');
              $v =2;
        }else{
            $pedido = DB::select('
            SELECT prv.tx_nombre AS proveedor, prv.id AS id_proveedor, hi.dt_fecha AS fecha, concat_ws(\' \', u.tx_nombre1, u.tx_apellido1) AS usuario_solicitud, u.id AS id_usuario, a.tx_nombre AS articulo, hi.nu_cantidad AS nu_cantidad, hi.id AS id_compra, hi.nu_monto AS monto_total, hi.nu_solicitado AS cantidad_pedida, hi.nu_merma AS merma, hi.nu_precio_unitario AS precio_unitario, hi.nu_flete AS flete, hi.tx_numero_orden AS orden
            FROM historial_inventario AS hi
            JOIN proveedor AS prv ON hi.id_proveedor = prv.id 
            JOIN usuario AS u ON hi.id_usuario = u.id 
            JOIN articulo AS a ON hi.id_articulo = a.id 
            ');
            $v =1;
        }
         
        
        return [
            "status" => true,
            "mensaje" => "Consulta exitosa".$v,
            "data" => [
                "pedido_proveedor" => $pedido
            ]
        ];
    }

    public function historial(Request $request)
    {
        $data = $request->all();

        $sql = '';
        
        isset($data['fecha_inicio']) ? 
            $fIni = 'pg2.dt_fecha >= \''.$data['fecha_inicio'].'\'' :
            $fIni = '';
        isset($data['fecha_fin']) ? 
            $fFin = 'pg2.dt_fecha <= \''.$data['fecha_fin'].'\'' :
            $fFin = '';
        
        if (strlen($fIni) > 0 && strlen($fFin) > 0) {
            $sql = $sql.'('.$fIni.' AND '.$fFin.')';
        }
        else {
            if (strlen($fIni) > 0) {
                $sql = $sql.$fIni;
            }
            else {
                $sql = $sql.$fFin;
            }            
        }
             
        isset($data['estatus']) ? 
            $estatus = 'pg2.tx_estatus = \''.$data['estatus'].'\'' :
            $estatus = '';

        strlen($estatus) > 0 && strlen($sql) > 0 ?
            $sql = $sql.' AND '.$estatus :
            $sql = $sql.$estatus;

        strlen($sql) > 0 ? 
            $sql = 'WHERE '.$sql :
            $sql = $sql; 

        //dd($sql);

        $pedido = DB::select('
            SELECT pg2.tx_vendido_en AS venta_lugar, a.tx_nombre AS articulo, p2.nu_cantidad AS cantidad, 
            p2.nu_subtotal AS subtotal, pg2.id AS id_grupo, d.id AS id_direccion, d.tx_direccion AS direccion, 
            u.tx_nombre1 AS nombre_cliente, u.tx_apellido1 AS apellido_cliente, u.id AS id_usuario, p2.id AS id_pedido_item
            FROM pedido AS p2 
            JOIN pedido_grupo AS pg2 ON p2.id_pedido_grupo = pg2.id 
            JOIN articulo AS a ON p2.id_articulo = a.id 
            JOIN direccion AS d ON pg2.id_direccion = d.id 
            JOIN usuario AS u ON d.id_usuario = u.id '.$sql);

        return [
            "status" => true,
            "mensaje" => "Solicitud completada",
            "data" => [
                "pedidos" => $pedido
            ]
        ];
    }

    public function pedidoAll()
    {        
        $pedido = DB::select('
            SELECT pg.id AS id_pedido, pg.dt_fecha AS fecha, concat_ws(\'\', \'CLT-\', u.id) AS cod_cliente, u.id AS id_cliente, u.tx_correo AS correo, 
                (
                    SELECT concat_ws(\' \', u2.tx_nombre1, u2.tx_apellido1) 
                    FROM usuario AS u2 
                    WHERE u2.id = pg.id_distribuidor
                ) AS operador,                 
                (
                    SELECT u2.id 
                    FROM usuario AS u2 
                    WHERE u2.id = pg.id_distribuidor
                ) AS id_operador,
                 pg.bo_pagada AS estatus_pago, pg.tx_estatus AS estatus_entrega, 
                (
                    SELECT tx_colonia
                    FROM ruta 
                    WHERE id_usuario = u.id
                    LIMIT 1
                ) AS ruta          
            FROM pedido_grupo AS pg            
            JOIN direccion AS d ON pg.id_direccion = d.id 
            JOIN usuario AS u ON d.id_usuario = u.id');

        return [
            "status" => true,
            "mensaje" => "Solicitud completada",
            "data" => [
                "pedidos" => $pedido
            ]
        ];
    }

    public function pedidoDetailsById($id)
    {        
        $pedido = DB::select('
            SELECT p.id_pedido_grupo AS id_pedido_grupo, a.tx_nombre AS articulo, a.id AS id_articulo, p.nu_cantidad AS cantidad, p.nu_subtotal AS subtotal, pg.tx_vendido_en AS departamento_venta, pg.dt_fecha_asignado AS fecha_asignado, pg.id_direccion AS id_direccion
            FROM pedido AS p   
            JOIN pedido_grupo AS pg ON pg.id = '.$id.'           
            JOIN articulo AS a ON p.id_articulo = a.id 
            WHERE p.id_pedido_grupo ='.$id);
        $id = $pedido[0]->id_direccion;
          $direccion = DB::select('
            SELECT * FROM direccion WHERE id = '.$id);
           
        return [
            "status" => true,
            "mensaje" => "Solicitud completada",
            "data" => [
                "pedidos" => $pedido,
                 "direccion" => $direccion
            ]
        ];
    }

    public function pedidoByEstatus(Request $request)
    {        
        $data = $request->all();

        $sql = '';
        isset($data['estatus']) ? 
            $sql = 'WHERE pg.tx_estatus = \''.$data['estatus'].'\'' : $sql = '';
            if ($data['estatus'] == "Sin asignar") {
               
                        $pedido = DB::select('
                        SELECT pg.id AS id_pedido, pg.dt_fecha AS fecha, concat_ws(\'CLT-\', u.id, \'\') AS cod_cliente, u.id AS id_cliente, u.tx_correo AS correo, pg.bo_pagada AS estatus_pago, pg.tx_estatus AS estatus_entrega                                   
                        FROM pedido_grupo AS pg            
                        JOIN direccion AS d ON pg.id_direccion = d.id 
                        JOIN usuario AS u ON d.id_usuario = u.id '.$sql.'
                         AND (pg.dt_fecha >= \''.$data['fecha_inicio'].'\' AND pg.dt_fecha <= \''.$data['fecha_fin'].'\')'
                    );

            }else{
                    $pedido = DB::select('
                        SELECT pg.id AS id_pedido, pg.dt_fecha AS fecha, concat_ws(\'CLT-\', u.id, \'\') AS cod_cliente, u.id AS id_cliente, u.tx_correo AS correo, concat_ws(\' \', u2.tx_nombre1, u2.tx_apellido1) AS operador, u2.id AS id_operador, pg.bo_pagada AS estatus_pago, pg.tx_estatus AS estatus_entrega, 
                            (
                                SELECT tx_colonia
                                FROM ruta 
                                WHERE id_usuario = u2.id
                                LIMIT 1
                            ) AS ruta          
                        FROM pedido_grupo AS pg            
                        JOIN direccion AS d ON pg.id_direccion = d.id 
                        JOIN usuario AS u ON d.id_usuario = u.id
                        JOIN usuario AS u2 ON pg.id_distribuidor = u2.id '.$sql.'
                         AND (pg.dt_fecha >= \''.$data['fecha_inicio'].'\' AND pg.dt_fecha <= \''.$data['fecha_fin'].'\')'
                     );
                }
        return [
            "status" => true,
            "mensaje" => "Solicitud completada",
            "data" => [
                "pedidos" => $pedido
            ]
        ];
    }

    public function getAllEstatus() {        
        $pedido = DB::select('
            SELECT pg.tx_estatus AS id_pedido
            FROM pedido_grupo AS pg 
            GROUP BY pg.tx_estatus');

        return [
            "status" => true,
            "mensaje" => "Solicitud completada",
            "data" => $pedido
        ];
    }

    public function pedidosEstatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_pedido' => 'required',
            'status' => 'required'
        ]);
        $data = $request->all();

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Los parametros enviados no son validos, por favor verifique',
                'errors' => $validator->errors()
            ];
        }

        $p = PedidoGrupo::findOrFail($data['id_pedido']);
        $p->tx_estatus = $data['status'];
            if ($data['status'] == "Entregado" ) {
                $p->bo_pagada = 1;     
            }
        $p->tx_motivo = isset($data['motivo']) ? $data['motivo'] : null;
        $p->save();

        return [
            "status" => true,
            "mensaje" => "Estatus cambiado",
            "data" => [
                'pedido' => $data['id_pedido']
            ]
        ];
    }

    /**
     * Mostrar un pedido especifico.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $obj = Pedido::find($id);

        if ($obj == null) {
            return [
                'detalle' => false,
                'error' => 'No se consigio el pedido con el id suministrado'
            ];
        }

        return [
            'detalle' => true,
            'pedido' => $obj
        ];
    }

    /**
     * Muestra los pedidos de un cliente
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pedidoCliente($id) 
    {
        $obj = DB::table('pedido')
                    ->select(DB::raw('usuario.tx_nombre1 as nombre1, usuario.tx_apellido1 as apellido1, direccion.tx_nombre as direccion, articulo.tx_nombre as articulo, pedido.nu_cantidad as cantidad, pedido.nu_subtotal as subtotal'))
                    ->join('pedido_grupo','pedido.id_pedido_grupo','=','pedido_grupo.id')
                    ->join('direccion','pedido_grupo.id_direccion','=','direccion.id')
                    ->join('usuario','direccion.id_usuario','=','usuario.id')
                    ->join('articulo','pedido.id_articulo','=','articulo.id')
                    ->where('usuario.id', '=', $id)
                    ->orderBy('pedido_grupo.id')
                    ->get();
    
        if (count($obj) == 0) {
            return [
                'detalle' => false,
                'error' => 'No se consiguieron pedidos con el id del cliente suministrado'
            ];
        }

        return [
            'detalle' => true,
            'pedido' => $obj
        ];
    }

    /**
     * Modificar un pedido.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( $id)
    {
        $obj = Pedido::find($id);

        if ($obj == null) {
            return [
                'modificar' => false,
                'error' => 'No se consigio el pedido con el id suministrado'
            ];
        }

        $obj->save();

        return [
            'modificar' => true
        ];
    }

    public function pedidoParaReplanificar()
    {        
        $pedido = DB::select('
            SELECT concat_ws(\' \', u.tx_nombre1, u.tx_apellido1) AS "cliente", d.tx_direccion AS "direccion", d.tx_referencia AS "referencia", pg.id AS id_pedido,pg.dt_fecha_asignado AS fecha_asignado , pg.dt_fecha AS fecha
            FROM pedido_grupo AS pg
            JOIN direccion AS d ON pg.id_direccion = d.id 
            JOIN usuario AS u ON d.id_usuario = u.id 
            WHERE pg.tx_estatus != \'Entregado\' OR pg.tx_estatus != \'En Ruta\'
            ');
        
        return [
            "status" => true,
            "mensaje" => "Consulta exitosa",
            "data" => [
                "pedidos" => $pedido
            ]
        ];
    }

    public function replanificarPedido(Request $request) {
        $validator = Validator::make($request->all(), [
            'id_pedido' => 'required',
            'id_distribuidor' => 'required',
            'fecha' => 'required'
        ]);
        $data = $request->all();

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Los parametros enviados no son validos, por favor verifique',
                'errors' => $validator->errors()
            ];
        }
        
        if (is_array($data['id_pedido']) && is_array($data['id_distribuidor'])) {
            foreach ($data['id_pedido'] as $key => $value) {
                $pedido = PedidoGrupo::find($value);     
                if ($pedido != null) {
                    $pedido->tx_estatus = "Asignación";
                    $pedido->id_distribuidor = $data['id_distribuidor'][$key];
                    $pedido->dt_fecha_asignado = $data['fecha'];
                    $pedido->save();
                }   
            }
            return [
                'status' => true,
                'mensaje' => 'Se asignaron todos los pedidos'
            ];
        }
        else {
            $pedido = PedidoGrupo::find($data['id_pedido']);
            if ($pedido != null) {

              $pedido->tx_estatus = "Asignación";
                $pedido->id_distribuidor = $data['id_distribuidor'];
                $pedido->dt_fecha_asignado = $data['fecha'];
                $pedido->save();
         
               
                return [
                    'status' => true,
                    'mensaje' => 'Se asigno el pedido'
                ];
            }
            else {
                return [
                    'status' => false,
                    'mensaje' => 'El pedido enviado no existe'
                ];
            }    
        }
    }

    public function pedidosNoEntregados()
    {        
        $pedido = DB::select('
            SELECT concat_ws(\' \', u.tx_nombre1, u.tx_apellido1) AS "cliente", d.tx_direccion AS "direccion", d.tx_referencia AS "referencia", pg.id AS id_pedido, pg.dt_fecha AS fecha, pg.tx_motivo AS "motivo"
            FROM pedido_grupo AS pg
            JOIN direccion AS d ON pg.id_direccion = d.id 
            JOIN usuario AS u ON d.id_usuario = u.id 
            WHERE pg.tx_estatus = \'Retraso\' OR pg.tx_estatus = \'Devolución\' 
            ');
        
        return [
            "status" => true,
            "mensaje" => "Consulta exitosa",
            "data" => [
                "pedidos" => $pedido
            ]
        ];
    }

    public function motivosNoEntregados()
    {        
        return [
            "status" => true,
            "mensaje" => "Consulta exitosa",
            "data" => [
                "motivos" => [
                    [
                        "estatus" => "Devolución",
                        "motivo" => "No se encontro al cliente"
                    ],
                    [
                        "estatus" => "Devolución",
                        "motivo" => "No se encontro el domicilio"
                    ],
                    [
                        "estatus" => "Devolución",
                        "motivo" => "No se pudo entrar al domicilio"
                    ],
                    [
                        "estatus" => "Devolución",
                        "motivo" => "Ya compro a otro proveedor"
                    ],
                    [
                        "estatus" => "Retraso",
                        "motivo" => "Motopipa choco"
                    ],
                    [
                        "estatus" => "Retraso",
                        "motivo" => "Retraso por tráfico"
                    ],
                    [
                        "estatus" => "Retraso",
                        "motivo" => "Accesos a la zona cerrado"
                    ],
                    [
                        "estatus" => "Retraso",
                        "motivo" => "Motopipa accidentada"
                    ],
                    [
                        "estatus" => "Retraso",
                        "motivo" => "Motopipa accidentada"
                    ],
                    [
                        "estatus" => "Retraso",
                        "motivo" => "Motopipa la robaron"
                    ]
                ]
            ]
        ];
    }

    public function canalesVenta()
    {        
        return [
            "status" => true,
            "mensaje" => "Consulta exitosa",
            "data" => [
                "canales" => [
                    [
                        "nombte" => "App"
                    ],
                    [
                        "nombte" => "Call Center"
                    ]
                ]
            ]
        ];
    }

    public function pedidosPorSucursal()
    {        
        $pedido = DB::select('
            SELECT concat_ws(\' \', u.tx_nombre1, u.tx_apellido1) AS "cliente", d.tx_direccion AS "direccion", d.tx_referencia AS "referencia", pg.id AS id_pedido, pg.dt_fecha AS fecha,
                CASE WHEN (
                    SELECT s.tx_nombre 
                    FROM sucursal AS s 
                    WHERE s.id = d.id_sucursal
                ) IS NULL 
                THEN \'No se ha asignado la dirección del cliente a ninguna sucursal\'  
                ELSE (
                        SELECT s.tx_nombre 
                        FROM sucursal AS s 
                        WHERE s.id = d.id_sucursal
                    ) 
                END AS "sucursal",
                (
                    SELECT s.id 
                    FROM sucursal AS s 
                    WHERE s.id = d.id_sucursal
                ) AS "id_sucursal"
            FROM pedido_grupo AS pg
            JOIN direccion AS d ON pg.id_direccion = d.id 
            JOIN usuario AS u ON d.id_usuario = u.id             
            ');
        
        return [
            "status" => true,
            "mensaje" => "Consulta exitosa",
            "data" => [
                "pedidos" => $pedido
            ]
        ];
    }

    public function pedidosPorCanal()
    {        
        $pedido = DB::select('
            SELECT concat_ws(\' \', u.tx_nombre1, u.tx_apellido1) AS "cliente", d.tx_direccion AS "direccion", d.tx_referencia AS "referencia", pg.id AS id_pedido, pg.dt_fecha AS fecha, pg.tx_vendido_en AS canal
            FROM pedido_grupo AS pg
            JOIN direccion AS d ON pg.id_direccion = d.id 
            JOIN usuario AS u ON d.id_usuario = u.id             
            ');
        
        return [
            "status" => true,
            "mensaje" => "Consulta exitosa",
            "data" => [
                "pedidos" => $pedido
            ]
        ];
    }
}
