<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Usuario;

use App\Http\Requests\LoginRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    
    /**
     * Inicio de Sesión
     * 
     * @param  LoginRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function login(LoginRequest $request) {
        // Creamos las reglas de validación
        $data = $request->all();

        $objects = DB::table('usuario')
                    ->select(DB::raw('usuario.id as id_usuario, usuario.tx_nombre1 as nombre1, usuario.tx_apellido1 as apellido1, tipo_identificacion.tx_nombre as identificacion, usuario.nu_identificacion as identificacion, usuario.tx_nombre2 as nombre2, usuario.tx_apellido2 as apellido2, usuario.tx_sexo as sexo, usuario.dt_fecha_nacimiento as fecha_nacimiento, rol.tx_nombre as rol, usuario.tx_telefono_celular as telefono_celular, usuario.tx_telefono_local as telefono_local'))
                    ->join('tipo_identificacion','usuario.id_tipo_identificacion','=','tipo_identificacion.id')
                    ->join('rol','usuario.id_rol','=','rol.id')
                    ->where('usuario.tx_alias', '=', $data['usuario'])
                    ->where('usuario.tx_password', '=', $data['password'])
                    ->get();

        if (count($objects) > 0) {
            $direcciones = DB::table('direccion')
                            ->select(DB::raw('direccion.id as id_direccion, direccion.tx_nombre as direccion, direccion.nu_latitud as latitud, direccion.nu_longitud as longitud'))
                            ->where('direccion.id_usuario', '=', $objects[0]->id_usuario)
                            ->get();

            return [
                'login' => true,
                'usuario'  => $objects,
                'direccion' => $direcciones
            ];       
        }

        return [
            'login' => false,
            'errors'  => 'Los datos no corresponden a ningun de nuestros usuarios'
        ];
    
        //User::create($request->all());
    }
}
