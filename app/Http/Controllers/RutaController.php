<?php

namespace App\Http\Controllers;

use App\VehiculoTipo;
use Validator;

use App\Ruta;
use App\RelRutaCliente;
use App\Sucursal;
use App\Vehiculo;
use App\Utilidad;
use App\Colonia;
use App\Usuario;
use App\RelRutaDistribuccion;

use App\Http\Requests\RutaStoreRequest;
use App\Http\Requests\RutaAsociarClienteRequest;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @resource Ruta
 *
 * Modulo de Ruta
 */
class RutaController extends Controller
{
    /**
     * Muestra todas las rutas
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = DB::select('
            SELECT rrd.id AS id_rel, rrd.id_ruta, r.alias, c.nombre AS colonia, c.id AS id_colonia, rrd.id_distribuidor, 
            (
              SELECT concat_ws(\' \', u.tx_nombre1, u.tx_apellido1)
              FROM usuario AS u 
              WHERE u.id = rrd.id_distribuidor
            ) AS "distribuidor"
            FROM rel_ruta_distribuccion AS rrd 
            JOIN ruta AS r ON rrd.id_ruta = r.id 
            JOIN colonia AS c ON rrd.id_colonia = c.id 
        ');

        return [
            'status' => true,
            'mensaje' => 'Consulta exitosa',
            'data' => [
                'rutas' => $clientes
            ]
        ];
    }

    /**
     * Crear nueva ruta
     *
     * @param  RutaStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            //'colonias' => 'required',
            'distribuidores' => 'required'            
        ]);
        $data = $request->all();

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Los parametros enviados no son validos, por favor verifique',
                'errors' => $validator->errors()
            ];
        }

        $ruta = new Ruta([
          "alias" => isset($data["alias"]) && $data["alias"] != '' ? 
            $data["alias"] : "001"
        ]);
        $ruta->save();

      //  $json = Utilidad::is_json($data['distribuidores'], TRUE);
        $json = explode(",", $data['distribuidores']); 
          
        if ($json) {
        
          /*if(is_array($json)) {*/
              // realizamos el ciclo
            
          
         
              foreach ($json as $key => $value) {                  
                 
                  $colonias = explode(",", $data['colonias']);
                  
                  foreach ($colonias as $colonia) {
                    $col = Colonia::where('nombre', $colonia)->get();

                    if ($col->count() > 0) {
                      $col = $col[0];
                    }
                    else {
                      $col = new Colonia([
                        "nombre" => $colonia
                      ]);
                      $col->save();
                    }

                    $rel = new RelRutaDistribuccion([
                      "id_ruta" => $ruta->id,
                      "id_colonia" => $col->id,
                      "id_distribuidor" => $value
                    ]);
                    $rel->save();
                  }
              }
         
        }
        else {            
          

          if(is_array($data['distribuidores'])) {
              // realizamos el ciclo
         

              foreach ($data['distribuidores'] as $key => $value) {                     
                  foreach ($value["colonias"] as $colonia) {
                    $col = Colonia::where('nombre', $colonia)->get();

                    if ($col->count() > 0) {
                      $col = $col[0];
                    }
                    else {
                      $col = new Colonia([
                        "nombre" => $colonia
                      ]);
                      $col->save();
                    }

                    $rel = new RelRutaDistribuccion([
                      "id_ruta" => $ruta->id,
                      "id_colonia" => $col->id,
                      "id_distribuidor" => $value
                    ]);
                    $rel->save();
                  }
              }
          }
        }        

        return [
        	"status" => true, 
          "mensaje" => "Registro exitoso"
        ];
    }

    public function getSucursal()
    {
        return [
            "status" => true,
            'mensaje' => 'Sucursal creada',
            "data" => Sucursal::all()
        ];
    }

    /**
     * Crear nueva ruta
     *
     * @param  RutaStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function storeSucursal(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tx_nombre' => 'required',
            'tx_numero' => 'required',
            'tx_colonia' => 'required',
            'tx_codigo_postal' => 'required'
        ]);
        $data = $request->all();

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Los parametros enviados no son validos, por favor verifique',
                'errors' => $validator->errors()
            ];
        }

        Sucursal::create($data);

        return [
            "crear" => true,
            'mensaje' => 'Sucursal creada'
        ];
    }

    /**
     * Crear nueva ruta
     *
     * @param  RutaStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function storeVehiculo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_tipo_vehiculo' => 'required',
            'tx_numero_identificacion' => 'required'
        ]);
        $data = $request->all();

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Los parametros enviados no son validos, por favor verifique',
                'errors' => $validator->errors()
            ];
        }

        Vehiculo::create($data);

        return [
            "crear" => true,
            'mensaje' => 'Vehiculo creado'
        ];
    }

    public function getAllVehiculos()
    {
        return Vehiculo::all();
    }

    public function storeTipoVehiculo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tx_nombre' => 'required',
        ]);
        $data = $request->all();

        if ($validator->fails()) {
            return [
                'status' => false,
                'mensaje' => 'Los parametros enviados no son validos, por favor verifique',
                'errors' => $validator->errors()
            ];
        }

        VehiculoTipo::create($data);

        return [
            "crear" => true,
            'mensaje' => 'Tipo vehiculo creado'
        ];
    }

    public function getAllTipoVehiculos()
    {
        return VehiculoTipo::all();
    }

    /**
     * Relacionar rutas a direcciones de cliente
     *
     * @param  RutaAsociarClienteRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function asociarClienteRuta(RutaAsociarClienteRequest $request)
    {
        $data = $request->validated();

        if (is_array($data['direccion_cliente'])) {
            while(list($key,$value) = each($data['direccion_cliente']))
            {
                $rel = RelRutaCliente::where('id_ruta', '=', $data['id_ruta'])
                                     ->where('id_direccion', '=', $value)
                                     ->get();

                if ($rel->count() == 0) {
                    $r = new RelRutaCliente([
                        'id_ruta' => $data['id_ruta'],
                        'id_direccion' => $value
                    ]);
                    $r->save();
                }

            }
        }

        return [
            "crear" => true
        ];
    }

    /**
     * Mostrar una ruta especifica.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $obj = Ruta::find($id);

        if ($obj == null) {
            return [
                'status' => false,
                'mensaje' => 'No se consigio la ruta con el id suministrado'
            ];
        }

        return [
            'status' => true,
            'mensaje' => 'Consulta exitosa',
            'data' => $obj
        ];
    }

    /**
     * Modificar una ruta.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $obj = Ruta::find($id);

        if ($obj == null) {
            return [
                'status' => false,
                'mensaje' => 'No se consigio la ruta con el id suministrado'
            ];
        }

        $obj->save();

        return [
            'status' => true,
            'mensaje' => ''
        ];
    }
}
