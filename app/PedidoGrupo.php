<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PedidoGrupo extends Model
{
    protected $table = "pedido_grupo";

    protected $fillable = ["id_direccion", "tx_codigo", "dt_fecha", "bo_pagada", "tx_estatus", "id_distribuidor", "dt_fecha_asignado", "tx_vendido_en", "tx_tipo_pago"];

    public $timestamps = false;

    public function randomString($length) {
	    $salt = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
	    
	    $rand = '';
	    $i = 0;
	    while ($i < $length) {
	        //Loop hasta que el string aleatorio contenga la longitud ingresada.
	        $num = rand() % strlen($salt);
	        $tmp = substr($salt, $num, 1);
	        $rand = $rand . $tmp;
	        $i++;
	    }
	    //Retorno del string aleatorio.
	    return $rand;
	}
}
