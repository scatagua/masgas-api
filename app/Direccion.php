<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{
    protected $table = "direccion";

    protected $fillable = ["tx_nombre", "id_usuario", "nu_latitud", "nu_longitud", "tx_direccion", "tx_colonia", "tx_calle", "tx_municipio", 'tx_referencia','tx_numero_int','tx_numero_ext','tx_codigo_postal',  'telefono'];

    public $timestamps = false;
}
