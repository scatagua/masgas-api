<?php
class Proveedor
{

    public function __construct()
    {

    }

    /**
     * @api {post} /proveedor Crear Proveedor
     * @apiVersion 0.1.0
     * @apiName PostProveedor
     * @apiGroup Proveedor
     *
     * @apiParam {String} nombre Nombre del proveedor.
     * @apiParam {String} numero_identificacion Numero de identificación (RFC).
     * @apiParam {String} direccion Dirección del proveedor.
     * @apiParam {String} telefono Telefono.
     * @apiParam {String} correo Correo.
     *
     * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
     * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        "status": true,
     *        "mensaje": "Registro exitoso"
     *     }
     *
     * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *		 "status": false,
     *       "mensaje": "Problemas para procesar solicitud"
     *       "errors": []
     *     }
     */
    public function storeProveedor()
    {

    }

    /**
     * @api {get} /proveedor Lista de Proveedores.
     * @apiVersion 0.1.0
     * @apiName GetProveedor
     * @apiGroup Proveedor
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        []
     *     }
     *
     * @apiError UserBadResponse Problema en solicitud
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *
     *     }
     */
    public function getAll()
    {

    }

    /**
     * @api {get} /hitorial_compra_proveedores Historial de compras a proveedores.
     * @apiVersion 0.1.0
     * @apiName GetProveedorHistorial
     * @apiGroup Proveedor
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        []
     *     }
     *
     * @apiError UserBadResponse Problema en solicitud
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *
     *     }
     */
    public function allBuyProvider()
    {

    }
}
