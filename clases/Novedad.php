<?php
class Novedad
{
 
 public function __construct()
 {
 
 }

 /**
 * @api {get} /novedades Listar novedades
 * @apiVersion 0.1.0
 * @apiName GetNovedades
 * @apiGroup Notificacion
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        []
 *     }
 *
 * @apiError UserBadResponse Problema en solicitud
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 
 *     }
 */
 public function index()
 {
 
 }

  /**
 * @api {post} /notificacion Registrar nueva notificación
 * @apiVersion 0.1.0
 * @apiName PostNotificacion
 * @apiGroup Notificacion
 * 
 * @apiParam {Number} id_usuario ID del usuario.
 * @apiParam {Number} id_mensaje ID del mensaje preestablecido.
 * @apiParam {String} tx_cuerpo Cuerpo del mensaje.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 *        "mensaje": "Registro exitoso"
 *     }
 *
 * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Problemas para procesar solicitud"
 *       "errors": []
 *     }
 */
 public function store()
 {
 
 }

   /**
 * @api {post} /notificacion/create Registrar mensaje de notificación
 * @apiVersion 0.1.0
 * @apiName PostNotificacionMensaje
 * @apiGroup Notificacion
 * 
 * @apiParam {Array} id_rol Lista de ID de rol a los cuales estara relacionado el mensaje.
 * @apiParam {Number} tx_mensaje Mensaje predefinido.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 *        "mensaje": "Registro exitoso"
 *     }
 *
 * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Problemas para procesar solicitud"
 *       "errors": []
 *     }
 */
 public function storeNotificacionMensaje()
 {
 
 }

  /**
 * @api {get} /notificacion_mensaje Mensajes para notificaciones
 * @apiVersion 0.1.0
 * @apiName GetMensajeNotificacion
 * @apiGroup Notificacion
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        []
 *     }
 *
 * @apiError UserBadResponse Problema en solicitud
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 
 *     }
 */
 public function getMensajeAll()
 {
 
 }

  /**
 * @api {get} /notificacion_mensaje_by_rol Mensajes para notificaciones por rol
 * @apiVersion 0.1.0
 * @apiName GetMensajeNotificacionRol
 * @apiGroup Notificacion
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        []
 *     }
 *
 * @apiError UserBadResponse Problema en solicitud
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 
 *     }
 */
 public function getMensajeRolAll()
 {
 
 }

    /**
     * @api {get} /mis_mensajes Mensajes de usuario
     * @apiVersion 0.1.0
     * @apiName GetMensajeUsuario
     * @apiGroup Notificacion
     *
     * @apiParam {Number} id_usuario ID de usuario.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        []
     *     }
     *
     * @apiError UserBadResponse Problema en solicitud
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *
     *     }
     */
    public function notificacionUsuario()
    {

    }

    /**
 * @api {get} /notificacion_by_rol/:id Novedades emitidas por rol
 * @apiVersion 0.1.0
 * @apiName GetNovedadesByIDRol
 * @apiGroup Notificacion
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        []
 *     }
 *
 * @apiError UserBadResponse Problema en solicitud
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       
 *     }
 */
 public function notificacionReportadasPor()
 {
 
 }
}
