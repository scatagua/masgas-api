<?php
class Pedido
{
 
 public function __construct()
 {
 
 }

   /**
 * @api {post} /pedido Crear pedido
 * @apiVersion 0.1.0
 * @apiName PostPedido
 * @apiGroup Pedido
 * 
 * @apiParam {Number} id_direccion ID de la direccion del usuario a donde sera enviado el pedido.
 * @apiParam {Boolean} bo_pagada Indica si la factura ha sido pagada.
 * @apiParam {String} departamento_venta Departamento de ventas (App, Callcenter, etc).
 * @apiParam {String} tipo_pago Tipo de pago (TDC, Cheque, Efectivo, etc).
 * @apiParam {Object} items Lista de items asociados al pedido, cada item debe indicar id_articulo, nu_cantidad y nu_subtotal este ultimo hace referencia al subtotal por el item en el momento en que se toma el pedido.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 *        "mensaje": "Registro exitoso"
 *     }
 *
 * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Problemas para procesar solicitud"
 *       "errors": []
 *     }
 */
 public function store()
 {
 
 }

   /**
 * @api {post} /payment Pago por medio de Conekta
 * @apiVersion 0.1.0
 * @apiName PostPedidoPago
 * @apiGroup Pedido
 * 
 * @apiParam {Object} card Datos del tarjetahabiente. 
 * @apiParam {String} token Token emitido por conekta para procesar el pago.
 * @apiParam {Number} id_direccion ID de la direccion del usuario a donde sera enviado el pedido.
 * @apiParam {Boolean} bo_pagada Indica si la factura ha sido pagada.
 * @apiParam {String} departamento_venta Departamento de ventas (App, Callcenter, etc).
 * @apiParam {String} tipo_pago Tipo de pago (TDC, Cheque, Efectivo, etc).
 * @apiParam {Object} items Lista de items asociados al pedido, cada item debe indicar id_articulo, nu_cantidad y nu_subtotal este ultimo hace referencia al subtotal por el item en el momento en que se toma el pedido.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 *        "mensaje": "Registro exitoso"
 *     }
 *
 * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Problemas para procesar solicitud"
 *       "errors": []
 *     }
 */
 public function pagar()
 {
 
 }

    /**
 * @api {post} /pedido/historial Historial de pedidos
 * @apiVersion 0.1.0
 * @apiName PostHistorialPedido
 * @apiGroup Pedido
 * 
 * @apiParam {String} fecha_ini Fecha de inicio (Opcional).
 * @apiParam {String} fecha_fin Fecha fin (Opcional).
 * @apiParam {String} estatus Filtrar por el estado del pedido (Opcional), Asignacion, Despacho, Entregado, etc..
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 * @apiSuccess {Object} data   Objeto que contiene la información de la solicitud.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 *        "mensaje": "Solicitud completada",
 *        "data": {
 *				"pedidos" : []
 * 		   }
 *     }
 *
 * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Problemas para procesar solicitud"
 *       "errors": []
 *     }
 */
 public function historial()
 {
 
 }

     /**
 * @api {post} /dashboard Dashboard
 * @apiVersion 0.1.0
 * @apiName PostDashboard
 * @apiGroup Dashboard
 * 
 * @apiParam {String} fecha_inicio Fecha de inicio.
 * @apiParam {String} fecha_fin Fecha fin.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 * @apiSuccess {Object} data   Objeto que contiene la información de la solicitud.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        {
 *		    "status": true, 
 *		    "mensaje": "Consulta exitosa",
 *		    "data": {
 *		        "inventario": [
 *		            {
 *		                "Gas Disponible": "10058",
 * 		                "Gas en Distribución": null
 * 		            }
 *		        ],
 *		        "venta": [
 *		            {
 *		                "Producto": "Cilindro 4 Lts",
 *		                "Cantidad Vendida": null,
 *		                "Monto": null
 *		            },
 *		            {
 *		                "Producto": "Cilindro 10 Lts",
 *		                "Cantidad Vendida": null,
 *		                "Monto": null
 *		            },
 * 		            {
 * 		                "Producto": "Cilindro 20 Lts",
 *		                "Cantidad Vendida": null,
 *		                "Monto": null
 *		            },
 *		            {
 *		                "Producto": "Cilindro 30 Lts",
 *		                "Cantidad Vendida": null,
 *		                "Monto": null
 *		            },
 *		            {
 *		                "Producto": "Cilindro 40 Lts",
 *		                "Cantidad Vendida": null,
 *		                "Monto": null
 *		            },
 *		            {
 *		                "Producto": "Pipa",
 *		                "Cantidad Vendida": null,
 *		                "Monto": null
 * 		            }
 *		        ],
 *		        "pago_pendiente": [
 *		            {
 *		                "Cantidad Vendida": null,
 * 		                "Monto": null
 *		            }
 *		        ],
 *		        "pago_pasarela": {
 *		            "cantidad": 0,
 *		            "monto": 0
 *		        },
 *		        "proveedor": [],
 *		        "ventas_departamento": []
 *		    }
 *		}
 *  }
 *
 * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Problemas para procesar solicitud"
 *       "errors": []
 *     }
 */
 public function dashboard()
 {
 
 }

      /**
 * @api {post} /compra_proveedor Compra Proveedor
 * @apiVersion 0.1.0
 * @apiName PostCompraProveedor
 * @apiGroup Pedido
 * 
 * @apiParam {Number} id_proveedor ID de proveedor.
 * @apiParam {Number} id_usuario ID de usuario que registra la compra.
 * @apiParam {String} numero_orden Numero de orden de compra.
 * @apiParam {Number} nu_cantidad Cantidad solicitada.
 * @apiParam {Number} nu_precio_unitario Precio unitario del producto.
 * @apiParam {Number} nu_flete Costo del flete.
 * @apiParam {Number} nu_total Costo total.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        {
 *		    "status": true, 
 *		    "mensaje": "Consulta exitosa"
 *		  }
 *     }
 *
 * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Problemas para procesar solicitud"
 *       "errors": []
 *     }
 */
 public function compraInsumos()
 {
 
 }

      /**
 * @api {post} /recepcion_compra Recepción de compra a Proveedor
 * @apiVersion 0.1.0
 * @apiName PostRecepcionCompraProveedor
 * @apiGroup Pedido
 * 
 * @apiParam {Number} id_compra ID de la compra.
 * @apiParam {String} tx_numero_orden Numero de orden de compra.
 * @apiParam {Number} nu_recibido Cantidad recibida.
 * @apiParam {Number} nu_merma Merma entre lo solicitado y lo recibido.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        {
 *		    "status": true, 
 *		    "mensaje": "Consulta exitosa"
 *		  }
 *     }
 *
 * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Problemas para procesar solicitud"
 *       "errors": []
 *     }
 */
 public function recepcionInsumos()
 {
 
 }

 /**
 * @api {post} /pedidos_status Cambiar estatus del pedido
 * @apiVersion 0.1.0
 * @apiName PostUpdateStatusPedido
 * @apiGroup Pedido
 * 
 * @apiParam {Number} id_pedido ID del pedido.
 * @apiParam {String} status Estatus a asigar (Por entregar, Entregado, En ruta, etc).
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        {
 *		    "status": true, 
 *		    "mensaje": "Consulta exitosa"
 *		  }
 *     }
 *
 * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Problemas para procesar solicitud"
 *       "errors": []
 *     }
 */
 public function pedidosEstatus()
 {
 
 }

  /**
 * @api {get} /pedido/cliente/:id Obtener pedidos por cliente
 * @apiVersion 0.1.0
 * @apiName GetPedidoCliente
 * @apiGroup Pedido
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        []
 *     }
 *
 * @apiError UserBadResponse Problema en solicitud
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 
 *     }
 */
 public function pedidoCliente()
 {
 
 }

 /**
 * @api {get} /pedido_all Obtener todos los pedidos efectuados
 * @apiVersion 0.1.0
 * @apiName GetPedidoAll
 * @apiGroup Pedido
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        []
 *     }
 *
 * @apiError UserBadResponse Problema en solicitud
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 
 *     }
 */
 public function pedidoAll()
 {
 
 }

 /**
 * @api {get} /all_status_pedidos Obtener todos los status para pedidos
 * @apiVersion 0.1.0
 * @apiName GetPedidoAllStatus
 * @apiGroup Pedido
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        []
 *     }
 *
 * @apiError UserBadResponse Problema en solicitud
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 
 *     }
 */
 public function pedidoAllStatus()
 {
 
 }

 /**
 * @api {get} /pedido_details/:id Detalle del pedido
 * @apiVersion 0.1.0
 * @apiName GetPedidoDetalleByID
 * @apiGroup Pedido
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        []
 *     }
 *
 * @apiError UserBadResponse Problema en solicitud
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 
 *     }
 */
 public function pedidoDetailsById()
 {
 
 }

 /**
 * @api {post} /pedidos_by_status Obtener pedidos por estatus
 * @apiVersion 0.1.0
 * @apiName PostStatusPedidoAll
 * @apiGroup Pedido
 * 
 * @apiParam {String} estatus Estatus a filtrar (Por entregar, Entregado, En ruta, Asignación, etc).
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        {
 *		    "status": true, 
 *		    "mensaje": "Consulta exitosa"
 *		  }
 *     }
 *
 * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Problemas para procesar solicitud"
 *       "errors": []
 *     }
 */
 public function pedidoByEstatus()
 {
 
 }

 /**
 * @api {get} /pedido_planificacion Pedidos por asignar o replanificar
 * @apiVersion 0.1.0
 * @apiName GetPedidoPlanificacion
 * @apiGroup Pedido
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        []
 *     }
 *
 * @apiError UserBadResponse Problema en solicitud
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 
 *     }
 */
 public function pedidoParaReplanificar()
 {
 
 }

 /**
 * @api {post} /pedido_planificacion Asignar nueva fecha de entrega
 * @apiVersion 0.1.0
 * @apiName PostPedidoPlanificacion
 * @apiGroup Pedido
 * @apiDescription Puede recibir un array de pedidos para replanificar o bien uno en particular
 * 
 * @apiParam {String} fecha Fecha en la cual sera enviado el pedido al cliente (Si se envia una lista de pedidos, se sugiere que sean despachados en la misma fecha)
 * @apiParam {Object} id_pedido Puede enviarse un array con todos los pedidos que se deseen asignar o replanificar para la misma fecha (Tambien se puede enviar un objeto).
 * @apiParam {Object} id_distribuidor Si el id_pedido es una array, este parametro debe ser un array con la misma longitud.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        {
 *		    "status": true, 
 *		    "mensaje": "Consulta exitosa"
 *		  }
 *     }
 *
 * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Problemas para procesar solicitud"
 *       "errors": []
 *     }
 */
 public function replanificarPedido()
 {
 
 }

 /**
 * @api {get} /pedido_no_entregado Pedidos reportados con retrasos o devueltos
 * @apiVersion 0.1.0
 * @apiName GetPedidoNoEntregados
 * @apiGroup Pedido
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        []
 *     }
 *
 * @apiError UserBadResponse Problema en solicitud
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 
 *     }
 */
 public function pedidosNoEntregados()
 {
 
 }


 /**
 * @api {get} /motivos_no_entrega Motivos de no entrega
 * @apiVersion 0.1.0
 * @apiName GetMotivosNoEntrega
 * @apiGroup Pedido
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        []
 *     }
 *
 * @apiError UserBadResponse Problema en solicitud
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 
 *     }
 */
 public function motivosNoEntregados()
 {
 
 }

 /**
 * @api {get} /canales_venta Canales de venta
 * @apiVersion 0.1.0
 * @apiName GetPedidoCanales
 * @apiGroup Pedido
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        []
 *     }
 *
 * @apiError UserBadResponse Problema en solicitud
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 
 *     }
 */
 public function canalesVenta()
 {
 
 }

  /**
 * @api {get} /pedido_por_sucursal Pedidos por sucursal
 * @apiVersion 0.1.0
 * @apiName GetPedidoBySucursal
 * @apiGroup Pedido
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        []
 *     }
 *
 * @apiError UserBadResponse Problema en solicitud
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 
 *     }
 */
 public function pedidoBySucursal()
 {
 
 }

  /**
 * @api {get} /pedido_por_canal Pedidos por canal
 * @apiVersion 0.1.0
 * @apiName GetPedidoBySucursal
 * @apiGroup Pedido
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        []
 *     }
 *
 * @apiError UserBadResponse Problema en solicitud
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 
 *     }
 */
 public function pedidoByCanal()
 {
 
 }

}
