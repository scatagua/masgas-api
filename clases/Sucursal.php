<?php
class Sucursal
{
 
 public function __construct()
 {
 
 }

   /**
 * @api {post} /sucursal/create Crear Sucursal
 * @apiVersion 0.1.0
 * @apiName PostSucursal
 * @apiGroup Sucursal
 * 
 * @apiParam {String} tx_nombre Nombre de la sucursal.
 * @apiParam {String} tx_numero Numero o codigo de la sucursal.
 * @apiParam {String} tx_colonia Colonia a la que se asocia.
 * @apiParam {String} tx_codigo_postal Codigo postal.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 *        "mensaje": "Registro exitoso"
 *     }
 *
 * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Problemas para procesar solicitud"
 *       "errors": []
 *     }
 */
 public function storeSucursal()
 {
 
 }

    /**
     * @api {post} /sucursal Obtener Sucursales
     * @apiVersion 0.1.0
     * @apiName GetSucursal
     * @apiGroup Sucursal
     *
     * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
     * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        "status": true,
     *        "mensaje": "Registro exitoso"
     *     }
     *
     * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *		 "status": false,
     *       "mensaje": "Problemas para procesar solicitud"
     *       "errors": []
     *     }
     */
    public function getSucursal()
    {

    }
}
