<?php
class Ruta
{

    public function __construct()
    {

    }

    /**
     * @api {get} /rutas Listar rutas
     * @apiVersion 0.1.0
     * @apiName GetRutas
     * @apiGroup Rutas
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        []
     *     }
     *
     * @apiError UserBadResponse Problema en solicitud
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *
     *     }
     */
    public function getRutasAll()
    {

    }

    /**
     * @api {post} /ruta Crear ruta
     * @apiVersion 0.1.0
     * @apiName PostRutas
     * @apiGroup Rutas
     * @apiDescription Crea una ruta por medio del alias enviado o usando uno por defecto, recorre cada elemento del array de distribuidores el mismo debe contener un objeto { id_distribuidor: Integer, colonias: Array<String> }
     *
     * @apiParam {Number} alias Nombre de la ruta (Opcional).
     * @apiParam {Array} colonias Lista de nombres de las colonias (Debe ser tipo String cada elemento del array).
     * @apiParam {Array} distribuidores Lista de id de distribuidores (Debe ser tipo Integer cada elemento del array).
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       []
     *     }
     *
     * @apiError UserBadResponse Problema en solicitud
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *
     *     }
     */
    public function store()
    {

    }

    /**
     * @api {post} /ruta/:id Modificar ruta
     * @apiVersion 0.1.0
     * @apiName PostRutasUpdate
     * @apiGroup Rutas
     *
     * @apiParam {Number} id_usuario ID del usuario (Opcional).
     * @apiParam {String} tx_codigo Codigo de la ruta (Opcional).
     * @apiParam {String} tx_colonia Colonia (Opcional).
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       []
     *     }
     *
     * @apiError UserBadResponse Problema en solicitud
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *
     *     }
     */
    public function update()
    {

    }

    /**
     * @api {get} /ruta/:id Obtener la ruta por ID
     * @apiVersion 0.1.0
     * @apiName GetRutasID
     * @apiGroup Rutas
     *
     * @apiParam {Number} id_usuario ID del usuario.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       []
     *     }
     *
     * @apiError UserBadResponse Problema en solicitud
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *
     *     }
     */
    public function show()
    {

    }


}
