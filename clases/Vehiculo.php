<?php
class Vehiculo
{
 
 public function __construct()
 {
 
 }

   /**
 * @api {post} /vehiculo/create Crear Vehiculo
 * @apiVersion 0.1.0
 * @apiName PostVehiculo
 * @apiGroup Vehiculo
 * 
 * @apiParam {Number} id_tipo_vehiculo ID del tipo de vehiculo.
 * @apiParam {String} tx_numero_identificacion Numero de identificación o placa.
 * @apiParam {Number} id_usuario ID Usuario (Opcional).
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 *        "mensaje": "Registro exitoso"
 *     }
 *
 * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Problemas para procesar solicitud"
 *       "errors": []
 *     }
 */
 public function storeVehiculo()
 {
 
 }

 /**
 * @api {post} /monitoreo Registrar nueva posición
 * @apiVersion 0.1.0
 * @apiName PostMonitoreo
 * @apiGroup Vehiculo
 * 
 * @apiParam {Number} id_usuario ID del usuario.
 * @apiParam {Number} nu_latitud Latitud de la posición registrada.
 * @apiParam {Number} nu_longitud Longitud de la posición registrada.
 * @apiParam {Number} id_vehiculo ID del vehiculo.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 *        "mensaje": "Registro exitoso"
 *     }
 *
 * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Problemas para procesar solicitud"
 *       "errors": []
 *     }
 */
 public function monitoreo()
 {
 
 }


 /**
 * @api {get} /monitoreo Obtener el monitoreo
 * @apiVersion 0.1.0
 * @apiName GetMonitoreo
 * @apiGroup Vehiculo
 *
 * @apiParam {Number} id_usuario (Opcional) ID del usuario poseedor del vehiculo.
 * @apiParam {String} fecha (Opcional) Fecha inicial desde donde quieres obtener el monitoreo.
 * @apiParam {String} fecha2 Fecha final hasta donde quieres obtener la ruta.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        []
 *     }
 *
 * @apiError UserBadResponse Problema en solicitud
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 
 *     }
 */
 public function getMonitoreo()
 {
 
 }

    /**
     * @api {get} /vehiculos Lista de vehiculos.
     * @apiVersion 0.1.0
     * @apiName GetVehiculos
     * @apiGroup Vehiculo
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        []
     *     }
     *
     * @apiError UserBadResponse Problema en solicitud
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *
     *     }
     */
    public function getAllVehiculos()
    {

    }

    /**
     * @api {post} /tipo_vehiculo Crear Tipo de Vehiculo
     * @apiVersion 0.1.0
     * @apiName PostTipoVehiculo
     * @apiGroup Vehiculo
     *
     * @apiParam {String} tx_nombre Nombre o denominacion del tipo.
     *
     * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
     * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        "status": true,
     *        "mensaje": "Registro exitoso"
     *     }
     *
     * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *		 "status": false,
     *       "mensaje": "Problemas para procesar solicitud"
     *       "errors": []
     *     }
     */
    public function storetTipoVehiculo()
    {

    }

    /**
     * @api {get} /tipo_vehiculo Lista de tipos de vehiculos.
     * @apiVersion 0.1.0
     * @apiName GeTipoVehiculos
     * @apiGroup Vehiculo
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        []
     *     }
     *
     * @apiError UserBadResponse Problema en solicitud
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *
     *     }
     */
    public function getAllTipoVehiculos()
    {

    }
}
