<?php
class Rol
{

    public function __construct()
    {

    }

    /**
     * @api {get} /rol Listar de roles
     * @apiVersion 0.1.0
     * @apiName GetRol
     * @apiGroup Rol
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        []
     *     }
     *
     * @apiError UserBadResponse Problema en solicitud
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *
     *     }
     */
    public function getRolAll()
    {

    }

    /**
     * @api {get} /usuario/rol/:id Listar usuarios por rol
     * @apiVersion 0.1.0
     * @apiName GetUsuarioRol
     * @apiGroup Rol
     *
     * @apiParam {Number} id ID del rol.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       []
     *     }
     *
     * @apiError UserBadResponse Problema en solicitud
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *
     *     }
     */
    public function getUsuarioByRol()
    {

    }

}
