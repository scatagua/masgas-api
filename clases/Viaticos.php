<?php
class Viaticos
{

    public function __construct()
    {

    }

    /**
     * @api {post} /viaticos Cargar viaticos
     * @apiVersion 0.1.0
     * @apiName PostViaticos
     * @apiGroup Viaticos
     *
     * @apiParam {Number} id_usuario ID de usuario.
     * @apiParam {Number} monto Monto de la factura o del consumo.
     * @apiParam {String} fecha Fecha de la factura o del consumo.
     * @apiParam {String} factura Codigo de la factura (Opcional en caso de que se posea).
     *
     * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
     * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        "status": true,
     *        "mensaje": "Registro exitoso"
     *     }
     *
     * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *		 "status": false,
     *       "mensaje": "Problemas para procesar solicitud"
     *       "errors": []
     *     }
     */
    public function storeProveedor()
    {

    }

    /**
     * @api {get} /viaticos Viaticos cargados.
     * @apiVersion 0.1.0
     * @apiName GetViaticos
     * @apiGroup Viaticos
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        []
     *     }
     *
     * @apiError UserBadResponse Problema en solicitud
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *
     *     }
     */
    public function getAll()
    {

    }
}
