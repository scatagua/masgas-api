<?php
class Articulo
{
 
 public function __construct()
 {
 
 }

 /**
 * @api {get} /articulo Listar articulos
 * @apiVersion 0.1.0
 * @apiName GetArticulos
 * @apiGroup Articulo
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        [{"id":1,"tx_nombre":"Bombona 10 Kg","nu_precio":5000,"nu_cantidad":10},{"id":2,"tx_nombre":"Bombona 18 Kg","nu_precio":8000,"nu_cantidad":15},{"id":3,"tx_nombre":"Bombona 30 Kg","nu_precio":12000,"nu_cantidad":8}]
 *     }
 *
 * @apiError UserBadResponse Problema en solicitud
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 
 *     }
 */
 public function index()
 {
 
 }

  /**
 * @api {get} /articulo/categoria/:id Listar articulos por categoria
 * @apiVersion 0.1.0
 * @apiName GetArticulosCategoria
 * @apiGroup Articulo
 *
 * @apiParam {Number} id ID de la categoria del producto.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       [{"id_articulo":1,"articulo":"Cilindro 5 Kg","precio":5000,"cantidad":"10","id_articulo_categoria":1},{"id_articulo":2,"articulo":"Cilindro 10 Kg","precio":8000,"cantidad":"15","id_articulo_categoria":1},{"id_articulo":3,"articulo":"Bombona 20 Kg","precio":12000,"cantidad":"8","id_articulo_categoria":1},{"id_articulo":4,"articulo":"Bombona 30 Kg","precio":18000,"cantidad":"10","id_articulo_categoria":1},{"id_articulo":5,"articulo":"Bombona 40 Kg","precio":25000,"cantidad":"15","id_articulo_categoria":1}]
 *     }
 *
 * @apiError UserBadResponse Problema en solicitud
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 
 *     }
 */
 public function getArticulosCategoria()
 {
 
 }

     /**
 * @api {put} /articulo/precio/:id Actualizar precio de articulo
 * @apiVersion 0.1.0
 * @apiName PutArticuloPrecio
 * @apiGroup Articulo
 * 
 * @apiParam {Number} id_articulo ID del articulo.
 * @apiParam {Number} precio Precio del articulo.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 *        "mensaje": "Solicitud completada"
 *     }
 *
 * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Problemas para procesar solicitud"
 *       "errors": []
 *     }
 */
 public function actualizarPrecio()
 {
 
 }

  /**
 * @api {post} /articulo/precio Crear articulo
 * @apiVersion 0.1.0
 * @apiName PostArticuloPrecio
 * @apiGroup Articulo
 * 
 * @apiParam {Object} id_articulo Lista de id de los articulos a actualizar su precio.
 * @apiParam {Object} precio Nuevo precio de los articulos, la posicion del articulo en la lista debe corresponder con la del precio, para no generar errores.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 *        "mensaje": "Solicitud completada"
 *     }
 *
 * @apiError UserBadResponse Problemas para procesar solictud
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Problemas para procesar solicitud"
 *       "errors": []
 *     }
 */
 public function store()
 {
 
 }

 /**
 * @api {post} /articulo/precio Actualizar precio por lote de articulos
 * @apiVersion 0.1.0
 * @apiName PostArticuloPrecio
 * @apiGroup Articulo
 * 
 * @apiParam {Object} id_articulo Lista de id de los articulos a actualizar su precio.
 * @apiParam {Object} precio Nuevo precio de los articulos, la posicion del articulo en la lista debe corresponder con la del precio, para no generar errores.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 *        "mensaje": "Solicitud completada"
 *     }
 *
 * @apiError UserBadResponse Problemas para procesar solictud
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Problemas para procesar solicitud"
 *       "errors": []
 *     }
 */
 public function actualizarPrecioLote()
 {
 
 }

  /**
 * @api {post} /articulo/inventario Actualizar inventario por lote de articulos
 * @apiVersion 0.1.0
 * @apiName PostArticuloInventario
 * @apiGroup Articulo
 * 
 * @apiParam {Object} id_articulo Lista de id de los articulos a actualizar su precio.
 * @apiParam {Object} cantidad Nueva cantidad de los articulos, la posicion del articulo en la lista debe corresponder con la de la cantidad, para no generar errores.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 *        "mensaje": "Solicitud completada"
 *     }
 *
 * @apiError UserBadResponse Problemas para procesar solictud
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Problemas para procesar solicitud"
 *       "errors": []
 *     }
 */
 public function actualizarInventarioLote()
 {
 
 }

}
