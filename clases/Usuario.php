<?php
class Usuario
{
 
 public function __construct()
 {
 
 }

 /**
 * @api {post} /login Iniciar Sesión
 * @apiVersion 0.1.0
 * @apiName PostLogin
 * @apiGroup Usuario
 * 
 * @apiParam {String} tx_correo Correo del usuario.
 * @apiParam {String} tx_password El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el registro fue exitoso o no respectivamente.
 * @apiSuccess {Object} data   Objeto que contiene los datos de usuario, dirección y articulos.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 *        "data": {
 *			"usuario": [], 
 *			"direccion": [],
 *			"articulos": []
 *		  }
 *     }
 *
 * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Las credenciales no corresponden a ninguno de nuestros usuarios"
 *     }
 */
 public function login()
 {
 
 }

 /**
 * @api {post} /cliente Registrar Cliente Fisico
 * @apiVersion 0.1.0
 * @apiName PostCreateClienteFisico
 * @apiGroup Usuario
 * 
 * @apiParam {String} correo Correo del usuario.
 * @apiParam {String} nombre1 Primer nombre.
 * @apiParam {String} nombre2 Segundo nombre (Opcional).
 * @apiParam {String} apellido1 Primer apellido.
 * @apiParam {String} apellido2 Segundo apellido (Opcional).
 * @apiParam {String} telefono_local Telefono local.
 * @apiParam {String} telefono_celular Telefono celular.
 * @apiParam {String} fecha_nacimiento Fecha de nacimiento (YYYY-MM-DD).
 * @apiParam {String} sexo Sexo (Masculino o Femenino).
 * @apiParam {Number} tipo_identificacion ID tipo de identificación (1 por default).
 * @apiParam {Number} identificacion Numero de documento de identidad.
 * @apiParam {String} direccion Dirección de envio.
 * @apiParam {Number} direccion_longitud Longitud de la dirección.
 * @apiParam {Number} direccion_latitud Latitud de la dirección.
 * @apiParam {String} codigo_postal Codigo postal.
 * @apiParam {String} colonia Colonia.
 * @apiParam {String} numero_ext Numero exterior (Opcional).
 * @apiParam {String} numero_int Numero interior (Opcional).
 * @apiParam {String} password El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 * @apiSuccess {Object} data   Objeto que contiene la información de la solicitud.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 * 		  "mensaje": "Se creo con exito el cliente",
 *        "data": {
 *			"codigo": "PRUEBA123" 
 *		  }
 *     }
 *
 * @apiError UserBadResponse Registro erroneo
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Registro erroneo",
 *       "errors": []
 *     }
 */
 public function storeCliente()
 {
 
 }

  /**
 * @api {post} /cliente_moral Registrar Cliente moral
 * @apiVersion 0.1.0
 * @apiName PostCreateClienteMoral
 * @apiGroup Usuario
 * 
 * @apiParam {String} correo Correo de la empresa.
 * @apiParam {String} empresa Nombre de la empresa.
 * @apiParam {String} telefono_local Telefono local.
 * @apiParam {String} telefono_celular Telefono celular.
 * @apiParam {Number} identificacion Numero de documento de identidad.
 * @apiParam {String} direccion Dirección de envio.
 * @apiParam {Number} direccion_longitud Longitud de la dirección.
 * @apiParam {Number} direccion_latitud Latitud de la dirección.
 * @apiParam {String} codigo_postal Codigo postal.
 * @apiParam {String} colonia Colonia.
 * @apiParam {String} numero_ext Numero exterior (Opcional).
 * @apiParam {String} numero_int Numero interior (Opcional).
 * @apiParam {String} password El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 * @apiSuccess {Object} data   Objeto que contiene la información de la solicitud.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 * 		  "mensaje": "Se creo con exito el cliente",
 *        "data": {
 *			"codigo": "PRUEBA123" 
 *		  }
 *     }
 *
 * @apiError UserBadResponse Registro erroneo
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Registro erroneo",
 *       "errors": []
 *     }
 */
 public function storeClienteMoral()
 {
 
 }

  /**
 * @api {post} /distribuidor Registrar Distribuidor
 * @apiVersion 0.1.0
 * @apiName PostCreateDistribuidor
 * @apiGroup Usuario
 * 
 * @apiParam {String} correo Correo del usuario.
 * @apiParam {String} nombre1 Primer nombre.
 * @apiParam {String} nombre2 Segundo nombre (Opcional).
 * @apiParam {String} apellido1 Primer apellido.
 * @apiParam {String} apellido2 Segundo apellido (Opcional).
 * @apiParam {String} telefono_local Telefono local.
 * @apiParam {String} telefono_celular Telefono celular.
 * @apiParam {String} fecha_nacimiento Fecha de nacimiento (YYYY-MM-DD).
 * @apiParam {String} sexo Sexo (Masculino o Femenino).
 * @apiParam {Number} tipo_identificacion ID tipo de identificación (1 por default).
 * @apiParam {Number} identificacion Numero de documento de identidad.
 * @apiParam {String} direccion Dirección de envio.
 * @apiParam {Number} direccion_longitud Longitud de la dirección.
 * @apiParam {Number} direccion_latitud Latitud de la dirección
 * @apiParam {String} codigo_postal Codigo postal.
 * @apiParam {String} colonia Colonia.
 * @apiParam {String} numero_ext Numero exterior (Opcional).
 * @apiParam {String} numero_int Numero interior (Opcional).
 * @apiParam {Number} id_vehiculo ID del vehiculo
 * @apiParam {String} password El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 * @apiSuccess {Object} data   Objeto que contiene la información de la solicitud.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 * 		  "mensaje": "Se creo con exito el distribuidor"
 *     }
 *
 * @apiError UserBadResponse Registro erroneo
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Registro erroneo",
 *       "errors": []
 *     }
 */
 public function storeDistribuidor()
 {
 
 }

   /**
 * @api {post} /administrador Registrar Administrador
 * @apiVersion 0.1.0
 * @apiName PostCreateAdmin
 * @apiGroup Usuario
 * 
 * @apiParam {String} correo Correo del usuario.
 * @apiParam {String} nombre1 Primer nombre.
 * @apiParam {String} nombre2 Segundo nombre (Opcional).
 * @apiParam {String} apellido1 Primer apellido.
 * @apiParam {String} apellido2 Segundo apellido (Opcional).
 * @apiParam {String} telefono_local Telefono local.
 * @apiParam {String} telefono_celular Telefono celular.
 * @apiParam {String} fecha_nacimiento Fecha de nacimiento (YYYY-MM-DD).
 * @apiParam {String} sexo Sexo (Masculino o Femenino).
 * @apiParam {Number} tipo_identificacion ID tipo de identificación (1 por default).
 * @apiParam {Number} identificacion Numero de documento de identidad.
 * @apiParam {String} direccion Dirección de envio.
 * @apiParam {Number} direccion_longitud Longitud de la dirección.
 * @apiParam {Number} direccion_latitud Latitud de la dirección.
 * @apiParam {String} codigo_postal Codigo postal.
 * @apiParam {String} colonia Colonia.
 * @apiParam {String} numero_ext Numero exterior (Opcional).
 * @apiParam {String} numero_int Numero interior (Opcional).
 * @apiParam {String} password El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 * @apiSuccess {Object} data   Objeto que contiene la información de la solicitud.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 * 		  "mensaje": "Se creo con exito"
 *     }
 *
 * @apiError UserBadResponse Registro erroneo
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Registro erroneo",
 *       "errors": []
 *     }
 */
 public function storeAdmin()
 {
 
 }

    /**
 * @api {post} /callcenter Registrar operador de call center
 * @apiVersion 0.1.0
 * @apiName PostCreateCallCenter
 * @apiGroup Usuario
 * 
 * @apiParam {String} correo Correo del usuario.
 * @apiParam {String} nombre1 Primer nombre.
 * @apiParam {String} nombre2 Segundo nombre (Opcional).
 * @apiParam {String} apellido1 Primer apellido.
 * @apiParam {String} apellido2 Segundo apellido (Opcional).
 * @apiParam {String} telefono_local Telefono local.
 * @apiParam {String} telefono_celular Telefono celular.
 * @apiParam {String} fecha_nacimiento Fecha de nacimiento (YYYY-MM-DD).
 * @apiParam {String} sexo Sexo (Masculino o Femenino).
 * @apiParam {Number} tipo_identificacion ID tipo de identificación (1 por default).
 * @apiParam {Number} identificacion Numero de documento de identidad.
 * @apiParam {String} direccion Dirección de envio.
 * @apiParam {Number} direccion_longitud Longitud de la dirección.
 * @apiParam {Number} direccion_latitud Latitud de la dirección.
 * @apiParam {String} codigo_postal Codigo postal.
 * @apiParam {String} colonia Colonia.
 * @apiParam {String} numero_ext Numero exterior (Opcional).
 * @apiParam {String} numero_int Numero interior (Opcional).
 * @apiParam {String} password El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 * @apiSuccess {Object} data   Objeto que contiene la información de la solicitud.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 * 		  "mensaje": "Se creo con exito"
 *     }
 *
 * @apiError UserBadResponse Registro erroneo
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Registro erroneo",
 *       "errors": []
 *     }
 */
 public function storeCallcenter()
 {
 
 }

     /**
 * @api {post} /sucursal Registrar perfil de sucursal
 * @apiVersion 0.1.0
 * @apiName PostCreateSucursal
 * @apiGroup Usuario
 * 
 * @apiParam {String} correo Correo del usuario.
 * @apiParam {String} nombre1 Primer nombre.
 * @apiParam {String} nombre2 Segundo nombre (Opcional).
 * @apiParam {String} apellido1 Primer apellido.
 * @apiParam {String} apellido2 Segundo apellido (Opcional).
 * @apiParam {String} telefono_local Telefono local.
 * @apiParam {String} telefono_celular Telefono celular.
 * @apiParam {String} fecha_nacimiento Fecha de nacimiento (YYYY-MM-DD).
 * @apiParam {String} sexo Sexo (Masculino o Femenino).
 * @apiParam {Number} tipo_identificacion ID tipo de identificación (1 por default).
 * @apiParam {Number} identificacion Numero de documento de identidad.
 * @apiParam {String} direccion Dirección de envio.
 * @apiParam {Number} direccion_longitud Longitud de la dirección.
 * @apiParam {Number} direccion_latitud Latitud de la dirección.
 * @apiParam {String} codigo_postal Codigo postal.
 * @apiParam {String} colonia Colonia.
 * @apiParam {String} numero_ext Numero exterior (Opcional).
 * @apiParam {String} numero_int Numero interior (Opcional).
 * @apiParam {String} password El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 * @apiSuccess {Object} data   Objeto que contiene la información de la solicitud.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 * 		  "mensaje": "Se creo con exito"
 *     }
 *
 * @apiError UserBadResponse Registro erroneo
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Registro erroneo",
 *       "errors": []
 *     }
 */
 public function storeSucursal()
 {
 
 }

     /**
 * @api {post} /franquicia Registrar perfil de franquicia
 * @apiVersion 0.1.0
 * @apiName PostCreateFranquicia
 * @apiGroup Usuario
 * 
 * @apiParam {String} correo Correo del usuario.
 * @apiParam {String} nombre1 Primer nombre.
 * @apiParam {String} nombre2 Segundo nombre (Opcional).
 * @apiParam {String} apellido1 Primer apellido.
 * @apiParam {String} apellido2 Segundo apellido (Opcional).
 * @apiParam {String} telefono_local Telefono local.
 * @apiParam {String} telefono_celular Telefono celular.
 * @apiParam {String} fecha_nacimiento Fecha de nacimiento (YYYY-MM-DD).
 * @apiParam {String} sexo Sexo (Masculino o Femenino).
 * @apiParam {Number} tipo_identificacion ID tipo de identificación (1 por default).
 * @apiParam {Number} identificacion Numero de documento de identidad.
 * @apiParam {String} direccion Dirección de envio.
 * @apiParam {Number} direccion_longitud Longitud de la dirección.
 * @apiParam {Number} direccion_latitud Latitud de la dirección.
 * @apiParam {String} codigo_postal Codigo postal.
 * @apiParam {String} colonia Colonia.
 * @apiParam {String} numero_ext Numero exterior (Opcional).
 * @apiParam {String} numero_int Numero interior (Opcional).
 * @apiParam {String} password El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 * @apiSuccess {Object} data   Objeto que contiene la información de la solicitud.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 * 		  "mensaje": "Se creo con exito"
 *     }
 *
 * @apiError UserBadResponse Registro erroneo
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Registro erroneo",
 *       "errors": []
 *     }
 */
 public function storeFranquicia()
 {
 
 }

      /**
 * @api {post} /supervisor Registrar perfil de supervisor
 * @apiVersion 0.1.0
 * @apiName PostCreateSupervisor
 * @apiGroup Usuario
 * 
 * @apiParam {String} correo Correo del usuario.
 * @apiParam {String} nombre1 Primer nombre.
 * @apiParam {String} nombre2 Segundo nombre (Opcional).
 * @apiParam {String} apellido1 Primer apellido.
 * @apiParam {String} apellido2 Segundo apellido (Opcional).
 * @apiParam {String} telefono_local Telefono local.
 * @apiParam {String} telefono_celular Telefono celular.
 * @apiParam {String} fecha_nacimiento Fecha de nacimiento (YYYY-MM-DD).
 * @apiParam {String} sexo Sexo (Masculino o Femenino).
 * @apiParam {Number} tipo_identificacion ID tipo de identificación (1 por default).
 * @apiParam {Number} identificacion Numero de documento de identidad.
 * @apiParam {String} direccion Dirección de envio.
 * @apiParam {Number} direccion_longitud Longitud de la dirección.
 * @apiParam {Number} direccion_latitud Latitud de la dirección.
 * @apiParam {String} codigo_postal Codigo postal.
 * @apiParam {String} colonia Colonia.
 * @apiParam {String} numero_ext Numero exterior (Opcional).
 * @apiParam {String} numero_int Numero interior (Opcional).
 * @apiParam {String} password El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 * @apiSuccess {Object} data   Objeto que contiene la información de la solicitud.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 * 		  "mensaje": "Se creo con exito"
 *     }
 *
 * @apiError UserBadResponse Registro erroneo
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Registro erroneo",
 *       "errors": []
 *     }
 */
 public function storeSupervisor()
 {
 
 }

      /**
 * @api {post} /planta Registrar perfil de planta
 * @apiVersion 0.1.0
 * @apiName PostCreatePlanta
 * @apiGroup Usuario
 * 
 * @apiParam {String} correo Correo del usuario.
 * @apiParam {String} nombre1 Primer nombre.
 * @apiParam {String} nombre2 Segundo nombre (Opcional).
 * @apiParam {String} apellido1 Primer apellido.
 * @apiParam {String} apellido2 Segundo apellido (Opcional).
 * @apiParam {String} telefono_local Telefono local.
 * @apiParam {String} telefono_celular Telefono celular.
 * @apiParam {String} fecha_nacimiento Fecha de nacimiento (YYYY-MM-DD).
 * @apiParam {String} sexo Sexo (Masculino o Femenino).
 * @apiParam {Number} tipo_identificacion ID tipo de identificación (1 por default).
 * @apiParam {Number} identificacion Numero de documento de identidad.
 * @apiParam {String} direccion Dirección de envio.
 * @apiParam {Number} direccion_longitud Longitud de la dirección.
 * @apiParam {Number} direccion_latitud Latitud de la dirección.
 * @apiParam {String} codigo_postal Codigo postal.
 * @apiParam {String} colonia Colonia.
 * @apiParam {String} numero_ext Numero exterior (Opcional).
 * @apiParam {String} numero_int Numero interior (Opcional).
 * @apiParam {String} password El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 * @apiSuccess {Object} data   Objeto que contiene la información de la solicitud.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 * 		  "mensaje": "Se creo con exito"
 *     }
 *
 * @apiError UserBadResponse Registro erroneo
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Registro erroneo",
 *       "errors": []
 *     }
 */
 public function storePlanta()
 {
 
 }

       /**
 * @api {post} /rrhh Registrar perfil de RRHH
 * @apiVersion 0.1.0
 * @apiName PostCreateRRHH
 * @apiGroup Usuario
 * 
 * @apiParam {String} correo Correo del usuario.
 * @apiParam {String} nombre1 Primer nombre.
 * @apiParam {String} nombre2 Segundo nombre (Opcional).
 * @apiParam {String} apellido1 Primer apellido.
 * @apiParam {String} apellido2 Segundo apellido (Opcional).
 * @apiParam {String} telefono_local Telefono local.
 * @apiParam {String} telefono_celular Telefono celular.
 * @apiParam {String} fecha_nacimiento Fecha de nacimiento (YYYY-MM-DD).
 * @apiParam {String} sexo Sexo (Masculino o Femenino).
 * @apiParam {Number} tipo_identificacion ID tipo de identificación (1 por default).
 * @apiParam {Number} identificacion Numero de documento de identidad.
 * @apiParam {String} direccion Dirección de envio.
 * @apiParam {Number} direccion_longitud Longitud de la dirección.
 * @apiParam {Number} direccion_latitud Latitud de la dirección.
 * @apiParam {String} codigo_postal Codigo postal.
 * @apiParam {String} colonia Colonia.
 * @apiParam {String} numero_ext Numero exterior (Opcional).
 * @apiParam {String} numero_int Numero interior (Opcional).
 * @apiParam {String} password El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 * @apiSuccess {Object} data   Objeto que contiene la información de la solicitud.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 * 		  "mensaje": "Se creo con exito"
 *     }
 *
 * @apiError UserBadResponse Registro erroneo
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Registro erroneo",
 *       "errors": []
 *     }
 */
 public function storeRrhh()
 {
 
 }

    /**
     * @api {post} /operador Registrar perfil de Operador
     * @apiVersion 0.1.0
     * @apiName PostCreateOperador
     * @apiGroup Usuario
     *
     * @apiParam {String} correo Correo del usuario.
     * @apiParam {String} nombre1 Primer nombre.
     * @apiParam {String} nombre2 Segundo nombre (Opcional).
     * @apiParam {String} apellido1 Primer apellido.
     * @apiParam {String} apellido2 Segundo apellido (Opcional).
     * @apiParam {String} telefono_local Telefono local.
     * @apiParam {String} telefono_celular Telefono celular.
     * @apiParam {String} fecha_nacimiento Fecha de nacimiento (YYYY-MM-DD).
     * @apiParam {String} sexo Sexo (Masculino o Femenino).
     * @apiParam {Number} tipo_identificacion ID tipo de identificación (1 por default).
     * @apiParam {Number} identificacion Numero de documento de identidad.
     * @apiParam {String} direccion Dirección de envio.
     * @apiParam {Number} direccion_longitud Longitud de la dirección.
     * @apiParam {Number} direccion_latitud Latitud de la dirección.
     * @apiParam {String} codigo_postal Codigo postal.
     * @apiParam {String} colonia Colonia.
     * @apiParam {String} numero_ext Numero exterior (Opcional).
     * @apiParam {String} numero_int Numero interior (Opcional).
     * @apiParam {String} password El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.
     *
     * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
     * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
     * @apiSuccess {Object} data   Objeto que contiene la información de la solicitud.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        "status": true,
     * 		  "mensaje": "Se creo con exito"
     *     }
     *
     * @apiError UserBadResponse Registro erroneo
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *		 "status": false,
     *       "mensaje": "Registro erroneo",
     *       "errors": []
     *     }
     */
    public function storeOperador()
    {

    }

    /**
     * @api {post} /staff Registrar perfil de Staff
     * @apiVersion 0.1.0
     * @apiName PostCreateStaff
     * @apiGroup Usuario
     *
     * @apiParam {String} correo Correo del usuario.
     * @apiParam {String} nombre1 Primer nombre.
     * @apiParam {String} nombre2 Segundo nombre (Opcional).
     * @apiParam {String} apellido1 Primer apellido.
     * @apiParam {String} apellido2 Segundo apellido (Opcional).
     * @apiParam {String} telefono_local Telefono local.
     * @apiParam {String} telefono_celular Telefono celular.
     * @apiParam {String} fecha_nacimiento Fecha de nacimiento (YYYY-MM-DD).
     * @apiParam {String} sexo Sexo (Masculino o Femenino).
     * @apiParam {Number} tipo_identificacion ID tipo de identificación (1 por default).
     * @apiParam {Number} identificacion Numero de documento de identidad.
     * @apiParam {String} direccion Dirección de envio.
     * @apiParam {Number} direccion_longitud Longitud de la dirección.
     * @apiParam {Number} direccion_latitud Latitud de la dirección.
     * @apiParam {String} codigo_postal Codigo postal.
     * @apiParam {String} colonia Colonia.
     * @apiParam {String} numero_ext Numero exterior (Opcional).
     * @apiParam {String} numero_int Numero interior (Opcional).
     * @apiParam {String} password El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.
     *
     * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
     * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
     * @apiSuccess {Object} data   Objeto que contiene la información de la solicitud.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        "status": true,
     * 		  "mensaje": "Se creo con exito"
     *     }
     *
     * @apiError UserBadResponse Registro erroneo
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *		 "status": false,
     *       "mensaje": "Registro erroneo",
     *       "errors": []
     *     }
     */
    public function storeStaff()
    {

    }

 /**
 * @api {put} /estatus/:id Activa o desactiva un usuario
 * @apiVersion 0.1.0
 * @apiName PutEstatusUser
 * @apiGroup Usuario
 *
 * @apiParam {Number} id ID de usuario.
 * @apiParam {Boolean} estado true o false.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 * 		  "mensaje": "Se cambio el estatus con exito del usuario"
 *     }
 *
 * @apiError UserNotFound No se consigio el usuario con el id suministrado
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "No se consigio el usuario con el id suministrado"
 *     }
 */
 public function estatus()
 {
 
 }

  /**
 * @api {post} /usuario/recuperar_password Recuperar password
 * @apiVersion 0.1.0
 * @apiName PostRecuperarPassword
 * @apiGroup Usuario
 * 
 * @apiParam {String} correo Correo del usuario.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 * @apiSuccess {Object} data   Objeto que contiene los datos de usuario, dirección y articulos.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 *        "mensaje": "Se completo la petición para la recuperación",
 *        "data": {
 *			"usuario": []
 *		  }
 *     }
 *
 * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Problemas para procesar solicitud"
 *       "errors": []
 *     }
 */
 public function recuperarPassword()
 {
 
 }

   /**
 * @api {post} /usuario/nuevo_password Nuevo password
 * @apiVersion 0.1.0
 * @apiName PostNuevoPassword
 * @apiGroup Usuario
 * 
 * @apiParam {String} password El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.
 * @apiParam {Number} id_usuario ID de usuari.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 *        "mensaje": "Se completo la petición para la recuperación"
 *     }
 *
 * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Problemas para procesar solicitud"
 *       "errors": []
 *     }
 */
 public function nuevoPasword()
 {
 
 }

  /**
 * @api {get} /usuario Listar todos los usuarios
 * @apiVersion 0.1.0
 * @apiName GetUsuarios
 * @apiGroup Usuario
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        [{"id_usuario":2,"nombre":"Otoniel","apellido":"Marquez","correo":"otonielmax@hotmail.com","telefono":"04264170396","id_rol":1,"perfil":"Cliente Fisico"},{"id_usuario":4,"nombre":"yusmay","apellido":"davila","correo":"yus@hot.com","telefono":"04265289643","id_rol":1,"perfil":"Cliente Fisico"},{"id_usuario":3,"nombre":"Distribuidor","apellido":"Prueba","correo":"distribuidor@hotmail.com","telefono":"04241234567","id_rol":2,"perfil":"Distribuidor"},{"id_usuario":1,"nombre":"Sistema","apellido":"Gestion","correo":"masgas@masgas.com","telefono":"04141234567","id_rol":3,"perfil":"Super Usuario"}]
 *     }
 *
 * @apiError UserBadResponse Problema en solicitud
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 
 *     }
 */
 public function index()
 {
 
 }

  /**
 * @api {get} /clientes_qr Clientes que no tienen impreso su QR
 * @apiVersion 0.1.0
 * @apiName GetClientesQR
 * @apiGroup Usuario
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       
 *     }
 *
 * @apiError UserBadResponse Problema en solicitud
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 
 *     }
 */
 public function clientesQr()
 {
 
 }

 /**
 * @api {post} /clientes_qr Actualizar QR impreso cliente
 * @apiVersion 0.1.0
 * @apiName PostClienteQRImpreso
 * @apiGroup Usuario
 * 
 * @apiParam {Number} id_usuario ID de usuario para actualizar el estatus de la impresión del codigo QR (Se puede enviar un array de ID para actualizar por lote).
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 *        "mensaje": "Se actualizaron los clientes con el QR impreso"
 *     }
 *
 * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Problemas para procesar solicitud"
 *       "errors": []
 *     }
 */
 public function qrImpreso()
 {
 
 }

}
