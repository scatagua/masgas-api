<?php
class UnidadMetrica
{

    public function __construct()
    {

    }

    /**
     * @api {post} /unidad_metrica Crear unidad metrica
     * @apiVersion 0.1.0
     * @apiName PostUnidadMetrica
     * @apiGroup Unidad Metrica
     *
     * @apiParam {String} tx_nombre Nombre de la unidad metrica.
     * @apiParam {String} tx_simbolo Simbolo de la unidad (Kg, cm3, Lts).
     *
     * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
     * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        "status": true,
     *        "mensaje": "Registro exitoso"
     *     }
     *
     * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *		 "status": false,
     *       "mensaje": "Problemas para procesar solicitud"
     *       "errors": []
     *     }
     */
    public function storeUnidad()
    {

    }

    /**
     * @api {get} /unidad_metrica Ver unidades metricas
     * @apiVersion 0.1.0
     * @apiName GetUnidadMetrica
     * @apiGroup Unidad Metrica
     *
     * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
     * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        "status": true,
     *        "mensaje": "Registro exitoso"
     *     }
     *
     * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *		 "status": false,
     *       "mensaje": "Problemas para procesar solicitud"
     *       "errors": []
     *     }
     */
    public function getUnidad()
    {

    }

    /**
     * @api {post} /equivalencia Crear equivalencia
     * @apiVersion 0.1.0
     * @apiName PostEquivalencia
     * @apiGroup Unidad Metrica
     *
     * @apiParam {Number} id_unidad_from ID de la unidad metrica desde la cual se convertira.
     * @apiParam {Number} id_unidad_to ID de la unidad metrica a la que se convertira.
     * @apiParam {Number} nu_valor Valor porcentual de conversion (0.9 Kg a Lts).
     *
     * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
     * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        "status": true,
     *        "mensaje": "Registro exitoso"
     *     }
     *
     * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *		 "status": false,
     *       "mensaje": "Problemas para procesar solicitud"
     *       "errors": []
     *     }
     */
    public function storeEquivalencia()
    {

    }

    /**
     * @api {get} /equivalencia Ver equivalencias
     * @apiVersion 0.1.0
     * @apiName GetEquivalencia
     * @apiGroup Unidad Metrica
     *
     * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
     * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        "status": true,
     *        "mensaje": "Registro exitoso"
     *     }
     *
     * @apiError UserBadResponse Las credenciales no corresponden a ninguno de nuestros usuarios
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *		 "status": false,
     *       "mensaje": "Problemas para procesar solicitud"
     *       "errors": []
     *     }
     */
    public function getEquivalencia()
    {

    }
}
