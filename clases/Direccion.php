<?php
class Direccion
{
 
 public function __construct()
 {
 
 }

 /**
 * @api {post} /direccion/cliente Crear dirección
 * @apiVersion 0.1.0
 * @apiName PostDireccion
 * @apiGroup Direccion
 * 
 * @apiParam {String} tx_nombre Dirección de envio.
 * @apiParam {String} tx_referencia Punto de referencia.
 * @apiParam {String} tx_codigo_postal Codigo postal.
 * @apiParam {String} tx_colonia Nombre de la colonia.
 * @apiParam {Number} nu_longitud Longitud de la dirección.
 * @apiParam {Number} nu_latitud Latitud de la dirección.
 * @apiParam {Number} id_usuario ID de usuario.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 * @apiSuccess {Object} data   Objeto que contiene la informacion de la solicitud.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 *        "mensaje": "Se registro con exito"
 *        "data": {
 *			"direccion": []
 *		  }
 *     }
 *
 * @apiError UserBadResponse Los parametros enviados no son validos, por favor verifique
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Los parametros enviados no son validos, por favor verifique",
 *		 "data": []
 *     }
 */
 public function storeDireccionCliente()
 {
 
 }

 /**
 * @api {post} /direccion/update Actualizar dirección
 * @apiVersion 0.1.0
 * @apiName PostDireccion
 * @apiGroup Direccion
 * 
 * @apiParam {String} tx_nombre Dirección de envio.
 * @apiParam {String} tx_referencia Punto de referencia.
 * @apiParam {String} tx_codigo_postal Codigo postal.
 * @apiParam {String} tx_colonia Nombre de la colonia.
 * @apiParam {Number} nu_longitud Longitud de la dirección.
 * @apiParam {Number} nu_latitud Latitud de la dirección.
 * @apiParam {Number} id_usuario ID de usuario.
 * @apiParam {Number} id_direccion ID de la dirección.
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 * @apiSuccess {Object} data   Objeto que contiene la informacion de la solicitud.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 *        "mensaje": "Se actualizo con exito"
 *        "data": {
 *			"direccion": []
 *		  }
 *     }
 *
 * @apiError UserBadResponse Los parametros enviados no son validos, por favor verifique
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *		 "status": false,
 *       "mensaje": "Los parametros enviados no son validos, por favor verifique",
 *		 "data": []
 *     }
 */
 public function updateDireccion()
 {
 
 }

    /**
     * @api {get} /direccion/cliente/{id} Obtener direcciones de usuario
     * @apiVersion 0.1.0
     * @apiName GetDireccionUsuario
     * @apiGroup Direccion
     *
     * @apiParam {Number} id ID de usuario.
     *
     * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
     * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
     * @apiSuccess {Object} data   Objeto que contiene la informacion de la solicitud.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        "status": true,
     *        "mensaje": "Se registro con exito"
     *        "data": {
     *			"direccion": []
     *		  }
     *     }
     *
     * @apiError UserBadResponse Los parametros enviados no son validos, por favor verifique
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *		 "status": false,
     *       "mensaje": "Los parametros enviados no son validos, por favor verifique",
     *		 "data": []
     *     }
     */
    public function direccionUsuario()
    {

    }

    /**
     * @api {get} /direccion_sin_sucursal Direcciones sin sucursal
     * @apiVersion 0.1.0
     * @apiName GetDireccionSinSucursal
     * @apiGroup Direccion
     *
     * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
     * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
     * @apiSuccess {Object} data   Objeto que contiene la informacion de la solicitud.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        "status": true,
     *        "mensaje": "Consulta exitosa"
     *        "data": {
     *          "direccion": []
     *        }
     *     }
     *
     * @apiError UserBadResponse Los parametros enviados no son validos, por favor verifique
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "status": false,
     *       "mensaje": "Los parametros enviados no son validos, por favor verifique",
     *       "data": []
     *     }
     */
    public function direccionSinSucursal()
    {

    }

    /**
 * @api {post} /asignar_direccion_sucursal Asignar dirección a sucursal
 * @apiVersion 0.1.0
 * @apiName PostDireccionSucursal
 * @apiGroup Direccion
 * 
 * @apiParam {Number} id_sucursal ID de la sucursal.
 * @apiParam {Number} id_direccion ID de la dirección (Puedes enviar un array con los id).
 *
 * @apiSuccess {Boolean} status  true o false dependiendo de si el proceso fue exitoso o no respectivamente.
 * @apiSuccess {String} mensaje  Devolvera un mensaje de acuerdo a lo acontecido.
 * @apiSuccess {Object} data   Objeto que contiene la informacion de la solicitud.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": true,
 *        "mensaje": "Se actualizo con exito"
 *        "data": {
 *          "direccion": []
 *        }
 *     }
 *
 * @apiError UserBadResponse Los parametros enviados no son validos, por favor verifique
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "status": false,
 *       "mensaje": "Los parametros enviados no son validos, por favor verifique",
 *       "data": []
 *     }
 */
 public function asignarSucursalDireccion()
 {
 
 }
}
