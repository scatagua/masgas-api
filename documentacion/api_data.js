define({ "api": [
  {
    "type": "get",
    "url": "/articulo",
    "title": "Listar articulos",
    "version": "0.1.0",
    "name": "GetArticulos",
    "group": "Articulo",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   [{\"id\":1,\"tx_nombre\":\"Bombona 10 Kg\",\"nu_precio\":5000,\"nu_cantidad\":10},{\"id\":2,\"tx_nombre\":\"Bombona 18 Kg\",\"nu_precio\":8000,\"nu_cantidad\":15},{\"id\":3,\"tx_nombre\":\"Bombona 30 Kg\",\"nu_precio\":12000,\"nu_cantidad\":8}]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Articulo.php",
    "groupTitle": "Articulo"
  },
  {
    "type": "get",
    "url": "/articulo/categoria/:id",
    "title": "Listar articulos por categoria",
    "version": "0.1.0",
    "name": "GetArticulosCategoria",
    "group": "Articulo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID de la categoria del producto.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  [{\"id_articulo\":1,\"articulo\":\"Cilindro 5 Kg\",\"precio\":5000,\"cantidad\":\"10\",\"id_articulo_categoria\":1},{\"id_articulo\":2,\"articulo\":\"Cilindro 10 Kg\",\"precio\":8000,\"cantidad\":\"15\",\"id_articulo_categoria\":1},{\"id_articulo\":3,\"articulo\":\"Bombona 20 Kg\",\"precio\":12000,\"cantidad\":\"8\",\"id_articulo_categoria\":1},{\"id_articulo\":4,\"articulo\":\"Bombona 30 Kg\",\"precio\":18000,\"cantidad\":\"10\",\"id_articulo_categoria\":1},{\"id_articulo\":5,\"articulo\":\"Bombona 40 Kg\",\"precio\":25000,\"cantidad\":\"15\",\"id_articulo_categoria\":1}]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Articulo.php",
    "groupTitle": "Articulo"
  },
  {
    "type": "post",
    "url": "/articulo/inventario",
    "title": "Actualizar inventario por lote de articulos",
    "version": "0.1.0",
    "name": "PostArticuloInventario",
    "group": "Articulo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "id_articulo",
            "description": "<p>Lista de id de los articulos a actualizar su precio.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "cantidad",
            "description": "<p>Nueva cantidad de los articulos, la posicion del articulo en la lista debe corresponder con la de la cantidad, para no generar errores.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Solicitud completada\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problemas para procesar solictud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Articulo.php",
    "groupTitle": "Articulo"
  },
  {
    "type": "post",
    "url": "/articulo/precio",
    "title": "Crear articulo",
    "version": "0.1.0",
    "name": "PostArticuloPrecio",
    "group": "Articulo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "id_articulo",
            "description": "<p>Lista de id de los articulos a actualizar su precio.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "precio",
            "description": "<p>Nuevo precio de los articulos, la posicion del articulo en la lista debe corresponder con la del precio, para no generar errores.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Solicitud completada\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problemas para procesar solictud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Articulo.php",
    "groupTitle": "Articulo"
  },
  {
    "type": "post",
    "url": "/articulo/precio",
    "title": "Actualizar precio por lote de articulos",
    "version": "0.1.0",
    "name": "PostArticuloPrecio",
    "group": "Articulo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "id_articulo",
            "description": "<p>Lista de id de los articulos a actualizar su precio.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "precio",
            "description": "<p>Nuevo precio de los articulos, la posicion del articulo en la lista debe corresponder con la del precio, para no generar errores.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Solicitud completada\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problemas para procesar solictud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Articulo.php",
    "groupTitle": "Articulo"
  },
  {
    "type": "put",
    "url": "/articulo/precio/:id",
    "title": "Actualizar precio de articulo",
    "version": "0.1.0",
    "name": "PutArticuloPrecio",
    "group": "Articulo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_articulo",
            "description": "<p>ID del articulo.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "precio",
            "description": "<p>Precio del articulo.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Solicitud completada\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Articulo.php",
    "groupTitle": "Articulo"
  },
  {
    "type": "post",
    "url": "/dashboard",
    "title": "Dashboard",
    "version": "0.1.0",
    "name": "PostDashboard",
    "group": "Dashboard",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fecha_inicio",
            "description": "<p>Fecha de inicio.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fecha_fin",
            "description": "<p>Fecha fin.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Objeto que contiene la información de la solicitud.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       {\n\t\t    \"status\": true, \n\t\t    \"mensaje\": \"Consulta exitosa\",\n\t\t    \"data\": {\n\t\t        \"inventario\": [\n\t\t            {\n\t\t                \"Gas Disponible\": \"10058\",\n\t\t                \"Gas en Distribución\": null\n\t\t            }\n\t\t        ],\n\t\t        \"venta\": [\n\t\t            {\n\t\t                \"Producto\": \"Cilindro 4 Lts\",\n\t\t                \"Cantidad Vendida\": null,\n\t\t                \"Monto\": null\n\t\t            },\n\t\t            {\n\t\t                \"Producto\": \"Cilindro 10 Lts\",\n\t\t                \"Cantidad Vendida\": null,\n\t\t                \"Monto\": null\n\t\t            },\n\t\t            {\n\t\t                \"Producto\": \"Cilindro 20 Lts\",\n\t\t                \"Cantidad Vendida\": null,\n\t\t                \"Monto\": null\n\t\t            },\n\t\t            {\n\t\t                \"Producto\": \"Cilindro 30 Lts\",\n\t\t                \"Cantidad Vendida\": null,\n\t\t                \"Monto\": null\n\t\t            },\n\t\t            {\n\t\t                \"Producto\": \"Cilindro 40 Lts\",\n\t\t                \"Cantidad Vendida\": null,\n\t\t                \"Monto\": null\n\t\t            },\n\t\t            {\n\t\t                \"Producto\": \"Pipa\",\n\t\t                \"Cantidad Vendida\": null,\n\t\t                \"Monto\": null\n\t\t            }\n\t\t        ],\n\t\t        \"pago_pendiente\": [\n\t\t            {\n\t\t                \"Cantidad Vendida\": null,\n\t\t                \"Monto\": null\n\t\t            }\n\t\t        ],\n\t\t        \"pago_pasarela\": {\n\t\t            \"cantidad\": 0,\n\t\t            \"monto\": 0\n\t\t        },\n\t\t        \"proveedor\": [],\n\t\t        \"ventas_departamento\": []\n\t\t    }\n\t\t}\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Pedido.php",
    "groupTitle": "Dashboard"
  },
  {
    "type": "get",
    "url": "/direccion_sin_sucursal",
    "title": "Direcciones sin sucursal",
    "version": "0.1.0",
    "name": "GetDireccionSinSucursal",
    "group": "Direccion",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Objeto que contiene la informacion de la solicitud.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Consulta exitosa\"\n   \"data\": {\n     \"direccion\": []\n   }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Los parametros enviados no son validos, por favor verifique</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"status\": false,\n  \"mensaje\": \"Los parametros enviados no son validos, por favor verifique\",\n  \"data\": []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Direccion.php",
    "groupTitle": "Direccion"
  },
  {
    "type": "get",
    "url": "/direccion/cliente/{id}",
    "title": "Obtener direcciones de usuario",
    "version": "0.1.0",
    "name": "GetDireccionUsuario",
    "group": "Direccion",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID de usuario.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Objeto que contiene la informacion de la solicitud.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       \"status\": true,\n       \"mensaje\": \"Se registro con exito\"\n       \"data\": {\n\t\t\t\"direccion\": []\n\t\t  }\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Los parametros enviados no son validos, por favor verifique</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Los parametros enviados no son validos, por favor verifique\",\n\t\t \"data\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Direccion.php",
    "groupTitle": "Direccion"
  },
  {
    "type": "post",
    "url": "/direccion/cliente",
    "title": "Crear dirección",
    "version": "0.1.0",
    "name": "PostDireccion",
    "group": "Direccion",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tx_nombre",
            "description": "<p>Dirección de envio.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tx_referencia",
            "description": "<p>Punto de referencia.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tx_codigo_postal",
            "description": "<p>Codigo postal.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tx_colonia",
            "description": "<p>Nombre de la colonia.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "nu_longitud",
            "description": "<p>Longitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "nu_latitud",
            "description": "<p>Latitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_usuario",
            "description": "<p>ID de usuario.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Objeto que contiene la informacion de la solicitud.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       \"status\": true,\n       \"mensaje\": \"Se registro con exito\"\n       \"data\": {\n\t\t\t\"direccion\": []\n\t\t  }\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Los parametros enviados no son validos, por favor verifique</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Los parametros enviados no son validos, por favor verifique\",\n\t\t \"data\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Direccion.php",
    "groupTitle": "Direccion"
  },
  {
    "type": "post",
    "url": "/direccion/update",
    "title": "Actualizar dirección",
    "version": "0.1.0",
    "name": "PostDireccion",
    "group": "Direccion",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tx_nombre",
            "description": "<p>Dirección de envio.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tx_referencia",
            "description": "<p>Punto de referencia.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tx_codigo_postal",
            "description": "<p>Codigo postal.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tx_colonia",
            "description": "<p>Nombre de la colonia.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "nu_longitud",
            "description": "<p>Longitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "nu_latitud",
            "description": "<p>Latitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_usuario",
            "description": "<p>ID de usuario.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_direccion",
            "description": "<p>ID de la dirección.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Objeto que contiene la informacion de la solicitud.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       \"status\": true,\n       \"mensaje\": \"Se actualizo con exito\"\n       \"data\": {\n\t\t\t\"direccion\": []\n\t\t  }\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Los parametros enviados no son validos, por favor verifique</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Los parametros enviados no son validos, por favor verifique\",\n\t\t \"data\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Direccion.php",
    "groupTitle": "Direccion"
  },
  {
    "type": "post",
    "url": "/asignar_direccion_sucursal",
    "title": "Asignar dirección a sucursal",
    "version": "0.1.0",
    "name": "PostDireccionSucursal",
    "group": "Direccion",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_sucursal",
            "description": "<p>ID de la sucursal.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_direccion",
            "description": "<p>ID de la dirección (Puedes enviar un array con los id).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Objeto que contiene la informacion de la solicitud.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Se actualizo con exito\"\n   \"data\": {\n     \"direccion\": []\n   }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Los parametros enviados no son validos, por favor verifique</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"status\": false,\n  \"mensaje\": \"Los parametros enviados no son validos, por favor verifique\",\n  \"data\": []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Direccion.php",
    "groupTitle": "Direccion"
  },
  {
    "type": "get",
    "url": "/notificacion_mensaje",
    "title": "Mensajes para notificaciones",
    "version": "0.1.0",
    "name": "GetMensajeNotificacion",
    "group": "Notificacion",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Novedad.php",
    "groupTitle": "Notificacion"
  },
  {
    "type": "get",
    "url": "/notificacion_mensaje_by_rol",
    "title": "Mensajes para notificaciones por rol",
    "version": "0.1.0",
    "name": "GetMensajeNotificacionRol",
    "group": "Notificacion",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Novedad.php",
    "groupTitle": "Notificacion"
  },
  {
    "type": "get",
    "url": "/mis_mensajes",
    "title": "Mensajes de usuario",
    "version": "0.1.0",
    "name": "GetMensajeUsuario",
    "group": "Notificacion",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_usuario",
            "description": "<p>ID de usuario.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Novedad.php",
    "groupTitle": "Notificacion"
  },
  {
    "type": "get",
    "url": "/novedades",
    "title": "Listar novedades",
    "version": "0.1.0",
    "name": "GetNovedades",
    "group": "Notificacion",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Novedad.php",
    "groupTitle": "Notificacion"
  },
  {
    "type": "get",
    "url": "/notificacion_by_rol/:id",
    "title": "Novedades emitidas por rol",
    "version": "0.1.0",
    "name": "GetNovedadesByIDRol",
    "group": "Notificacion",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Novedad.php",
    "groupTitle": "Notificacion"
  },
  {
    "type": "post",
    "url": "/notificacion",
    "title": "Registrar nueva notificación",
    "version": "0.1.0",
    "name": "PostNotificacion",
    "group": "Notificacion",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_usuario",
            "description": "<p>ID del usuario.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_mensaje",
            "description": "<p>ID del mensaje preestablecido.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tx_cuerpo",
            "description": "<p>Cuerpo del mensaje.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Registro exitoso\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Novedad.php",
    "groupTitle": "Notificacion"
  },
  {
    "type": "post",
    "url": "/notificacion/create",
    "title": "Registrar mensaje de notificación",
    "version": "0.1.0",
    "name": "PostNotificacionMensaje",
    "group": "Notificacion",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "id_rol",
            "description": "<p>Lista de ID de rol a los cuales estara relacionado el mensaje.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "tx_mensaje",
            "description": "<p>Mensaje predefinido.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Registro exitoso\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Novedad.php",
    "groupTitle": "Notificacion"
  },
  {
    "type": "get",
    "url": "/motivos_no_entrega",
    "title": "Motivos de no entrega",
    "version": "0.1.0",
    "name": "GetMotivosNoEntrega",
    "group": "Pedido",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Pedido.php",
    "groupTitle": "Pedido"
  },
  {
    "type": "get",
    "url": "/pedido_all",
    "title": "Obtener todos los pedidos efectuados",
    "version": "0.1.0",
    "name": "GetPedidoAll",
    "group": "Pedido",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Pedido.php",
    "groupTitle": "Pedido"
  },
  {
    "type": "get",
    "url": "/all_status_pedidos",
    "title": "Obtener todos los status para pedidos",
    "version": "0.1.0",
    "name": "GetPedidoAllStatus",
    "group": "Pedido",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Pedido.php",
    "groupTitle": "Pedido"
  },
  {
    "type": "get",
    "url": "/pedido_por_canal",
    "title": "Pedidos por canal",
    "version": "0.1.0",
    "name": "GetPedidoBySucursal",
    "group": "Pedido",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Pedido.php",
    "groupTitle": "Pedido"
  },
  {
    "type": "get",
    "url": "/pedido_por_sucursal",
    "title": "Pedidos por sucursal",
    "version": "0.1.0",
    "name": "GetPedidoBySucursal",
    "group": "Pedido",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Pedido.php",
    "groupTitle": "Pedido"
  },
  {
    "type": "get",
    "url": "/canales_venta",
    "title": "Canales de venta",
    "version": "0.1.0",
    "name": "GetPedidoCanales",
    "group": "Pedido",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Pedido.php",
    "groupTitle": "Pedido"
  },
  {
    "type": "get",
    "url": "/pedido/cliente/:id",
    "title": "Obtener pedidos por cliente",
    "version": "0.1.0",
    "name": "GetPedidoCliente",
    "group": "Pedido",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Pedido.php",
    "groupTitle": "Pedido"
  },
  {
    "type": "get",
    "url": "/pedido_details/:id",
    "title": "Detalle del pedido",
    "version": "0.1.0",
    "name": "GetPedidoDetalleByID",
    "group": "Pedido",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Pedido.php",
    "groupTitle": "Pedido"
  },
  {
    "type": "get",
    "url": "/pedido_no_entregado",
    "title": "Pedidos reportados con retrasos o devueltos",
    "version": "0.1.0",
    "name": "GetPedidoNoEntregados",
    "group": "Pedido",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Pedido.php",
    "groupTitle": "Pedido"
  },
  {
    "type": "get",
    "url": "/pedido_planificacion",
    "title": "Pedidos por asignar o replanificar",
    "version": "0.1.0",
    "name": "GetPedidoPlanificacion",
    "group": "Pedido",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Pedido.php",
    "groupTitle": "Pedido"
  },
  {
    "type": "post",
    "url": "/compra_proveedor",
    "title": "Compra Proveedor",
    "version": "0.1.0",
    "name": "PostCompraProveedor",
    "group": "Pedido",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_proveedor",
            "description": "<p>ID de proveedor.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_usuario",
            "description": "<p>ID de usuario que registra la compra.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_orden",
            "description": "<p>Numero de orden de compra.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "nu_cantidad",
            "description": "<p>Cantidad solicitada.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "nu_precio_unitario",
            "description": "<p>Precio unitario del producto.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "nu_flete",
            "description": "<p>Costo del flete.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "nu_total",
            "description": "<p>Costo total.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       {\n\t\t    \"status\": true, \n\t\t    \"mensaje\": \"Consulta exitosa\"\n\t\t  }\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Pedido.php",
    "groupTitle": "Pedido"
  },
  {
    "type": "post",
    "url": "/pedido/historial",
    "title": "Historial de pedidos",
    "version": "0.1.0",
    "name": "PostHistorialPedido",
    "group": "Pedido",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fecha_ini",
            "description": "<p>Fecha de inicio (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fecha_fin",
            "description": "<p>Fecha fin (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "estatus",
            "description": "<p>Filtrar por el estado del pedido (Opcional), Asignacion, Despacho, Entregado, etc..</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Objeto que contiene la información de la solicitud.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       \"status\": true,\n       \"mensaje\": \"Solicitud completada\",\n       \"data\": {\n\t\t\t\t\"pedidos\" : []\n\t\t   }\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Pedido.php",
    "groupTitle": "Pedido"
  },
  {
    "type": "post",
    "url": "/pedido",
    "title": "Crear pedido",
    "version": "0.1.0",
    "name": "PostPedido",
    "group": "Pedido",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_direccion",
            "description": "<p>ID de la direccion del usuario a donde sera enviado el pedido.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "bo_pagada",
            "description": "<p>Indica si la factura ha sido pagada.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "departamento_venta",
            "description": "<p>Departamento de ventas (App, Callcenter, etc).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tipo_pago",
            "description": "<p>Tipo de pago (TDC, Cheque, Efectivo, etc).</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "items",
            "description": "<p>Lista de items asociados al pedido, cada item debe indicar id_articulo, nu_cantidad y nu_subtotal este ultimo hace referencia al subtotal por el item en el momento en que se toma el pedido.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Registro exitoso\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Pedido.php",
    "groupTitle": "Pedido"
  },
  {
    "type": "post",
    "url": "/payment",
    "title": "Pago por medio de Conekta",
    "version": "0.1.0",
    "name": "PostPedidoPago",
    "group": "Pedido",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "card",
            "description": "<p>Datos del tarjetahabiente.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token emitido por conekta para procesar el pago.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_direccion",
            "description": "<p>ID de la direccion del usuario a donde sera enviado el pedido.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "bo_pagada",
            "description": "<p>Indica si la factura ha sido pagada.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "departamento_venta",
            "description": "<p>Departamento de ventas (App, Callcenter, etc).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tipo_pago",
            "description": "<p>Tipo de pago (TDC, Cheque, Efectivo, etc).</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "items",
            "description": "<p>Lista de items asociados al pedido, cada item debe indicar id_articulo, nu_cantidad y nu_subtotal este ultimo hace referencia al subtotal por el item en el momento en que se toma el pedido.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Registro exitoso\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Pedido.php",
    "groupTitle": "Pedido"
  },
  {
    "type": "post",
    "url": "/pedido_planificacion",
    "title": "Asignar nueva fecha de entrega",
    "version": "0.1.0",
    "name": "PostPedidoPlanificacion",
    "group": "Pedido",
    "description": "<p>Puede recibir un array de pedidos para replanificar o bien uno en particular</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fecha",
            "description": "<p>Fecha en la cual sera enviado el pedido al cliente (Si se envia una lista de pedidos, se sugiere que sean despachados en la misma fecha)</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "id_pedido",
            "description": "<p>Puede enviarse un array con todos los pedidos que se deseen asignar o replanificar para la misma fecha (Tambien se puede enviar un objeto).</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "id_distribuidor",
            "description": "<p>Si el id_pedido es una array, este parametro debe ser un array con la misma longitud.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       {\n\t\t    \"status\": true, \n\t\t    \"mensaje\": \"Consulta exitosa\"\n\t\t  }\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Pedido.php",
    "groupTitle": "Pedido"
  },
  {
    "type": "post",
    "url": "/recepcion_compra",
    "title": "Recepción de compra a Proveedor",
    "version": "0.1.0",
    "name": "PostRecepcionCompraProveedor",
    "group": "Pedido",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_compra",
            "description": "<p>ID de la compra.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tx_numero_orden",
            "description": "<p>Numero de orden de compra.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "nu_recibido",
            "description": "<p>Cantidad recibida.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "nu_merma",
            "description": "<p>Merma entre lo solicitado y lo recibido.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       {\n\t\t    \"status\": true, \n\t\t    \"mensaje\": \"Consulta exitosa\"\n\t\t  }\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Pedido.php",
    "groupTitle": "Pedido"
  },
  {
    "type": "post",
    "url": "/pedidos_by_status",
    "title": "Obtener pedidos por estatus",
    "version": "0.1.0",
    "name": "PostStatusPedidoAll",
    "group": "Pedido",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "estatus",
            "description": "<p>Estatus a filtrar (Por entregar, Entregado, En ruta, Asignación, etc).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       {\n\t\t    \"status\": true, \n\t\t    \"mensaje\": \"Consulta exitosa\"\n\t\t  }\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Pedido.php",
    "groupTitle": "Pedido"
  },
  {
    "type": "post",
    "url": "/pedidos_status",
    "title": "Cambiar estatus del pedido",
    "version": "0.1.0",
    "name": "PostUpdateStatusPedido",
    "group": "Pedido",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_pedido",
            "description": "<p>ID del pedido.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Estatus a asigar (Por entregar, Entregado, En ruta, etc).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       {\n\t\t    \"status\": true, \n\t\t    \"mensaje\": \"Consulta exitosa\"\n\t\t  }\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Pedido.php",
    "groupTitle": "Pedido"
  },
  {
    "type": "get",
    "url": "/proveedor",
    "title": "Lista de Proveedores.",
    "version": "0.1.0",
    "name": "GetProveedor",
    "group": "Proveedor",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Proveedor.php",
    "groupTitle": "Proveedor"
  },
  {
    "type": "get",
    "url": "/hitorial_compra_proveedores",
    "title": "Historial de compras a proveedores.",
    "version": "0.1.0",
    "name": "GetProveedorHistorial",
    "group": "Proveedor",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Proveedor.php",
    "groupTitle": "Proveedor"
  },
  {
    "type": "post",
    "url": "/proveedor",
    "title": "Crear Proveedor",
    "version": "0.1.0",
    "name": "PostProveedor",
    "group": "Proveedor",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre",
            "description": "<p>Nombre del proveedor.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_identificacion",
            "description": "<p>Numero de identificación (RFC).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "direccion",
            "description": "<p>Dirección del proveedor.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono",
            "description": "<p>Telefono.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "correo",
            "description": "<p>Correo.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Registro exitoso\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Proveedor.php",
    "groupTitle": "Proveedor"
  },
  {
    "type": "get",
    "url": "/rol",
    "title": "Listar de roles",
    "version": "0.1.0",
    "name": "GetRol",
    "group": "Rol",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Rol.php",
    "groupTitle": "Rol"
  },
  {
    "type": "get",
    "url": "/usuario/rol/:id",
    "title": "Listar usuarios por rol",
    "version": "0.1.0",
    "name": "GetUsuarioRol",
    "group": "Rol",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID del rol.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Rol.php",
    "groupTitle": "Rol"
  },
  {
    "type": "get",
    "url": "/rutas",
    "title": "Listar rutas",
    "version": "0.1.0",
    "name": "GetRutas",
    "group": "Rutas",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Ruta.php",
    "groupTitle": "Rutas"
  },
  {
    "type": "get",
    "url": "/ruta/:id",
    "title": "Obtener la ruta por ID",
    "version": "0.1.0",
    "name": "GetRutasID",
    "group": "Rutas",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_usuario",
            "description": "<p>ID del usuario.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Ruta.php",
    "groupTitle": "Rutas"
  },
  {
    "type": "post",
    "url": "/ruta",
    "title": "Crear ruta",
    "version": "0.1.0",
    "name": "PostRutas",
    "group": "Rutas",
    "description": "<p>Crea una ruta por medio del alias enviado o usando uno por defecto, recorre cada elemento del array de distribuidores el mismo debe contener un objeto { id_distribuidor: Integer, colonias: Array<String> }</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "alias",
            "description": "<p>Nombre de la ruta (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "colonias",
            "description": "<p>Lista de nombres de las colonias (Debe ser tipo String cada elemento del array).</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "distribuidores",
            "description": "<p>Lista de id de distribuidores (Debe ser tipo Integer cada elemento del array).</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Ruta.php",
    "groupTitle": "Rutas"
  },
  {
    "type": "post",
    "url": "/ruta/:id",
    "title": "Modificar ruta",
    "version": "0.1.0",
    "name": "PostRutasUpdate",
    "group": "Rutas",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_usuario",
            "description": "<p>ID del usuario (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tx_codigo",
            "description": "<p>Codigo de la ruta (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tx_colonia",
            "description": "<p>Colonia (Opcional).</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Ruta.php",
    "groupTitle": "Rutas"
  },
  {
    "type": "post",
    "url": "/sucursal",
    "title": "Obtener Sucursales",
    "version": "0.1.0",
    "name": "GetSucursal",
    "group": "Sucursal",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Registro exitoso\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Sucursal.php",
    "groupTitle": "Sucursal"
  },
  {
    "type": "post",
    "url": "/sucursal/create",
    "title": "Crear Sucursal",
    "version": "0.1.0",
    "name": "PostSucursal",
    "group": "Sucursal",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tx_nombre",
            "description": "<p>Nombre de la sucursal.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tx_numero",
            "description": "<p>Numero o codigo de la sucursal.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tx_colonia",
            "description": "<p>Colonia a la que se asocia.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tx_codigo_postal",
            "description": "<p>Codigo postal.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Registro exitoso\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Sucursal.php",
    "groupTitle": "Sucursal"
  },
  {
    "type": "get",
    "url": "/equivalencia",
    "title": "Ver equivalencias",
    "version": "0.1.0",
    "name": "GetEquivalencia",
    "group": "Unidad_Metrica",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Registro exitoso\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/UnidadMetrica.php",
    "groupTitle": "Unidad_Metrica"
  },
  {
    "type": "get",
    "url": "/unidad_metrica",
    "title": "Ver unidades metricas",
    "version": "0.1.0",
    "name": "GetUnidadMetrica",
    "group": "Unidad_Metrica",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Registro exitoso\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/UnidadMetrica.php",
    "groupTitle": "Unidad_Metrica"
  },
  {
    "type": "post",
    "url": "/equivalencia",
    "title": "Crear equivalencia",
    "version": "0.1.0",
    "name": "PostEquivalencia",
    "group": "Unidad_Metrica",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_unidad_from",
            "description": "<p>ID de la unidad metrica desde la cual se convertira.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_unidad_to",
            "description": "<p>ID de la unidad metrica a la que se convertira.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "nu_valor",
            "description": "<p>Valor porcentual de conversion (0.9 Kg a Lts).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Registro exitoso\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/UnidadMetrica.php",
    "groupTitle": "Unidad_Metrica"
  },
  {
    "type": "post",
    "url": "/unidad_metrica",
    "title": "Crear unidad metrica",
    "version": "0.1.0",
    "name": "PostUnidadMetrica",
    "group": "Unidad_Metrica",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tx_nombre",
            "description": "<p>Nombre de la unidad metrica.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tx_simbolo",
            "description": "<p>Simbolo de la unidad (Kg, cm3, Lts).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Registro exitoso\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/UnidadMetrica.php",
    "groupTitle": "Unidad_Metrica"
  },
  {
    "type": "get",
    "url": "/clientes_qr",
    "title": "Clientes que no tienen impreso su QR",
    "version": "0.1.0",
    "name": "GetClientesQR",
    "group": "Usuario",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Usuario.php",
    "groupTitle": "Usuario"
  },
  {
    "type": "get",
    "url": "/usuario",
    "title": "Listar todos los usuarios",
    "version": "0.1.0",
    "name": "GetUsuarios",
    "group": "Usuario",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   [{\"id_usuario\":2,\"nombre\":\"Otoniel\",\"apellido\":\"Marquez\",\"correo\":\"otonielmax@hotmail.com\",\"telefono\":\"04264170396\",\"id_rol\":1,\"perfil\":\"Cliente Fisico\"},{\"id_usuario\":4,\"nombre\":\"yusmay\",\"apellido\":\"davila\",\"correo\":\"yus@hot.com\",\"telefono\":\"04265289643\",\"id_rol\":1,\"perfil\":\"Cliente Fisico\"},{\"id_usuario\":3,\"nombre\":\"Distribuidor\",\"apellido\":\"Prueba\",\"correo\":\"distribuidor@hotmail.com\",\"telefono\":\"04241234567\",\"id_rol\":2,\"perfil\":\"Distribuidor\"},{\"id_usuario\":1,\"nombre\":\"Sistema\",\"apellido\":\"Gestion\",\"correo\":\"masgas@masgas.com\",\"telefono\":\"04141234567\",\"id_rol\":3,\"perfil\":\"Super Usuario\"}]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Usuario.php",
    "groupTitle": "Usuario"
  },
  {
    "type": "post",
    "url": "/clientes_qr",
    "title": "Actualizar QR impreso cliente",
    "version": "0.1.0",
    "name": "PostClienteQRImpreso",
    "group": "Usuario",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_usuario",
            "description": "<p>ID de usuario para actualizar el estatus de la impresión del codigo QR (Se puede enviar un array de ID para actualizar por lote).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Se actualizaron los clientes con el QR impreso\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Usuario.php",
    "groupTitle": "Usuario"
  },
  {
    "type": "post",
    "url": "/administrador",
    "title": "Registrar Administrador",
    "version": "0.1.0",
    "name": "PostCreateAdmin",
    "group": "Usuario",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "correo",
            "description": "<p>Correo del usuario.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre1",
            "description": "<p>Primer nombre.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre2",
            "description": "<p>Segundo nombre (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido1",
            "description": "<p>Primer apellido.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido2",
            "description": "<p>Segundo apellido (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_local",
            "description": "<p>Telefono local.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_celular",
            "description": "<p>Telefono celular.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fecha_nacimiento",
            "description": "<p>Fecha de nacimiento (YYYY-MM-DD).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sexo",
            "description": "<p>Sexo (Masculino o Femenino).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "tipo_identificacion",
            "description": "<p>ID tipo de identificación (1 por default).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "identificacion",
            "description": "<p>Numero de documento de identidad.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "direccion",
            "description": "<p>Dirección de envio.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_longitud",
            "description": "<p>Longitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_latitud",
            "description": "<p>Latitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "codigo_postal",
            "description": "<p>Codigo postal.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "colonia",
            "description": "<p>Colonia.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_ext",
            "description": "<p>Numero exterior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_int",
            "description": "<p>Numero interior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Objeto que contiene la información de la solicitud.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       \"status\": true,\n\t\t  \"mensaje\": \"Se creo con exito\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Registro erroneo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Registro erroneo\",\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Usuario.php",
    "groupTitle": "Usuario"
  },
  {
    "type": "post",
    "url": "/callcenter",
    "title": "Registrar operador de call center",
    "version": "0.1.0",
    "name": "PostCreateCallCenter",
    "group": "Usuario",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "correo",
            "description": "<p>Correo del usuario.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre1",
            "description": "<p>Primer nombre.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre2",
            "description": "<p>Segundo nombre (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido1",
            "description": "<p>Primer apellido.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido2",
            "description": "<p>Segundo apellido (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_local",
            "description": "<p>Telefono local.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_celular",
            "description": "<p>Telefono celular.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fecha_nacimiento",
            "description": "<p>Fecha de nacimiento (YYYY-MM-DD).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sexo",
            "description": "<p>Sexo (Masculino o Femenino).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "tipo_identificacion",
            "description": "<p>ID tipo de identificación (1 por default).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "identificacion",
            "description": "<p>Numero de documento de identidad.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "direccion",
            "description": "<p>Dirección de envio.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_longitud",
            "description": "<p>Longitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_latitud",
            "description": "<p>Latitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "codigo_postal",
            "description": "<p>Codigo postal.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "colonia",
            "description": "<p>Colonia.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_ext",
            "description": "<p>Numero exterior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_int",
            "description": "<p>Numero interior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Objeto que contiene la información de la solicitud.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       \"status\": true,\n\t\t  \"mensaje\": \"Se creo con exito\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Registro erroneo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Registro erroneo\",\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Usuario.php",
    "groupTitle": "Usuario"
  },
  {
    "type": "post",
    "url": "/cliente",
    "title": "Registrar Cliente Fisico",
    "version": "0.1.0",
    "name": "PostCreateClienteFisico",
    "group": "Usuario",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "correo",
            "description": "<p>Correo del usuario.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre1",
            "description": "<p>Primer nombre.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre2",
            "description": "<p>Segundo nombre (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido1",
            "description": "<p>Primer apellido.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido2",
            "description": "<p>Segundo apellido (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_local",
            "description": "<p>Telefono local.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_celular",
            "description": "<p>Telefono celular.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fecha_nacimiento",
            "description": "<p>Fecha de nacimiento (YYYY-MM-DD).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sexo",
            "description": "<p>Sexo (Masculino o Femenino).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "tipo_identificacion",
            "description": "<p>ID tipo de identificación (1 por default).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "identificacion",
            "description": "<p>Numero de documento de identidad.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "direccion",
            "description": "<p>Dirección de envio.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_longitud",
            "description": "<p>Longitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_latitud",
            "description": "<p>Latitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "codigo_postal",
            "description": "<p>Codigo postal.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "colonia",
            "description": "<p>Colonia.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_ext",
            "description": "<p>Numero exterior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_int",
            "description": "<p>Numero interior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Objeto que contiene la información de la solicitud.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       \"status\": true,\n\t\t  \"mensaje\": \"Se creo con exito el cliente\",\n       \"data\": {\n\t\t\t\"codigo\": \"PRUEBA123\" \n\t\t  }\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Registro erroneo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Registro erroneo\",\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Usuario.php",
    "groupTitle": "Usuario"
  },
  {
    "type": "post",
    "url": "/cliente_moral",
    "title": "Registrar Cliente moral",
    "version": "0.1.0",
    "name": "PostCreateClienteMoral",
    "group": "Usuario",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "correo",
            "description": "<p>Correo de la empresa.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "empresa",
            "description": "<p>Nombre de la empresa.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_local",
            "description": "<p>Telefono local.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_celular",
            "description": "<p>Telefono celular.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "identificacion",
            "description": "<p>Numero de documento de identidad.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "direccion",
            "description": "<p>Dirección de envio.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_longitud",
            "description": "<p>Longitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_latitud",
            "description": "<p>Latitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "codigo_postal",
            "description": "<p>Codigo postal.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "colonia",
            "description": "<p>Colonia.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_ext",
            "description": "<p>Numero exterior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_int",
            "description": "<p>Numero interior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Objeto que contiene la información de la solicitud.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       \"status\": true,\n\t\t  \"mensaje\": \"Se creo con exito el cliente\",\n       \"data\": {\n\t\t\t\"codigo\": \"PRUEBA123\" \n\t\t  }\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Registro erroneo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Registro erroneo\",\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Usuario.php",
    "groupTitle": "Usuario"
  },
  {
    "type": "post",
    "url": "/distribuidor",
    "title": "Registrar Distribuidor",
    "version": "0.1.0",
    "name": "PostCreateDistribuidor",
    "group": "Usuario",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "correo",
            "description": "<p>Correo del usuario.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre1",
            "description": "<p>Primer nombre.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre2",
            "description": "<p>Segundo nombre (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido1",
            "description": "<p>Primer apellido.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido2",
            "description": "<p>Segundo apellido (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_local",
            "description": "<p>Telefono local.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_celular",
            "description": "<p>Telefono celular.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fecha_nacimiento",
            "description": "<p>Fecha de nacimiento (YYYY-MM-DD).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sexo",
            "description": "<p>Sexo (Masculino o Femenino).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "tipo_identificacion",
            "description": "<p>ID tipo de identificación (1 por default).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "identificacion",
            "description": "<p>Numero de documento de identidad.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "direccion",
            "description": "<p>Dirección de envio.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_longitud",
            "description": "<p>Longitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_latitud",
            "description": "<p>Latitud de la dirección</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "codigo_postal",
            "description": "<p>Codigo postal.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "colonia",
            "description": "<p>Colonia.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_ext",
            "description": "<p>Numero exterior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_int",
            "description": "<p>Numero interior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_vehiculo",
            "description": "<p>ID del vehiculo</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Objeto que contiene la información de la solicitud.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       \"status\": true,\n\t\t  \"mensaje\": \"Se creo con exito el distribuidor\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Registro erroneo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Registro erroneo\",\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Usuario.php",
    "groupTitle": "Usuario"
  },
  {
    "type": "post",
    "url": "/franquicia",
    "title": "Registrar perfil de franquicia",
    "version": "0.1.0",
    "name": "PostCreateFranquicia",
    "group": "Usuario",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "correo",
            "description": "<p>Correo del usuario.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre1",
            "description": "<p>Primer nombre.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre2",
            "description": "<p>Segundo nombre (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido1",
            "description": "<p>Primer apellido.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido2",
            "description": "<p>Segundo apellido (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_local",
            "description": "<p>Telefono local.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_celular",
            "description": "<p>Telefono celular.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fecha_nacimiento",
            "description": "<p>Fecha de nacimiento (YYYY-MM-DD).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sexo",
            "description": "<p>Sexo (Masculino o Femenino).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "tipo_identificacion",
            "description": "<p>ID tipo de identificación (1 por default).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "identificacion",
            "description": "<p>Numero de documento de identidad.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "direccion",
            "description": "<p>Dirección de envio.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_longitud",
            "description": "<p>Longitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_latitud",
            "description": "<p>Latitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "codigo_postal",
            "description": "<p>Codigo postal.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "colonia",
            "description": "<p>Colonia.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_ext",
            "description": "<p>Numero exterior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_int",
            "description": "<p>Numero interior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Objeto que contiene la información de la solicitud.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       \"status\": true,\n\t\t  \"mensaje\": \"Se creo con exito\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Registro erroneo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Registro erroneo\",\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Usuario.php",
    "groupTitle": "Usuario"
  },
  {
    "type": "post",
    "url": "/operador",
    "title": "Registrar perfil de Operador",
    "version": "0.1.0",
    "name": "PostCreateOperador",
    "group": "Usuario",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "correo",
            "description": "<p>Correo del usuario.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre1",
            "description": "<p>Primer nombre.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre2",
            "description": "<p>Segundo nombre (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido1",
            "description": "<p>Primer apellido.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido2",
            "description": "<p>Segundo apellido (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_local",
            "description": "<p>Telefono local.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_celular",
            "description": "<p>Telefono celular.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fecha_nacimiento",
            "description": "<p>Fecha de nacimiento (YYYY-MM-DD).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sexo",
            "description": "<p>Sexo (Masculino o Femenino).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "tipo_identificacion",
            "description": "<p>ID tipo de identificación (1 por default).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "identificacion",
            "description": "<p>Numero de documento de identidad.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "direccion",
            "description": "<p>Dirección de envio.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_longitud",
            "description": "<p>Longitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_latitud",
            "description": "<p>Latitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "codigo_postal",
            "description": "<p>Codigo postal.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "colonia",
            "description": "<p>Colonia.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_ext",
            "description": "<p>Numero exterior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_int",
            "description": "<p>Numero interior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Objeto que contiene la información de la solicitud.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       \"status\": true,\n\t\t  \"mensaje\": \"Se creo con exito\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Registro erroneo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Registro erroneo\",\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Usuario.php",
    "groupTitle": "Usuario"
  },
  {
    "type": "post",
    "url": "/planta",
    "title": "Registrar perfil de planta",
    "version": "0.1.0",
    "name": "PostCreatePlanta",
    "group": "Usuario",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "correo",
            "description": "<p>Correo del usuario.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre1",
            "description": "<p>Primer nombre.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre2",
            "description": "<p>Segundo nombre (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido1",
            "description": "<p>Primer apellido.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido2",
            "description": "<p>Segundo apellido (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_local",
            "description": "<p>Telefono local.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_celular",
            "description": "<p>Telefono celular.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fecha_nacimiento",
            "description": "<p>Fecha de nacimiento (YYYY-MM-DD).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sexo",
            "description": "<p>Sexo (Masculino o Femenino).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "tipo_identificacion",
            "description": "<p>ID tipo de identificación (1 por default).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "identificacion",
            "description": "<p>Numero de documento de identidad.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "direccion",
            "description": "<p>Dirección de envio.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_longitud",
            "description": "<p>Longitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_latitud",
            "description": "<p>Latitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "codigo_postal",
            "description": "<p>Codigo postal.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "colonia",
            "description": "<p>Colonia.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_ext",
            "description": "<p>Numero exterior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_int",
            "description": "<p>Numero interior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Objeto que contiene la información de la solicitud.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       \"status\": true,\n\t\t  \"mensaje\": \"Se creo con exito\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Registro erroneo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Registro erroneo\",\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Usuario.php",
    "groupTitle": "Usuario"
  },
  {
    "type": "post",
    "url": "/rrhh",
    "title": "Registrar perfil de RRHH",
    "version": "0.1.0",
    "name": "PostCreateRRHH",
    "group": "Usuario",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "correo",
            "description": "<p>Correo del usuario.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre1",
            "description": "<p>Primer nombre.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre2",
            "description": "<p>Segundo nombre (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido1",
            "description": "<p>Primer apellido.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido2",
            "description": "<p>Segundo apellido (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_local",
            "description": "<p>Telefono local.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_celular",
            "description": "<p>Telefono celular.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fecha_nacimiento",
            "description": "<p>Fecha de nacimiento (YYYY-MM-DD).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sexo",
            "description": "<p>Sexo (Masculino o Femenino).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "tipo_identificacion",
            "description": "<p>ID tipo de identificación (1 por default).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "identificacion",
            "description": "<p>Numero de documento de identidad.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "direccion",
            "description": "<p>Dirección de envio.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_longitud",
            "description": "<p>Longitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_latitud",
            "description": "<p>Latitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "codigo_postal",
            "description": "<p>Codigo postal.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "colonia",
            "description": "<p>Colonia.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_ext",
            "description": "<p>Numero exterior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_int",
            "description": "<p>Numero interior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Objeto que contiene la información de la solicitud.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       \"status\": true,\n\t\t  \"mensaje\": \"Se creo con exito\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Registro erroneo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Registro erroneo\",\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Usuario.php",
    "groupTitle": "Usuario"
  },
  {
    "type": "post",
    "url": "/staff",
    "title": "Registrar perfil de Staff",
    "version": "0.1.0",
    "name": "PostCreateStaff",
    "group": "Usuario",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "correo",
            "description": "<p>Correo del usuario.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre1",
            "description": "<p>Primer nombre.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre2",
            "description": "<p>Segundo nombre (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido1",
            "description": "<p>Primer apellido.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido2",
            "description": "<p>Segundo apellido (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_local",
            "description": "<p>Telefono local.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_celular",
            "description": "<p>Telefono celular.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fecha_nacimiento",
            "description": "<p>Fecha de nacimiento (YYYY-MM-DD).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sexo",
            "description": "<p>Sexo (Masculino o Femenino).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "tipo_identificacion",
            "description": "<p>ID tipo de identificación (1 por default).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "identificacion",
            "description": "<p>Numero de documento de identidad.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "direccion",
            "description": "<p>Dirección de envio.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_longitud",
            "description": "<p>Longitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_latitud",
            "description": "<p>Latitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "codigo_postal",
            "description": "<p>Codigo postal.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "colonia",
            "description": "<p>Colonia.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_ext",
            "description": "<p>Numero exterior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_int",
            "description": "<p>Numero interior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Objeto que contiene la información de la solicitud.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       \"status\": true,\n\t\t  \"mensaje\": \"Se creo con exito\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Registro erroneo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Registro erroneo\",\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Usuario.php",
    "groupTitle": "Usuario"
  },
  {
    "type": "post",
    "url": "/sucursal",
    "title": "Registrar perfil de sucursal",
    "version": "0.1.0",
    "name": "PostCreateSucursal",
    "group": "Usuario",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "correo",
            "description": "<p>Correo del usuario.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre1",
            "description": "<p>Primer nombre.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre2",
            "description": "<p>Segundo nombre (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido1",
            "description": "<p>Primer apellido.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido2",
            "description": "<p>Segundo apellido (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_local",
            "description": "<p>Telefono local.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_celular",
            "description": "<p>Telefono celular.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fecha_nacimiento",
            "description": "<p>Fecha de nacimiento (YYYY-MM-DD).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sexo",
            "description": "<p>Sexo (Masculino o Femenino).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "tipo_identificacion",
            "description": "<p>ID tipo de identificación (1 por default).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "identificacion",
            "description": "<p>Numero de documento de identidad.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "direccion",
            "description": "<p>Dirección de envio.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_longitud",
            "description": "<p>Longitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_latitud",
            "description": "<p>Latitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "codigo_postal",
            "description": "<p>Codigo postal.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "colonia",
            "description": "<p>Colonia.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_ext",
            "description": "<p>Numero exterior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_int",
            "description": "<p>Numero interior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Objeto que contiene la información de la solicitud.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       \"status\": true,\n\t\t  \"mensaje\": \"Se creo con exito\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Registro erroneo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Registro erroneo\",\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Usuario.php",
    "groupTitle": "Usuario"
  },
  {
    "type": "post",
    "url": "/supervisor",
    "title": "Registrar perfil de supervisor",
    "version": "0.1.0",
    "name": "PostCreateSupervisor",
    "group": "Usuario",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "correo",
            "description": "<p>Correo del usuario.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre1",
            "description": "<p>Primer nombre.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre2",
            "description": "<p>Segundo nombre (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido1",
            "description": "<p>Primer apellido.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido2",
            "description": "<p>Segundo apellido (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_local",
            "description": "<p>Telefono local.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telefono_celular",
            "description": "<p>Telefono celular.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fecha_nacimiento",
            "description": "<p>Fecha de nacimiento (YYYY-MM-DD).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sexo",
            "description": "<p>Sexo (Masculino o Femenino).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "tipo_identificacion",
            "description": "<p>ID tipo de identificación (1 por default).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "identificacion",
            "description": "<p>Numero de documento de identidad.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "direccion",
            "description": "<p>Dirección de envio.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_longitud",
            "description": "<p>Longitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "direccion_latitud",
            "description": "<p>Latitud de la dirección.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "codigo_postal",
            "description": "<p>Codigo postal.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "colonia",
            "description": "<p>Colonia.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_ext",
            "description": "<p>Numero exterior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "numero_int",
            "description": "<p>Numero interior (Opcional).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Objeto que contiene la información de la solicitud.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       \"status\": true,\n\t\t  \"mensaje\": \"Se creo con exito\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Registro erroneo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Registro erroneo\",\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Usuario.php",
    "groupTitle": "Usuario"
  },
  {
    "type": "post",
    "url": "/login",
    "title": "Iniciar Sesión",
    "version": "0.1.0",
    "name": "PostLogin",
    "group": "Usuario",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tx_correo",
            "description": "<p>Correo del usuario.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tx_password",
            "description": "<p>El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el registro fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Objeto que contiene los datos de usuario, dirección y articulos.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       \"status\": true,\n       \"data\": {\n\t\t\t\"usuario\": [], \n\t\t\t\"direccion\": [],\n\t\t\t\"articulos\": []\n\t\t  }\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Las credenciales no corresponden a ninguno de nuestros usuarios\"\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Usuario.php",
    "groupTitle": "Usuario"
  },
  {
    "type": "post",
    "url": "/usuario/nuevo_password",
    "title": "Nuevo password",
    "version": "0.1.0",
    "name": "PostNuevoPassword",
    "group": "Usuario",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>El password debe estar antecedido de la palabra MASGAS con encriptación CRYPT.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_usuario",
            "description": "<p>ID de usuari.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Se completo la petición para la recuperación\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Usuario.php",
    "groupTitle": "Usuario"
  },
  {
    "type": "post",
    "url": "/usuario/recuperar_password",
    "title": "Recuperar password",
    "version": "0.1.0",
    "name": "PostRecuperarPassword",
    "group": "Usuario",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "correo",
            "description": "<p>Correo del usuario.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Objeto que contiene los datos de usuario, dirección y articulos.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       \"status\": true,\n       \"mensaje\": \"Se completo la petición para la recuperación\",\n       \"data\": {\n\t\t\t\"usuario\": []\n\t\t  }\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Usuario.php",
    "groupTitle": "Usuario"
  },
  {
    "type": "put",
    "url": "/estatus/:id",
    "title": "Activa o desactiva un usuario",
    "version": "0.1.0",
    "name": "PutEstatusUser",
    "group": "Usuario",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>ID de usuario.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "estado",
            "description": "<p>true o false.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n       \"status\": true,\n\t\t  \"mensaje\": \"Se cambio el estatus con exito del usuario\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>No se consigio el usuario con el id suministrado</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"No se consigio el usuario con el id suministrado\"\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Usuario.php",
    "groupTitle": "Usuario"
  },
  {
    "type": "get",
    "url": "/tipo_vehiculo",
    "title": "Lista de tipos de vehiculos.",
    "version": "0.1.0",
    "name": "GeTipoVehiculos",
    "group": "Vehiculo",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Vehiculo.php",
    "groupTitle": "Vehiculo"
  },
  {
    "type": "get",
    "url": "/monitoreo",
    "title": "Obtener el monitoreo",
    "version": "0.1.0",
    "name": "GetMonitoreo",
    "group": "Vehiculo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_usuario",
            "description": "<p>(Opcional) ID del usuario poseedor del vehiculo.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fecha",
            "description": "<p>(Opcional) Fecha inicial desde donde quieres obtener el monitoreo.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fecha2",
            "description": "<p>Fecha final hasta donde quieres obtener la ruta.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Vehiculo.php",
    "groupTitle": "Vehiculo"
  },
  {
    "type": "get",
    "url": "/vehiculos",
    "title": "Lista de vehiculos.",
    "version": "0.1.0",
    "name": "GetVehiculos",
    "group": "Vehiculo",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Vehiculo.php",
    "groupTitle": "Vehiculo"
  },
  {
    "type": "post",
    "url": "/monitoreo",
    "title": "Registrar nueva posición",
    "version": "0.1.0",
    "name": "PostMonitoreo",
    "group": "Vehiculo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_usuario",
            "description": "<p>ID del usuario.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "nu_latitud",
            "description": "<p>Latitud de la posición registrada.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "nu_longitud",
            "description": "<p>Longitud de la posición registrada.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_vehiculo",
            "description": "<p>ID del vehiculo.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Registro exitoso\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Vehiculo.php",
    "groupTitle": "Vehiculo"
  },
  {
    "type": "post",
    "url": "/tipo_vehiculo",
    "title": "Crear Tipo de Vehiculo",
    "version": "0.1.0",
    "name": "PostTipoVehiculo",
    "group": "Vehiculo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tx_nombre",
            "description": "<p>Nombre o denominacion del tipo.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Registro exitoso\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Vehiculo.php",
    "groupTitle": "Vehiculo"
  },
  {
    "type": "post",
    "url": "/vehiculo/create",
    "title": "Crear Vehiculo",
    "version": "0.1.0",
    "name": "PostVehiculo",
    "group": "Vehiculo",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_tipo_vehiculo",
            "description": "<p>ID del tipo de vehiculo.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "tx_numero_identificacion",
            "description": "<p>Numero de identificación o placa.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_usuario",
            "description": "<p>ID Usuario (Opcional).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Registro exitoso\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Vehiculo.php",
    "groupTitle": "Vehiculo"
  },
  {
    "type": "get",
    "url": "/viaticos",
    "title": "Viaticos cargados.",
    "version": "0.1.0",
    "name": "GetViaticos",
    "group": "Viaticos",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Problema en solicitud</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n\n}",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Viaticos.php",
    "groupTitle": "Viaticos"
  },
  {
    "type": "post",
    "url": "/viaticos",
    "title": "Cargar viaticos",
    "version": "0.1.0",
    "name": "PostViaticos",
    "group": "Viaticos",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id_usuario",
            "description": "<p>ID de usuario.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "monto",
            "description": "<p>Monto de la factura o del consumo.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fecha",
            "description": "<p>Fecha de la factura o del consumo.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "factura",
            "description": "<p>Codigo de la factura (Opcional en caso de que se posea).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>true o false dependiendo de si el proceso fue exitoso o no respectivamente.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mensaje",
            "description": "<p>Devolvera un mensaje de acuerdo a lo acontecido.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": true,\n   \"mensaje\": \"Registro exitoso\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserBadResponse",
            "description": "<p>Las credenciales no corresponden a ninguno de nuestros usuarios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 404 Not Found\n    {\n\t\t \"status\": false,\n      \"mensaje\": \"Problemas para procesar solicitud\"\n      \"errors\": []\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "clases/Viaticos.php",
    "groupTitle": "Viaticos"
  }
] });
