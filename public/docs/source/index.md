---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)
<!-- END_INFO -->

#Articulo

Modulo de Articulo
<!-- START_966731f1d2b84afd6be5b115bb45937f -->
## Muestra todos los articulos

> Example request:

```bash
curl -X GET "http://localhost/api/v1/articulo" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/articulo",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
[
    {
        "id": 1,
        "tx_nombre": "Bombona 10 Kg",
        "nu_precio": 5000,
        "nu_cantidad": 10
    },
    {
        "id": 2,
        "tx_nombre": "Bombona 18 Kg",
        "nu_precio": 8000,
        "nu_cantidad": 15
    },
    {
        "id": 3,
        "tx_nombre": "Bombona 30 Kg",
        "nu_precio": 12000,
        "nu_cantidad": 8
    }
]
```

### HTTP Request
`GET api/v1/articulo`

`HEAD api/v1/articulo`


<!-- END_966731f1d2b84afd6be5b115bb45937f -->

<!-- START_2d2c7ab4a765f1eadf524a18765d7579 -->
## Crear nuevo articulo

> Example request:

```bash
curl -X POST "http://localhost/api/v1/articulo" \
-H "Accept: application/json" \
    -d "tx_nombre"="voluptatem" \
    -d "nu_precio"="467" \
    -d "nu_cantidad"="467" \
    -d "id_usuario"="467" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/articulo",
    "method": "POST",
    "data": {
        "tx_nombre": "voluptatem",
        "nu_precio": 467,
        "nu_cantidad": 467,
        "id_usuario": 467
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/articulo`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    tx_nombre | string |  required  | 
    nu_precio | numeric |  required  | 
    nu_cantidad | integer |  required  | 
    id_usuario | integer |  required  | 

<!-- END_2d2c7ab4a765f1eadf524a18765d7579 -->

<!-- START_2f3e1deb868dedc10b7ca7ddbb46ff63 -->
## Mostrar un articulo especifico.

> Example request:

```bash
curl -X GET "http://localhost/api/v1/articulo/{articulo}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/articulo/{articulo}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "detalle": true,
    "articulo": {
        "id": 1,
        "tx_nombre": "Bombona 10 Kg",
        "nu_precio": 5000,
        "nu_cantidad": 10
    }
}
```

### HTTP Request
`GET api/v1/articulo/{articulo}`

`HEAD api/v1/articulo/{articulo}`


<!-- END_2f3e1deb868dedc10b7ca7ddbb46ff63 -->

<!-- START_a897d019f97f2969d492b43bf29affef -->
## Modificar un articulo.

> Example request:

```bash
curl -X PUT "http://localhost/api/v1/articulo/{articulo}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/articulo/{articulo}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/articulo/{articulo}`

`PATCH api/v1/articulo/{articulo}`


<!-- END_a897d019f97f2969d492b43bf29affef -->

#Direccion

Modulo de Direccion
<!-- START_de2995e1c71c7037cd11ab9035a81aa5 -->
## Muestra todas las direcciones registrada por los clientes

> Example request:

```bash
curl -X GET "http://localhost/api/v1/direccion" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/direccion",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
[
    {
        "id": 1,
        "tx_nombre": "Casa",
        "nu_latitud": 16.77,
        "nu_longitud": -93.04,
        "id_usuario": 2,
        "id_zona": 4
    }
]
```

### HTTP Request
`GET api/v1/direccion`

`HEAD api/v1/direccion`


<!-- END_de2995e1c71c7037cd11ab9035a81aa5 -->

<!-- START_e71a439962df05fee00568f2848dbf71 -->
## Crear una nueva direccion para un usuario.

> Example request:

```bash
curl -X POST "http://localhost/api/v1/direccion" \
-H "Accept: application/json" \
    -d "tx_nombre"="voluptatem" \
    -d "id_usuario"="-2" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/direccion",
    "method": "POST",
    "data": {
        "tx_nombre": "voluptatem",
        "id_usuario": -2
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/direccion`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    tx_nombre | string |  required  | 
    id_usuario | integer |  required  | 

<!-- END_e71a439962df05fee00568f2848dbf71 -->

<!-- START_085d7c0924fc641a1dc3895d56a6239d -->
## Muestra una direccion relacionada al id suministrado

> Example request:

```bash
curl -X GET "http://localhost/api/v1/direccion/{direccion}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/direccion/{direccion}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "detalle": true,
    "direccion": {
        "id": 1,
        "tx_nombre": "Casa",
        "nu_latitud": 16.77,
        "nu_longitud": -93.04,
        "id_usuario": 2,
        "id_zona": 4
    }
}
```

### HTTP Request
`GET api/v1/direccion/{direccion}`

`HEAD api/v1/direccion/{direccion}`


<!-- END_085d7c0924fc641a1dc3895d56a6239d -->

<!-- START_507a41d1aa243b470e68f8f817335a85 -->
## Modifica una direccion.

> Example request:

```bash
curl -X PUT "http://localhost/api/v1/direccion/{direccion}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/direccion/{direccion}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/direccion/{direccion}`

`PATCH api/v1/direccion/{direccion}`


<!-- END_507a41d1aa243b470e68f8f817335a85 -->

<!-- START_4c66a9a77b4ebf88b7470acecd20bf84 -->
## Muestra la distribucion geopolitica

> Example request:

```bash
curl -X GET "http://localhost/api/v1/direccion/territorio" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/direccion/territorio",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "detalle": false,
    "error": "No se consigio la direccion con el id suministrado"
}
```

### HTTP Request
`GET api/v1/direccion/territorio`

`HEAD api/v1/direccion/territorio`


<!-- END_4c66a9a77b4ebf88b7470acecd20bf84 -->

<!-- START_c6baed9a469e18c1e4bdf45406d29503 -->
## Clientes por zona

Recibe el id de la zona para filtar los usuarios que pertenezcan a dicha zona

> Example request:

```bash
curl -X GET "http://localhost/api/v1/direccion/cliente/zona/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/direccion/cliente/zona/{id}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "detalle": false,
    "error": "No se consigieron clientes con el id de zona suministrado"
}
```

### HTTP Request
`GET api/v1/direccion/cliente/zona/{id}`

`HEAD api/v1/direccion/cliente/zona/{id}`


<!-- END_c6baed9a469e18c1e4bdf45406d29503 -->

<!-- START_8941b9a98ba2e9482c50e58db810b71d -->
## Muestra las direcciones de un usuario

> Example request:

```bash
curl -X GET "http://localhost/api/v1/direccion/cliente/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/direccion/cliente/{id}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "detalle": false,
    "error": "No se consigieron direcciones con el id de usuario suministrado"
}
```

### HTTP Request
`GET api/v1/direccion/cliente/{id}`

`HEAD api/v1/direccion/cliente/{id}`


<!-- END_8941b9a98ba2e9482c50e58db810b71d -->

#Pedido

Modulo de Pedido
<!-- START_372bdc5aceae1a582b78ea33326a8528 -->
## Muestra todos los pedidos

> Example request:

```bash
curl -X GET "http://localhost/api/v1/pedido" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/pedido",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
[
    {
        "id": 1,
        "id_pedido_grupo": 1,
        "id_articulo": 1,
        "nu_cantidad": 3,
        "nu_subtotal": 15000
    },
    {
        "id": 2,
        "id_pedido_grupo": 1,
        "id_articulo": 2,
        "nu_cantidad": 1,
        "nu_subtotal": 16000
    },
    {
        "id": 3,
        "id_pedido_grupo": 1,
        "id_articulo": 3,
        "nu_cantidad": 2,
        "nu_subtotal": 24000
    }
]
```

### HTTP Request
`GET api/v1/pedido`

`HEAD api/v1/pedido`


<!-- END_372bdc5aceae1a582b78ea33326a8528 -->

<!-- START_ef48129a4cc799526cc04abfe9250fad -->
## Crear nuevo pedido

> Example request:

```bash
curl -X POST "http://localhost/api/v1/pedido" \
-H "Accept: application/json" \
    -d "id_direccion"="-4425" \
    -d "bo_pagada"="1" \
    -d "id_articulo"="esse" \
    -d "nu_cantidad"="esse" \
    -d "nu_subtotal"="esse" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/pedido",
    "method": "POST",
    "data": {
        "id_direccion": -4425,
        "bo_pagada": true,
        "id_articulo": "esse",
        "nu_cantidad": "esse",
        "nu_subtotal": "esse"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/pedido`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    id_direccion | integer |  required  | 
    bo_pagada | boolean |  required  | 
    id_articulo | array |  required  | 
    nu_cantidad | array |  required  | 
    nu_subtotal | array |  required  | 

<!-- END_ef48129a4cc799526cc04abfe9250fad -->

<!-- START_de7aad3deca9be304f1537f28b126cc1 -->
## Mostrar un pedido especifico.

> Example request:

```bash
curl -X GET "http://localhost/api/v1/pedido/{pedido}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/pedido/{pedido}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "detalle": true,
    "pedido": {
        "id": 1,
        "id_pedido_grupo": 1,
        "id_articulo": 1,
        "nu_cantidad": 3,
        "nu_subtotal": 15000
    }
}
```

### HTTP Request
`GET api/v1/pedido/{pedido}`

`HEAD api/v1/pedido/{pedido}`


<!-- END_de7aad3deca9be304f1537f28b126cc1 -->

<!-- START_fb05dd2fed2750b67eeddc67ef5e17e9 -->
## Modificar un pedido.

> Example request:

```bash
curl -X PUT "http://localhost/api/v1/pedido/{pedido}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/pedido/{pedido}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/pedido/{pedido}`

`PATCH api/v1/pedido/{pedido}`


<!-- END_fb05dd2fed2750b67eeddc67ef5e17e9 -->

<!-- START_6a9e4ef470574d17812ae87751a3a18c -->
## Muestra los pedidos de un cliente

> Example request:

```bash
curl -X GET "http://localhost/api/v1/pedido/cliente/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/pedido/cliente/{id}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "detalle": false,
    "error": "No se consiguieron pedidos con el id del cliente suministrado"
}
```

### HTTP Request
`GET api/v1/pedido/cliente/{id}`

`HEAD api/v1/pedido/cliente/{id}`


<!-- END_6a9e4ef470574d17812ae87751a3a18c -->

#Ruta

Modulo de Ruta
<!-- START_a8880493ffffc12235062a1ea025eac1 -->
## Muestra todas las rutas

> Example request:

```bash
curl -X GET "http://localhost/api/v1/ruta" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/ruta",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
[
    {
        "id_ruta": 1,
        "codigo": "001",
        "apellido1": "Prueba",
        "nombre1": "Distribuidor",
        "id_usuario": 3
    }
]
```

### HTTP Request
`GET api/v1/ruta`

`HEAD api/v1/ruta`


<!-- END_a8880493ffffc12235062a1ea025eac1 -->

<!-- START_0937a8b80363f33589f6905aa41b67a5 -->
## Crear nueva ruta

> Example request:

```bash
curl -X POST "http://localhost/api/v1/ruta" \
-H "Accept: application/json" \
    -d "id_usuario"="nisi" \
    -d "tx_codigo"="nisi" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/ruta",
    "method": "POST",
    "data": {
        "id_usuario": "nisi",
        "tx_codigo": "nisi"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/ruta`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    id_usuario | string |  required  | 
    tx_codigo | string |  required  | 

<!-- END_0937a8b80363f33589f6905aa41b67a5 -->

<!-- START_4e476e5378f2ad7d11da9d61cbb830bc -->
## Mostrar una ruta especifica.

> Example request:

```bash
curl -X GET "http://localhost/api/v1/ruta/{rutum}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/ruta/{rutum}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "detalle": true,
    "ruta": {
        "id": 1,
        "tx_codigo": "001",
        "id_usuario": 3
    }
}
```

### HTTP Request
`GET api/v1/ruta/{rutum}`

`HEAD api/v1/ruta/{rutum}`


<!-- END_4e476e5378f2ad7d11da9d61cbb830bc -->

<!-- START_9c5dd94dccba1cb1dee34fdd849a6f4d -->
## Modificar una ruta.

> Example request:

```bash
curl -X PUT "http://localhost/api/v1/ruta/{rutum}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/ruta/{rutum}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/ruta/{rutum}`

`PATCH api/v1/ruta/{rutum}`


<!-- END_9c5dd94dccba1cb1dee34fdd849a6f4d -->

<!-- START_585c95ed8c0a3bbc5d3346cc44aeac4f -->
## Relacionar rutas a direcciones de cliente

> Example request:

```bash
curl -X POST "http://localhost/api/v1/ruta/relacionar" \
-H "Accept: application/json" \
    -d "id_ruta"="26" \
    -d "direccion_cliente"="amet" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/ruta/relacionar",
    "method": "POST",
    "data": {
        "id_ruta": 26,
        "direccion_cliente": "amet"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/ruta/relacionar`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    id_ruta | integer |  required  | 
    direccion_cliente | array |  required  | 

<!-- END_585c95ed8c0a3bbc5d3346cc44aeac4f -->

#Usuario

Modulo de Usuario
<!-- START_8c0e48cd8efa861b308fc45872ff0837 -->
## Inicio de Sesión

> Example request:

```bash
curl -X POST "http://localhost/api/v1/login" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/login",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/login`


<!-- END_8c0e48cd8efa861b308fc45872ff0837 -->

<!-- START_7d374fe8a9e188993e5e5f61800931c5 -->
## Crear nuevo distribuidor

> Example request:

```bash
curl -X POST "http://localhost/api/v1/distribuidor" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/distribuidor",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/distribuidor`


<!-- END_7d374fe8a9e188993e5e5f61800931c5 -->

<!-- START_985c36796aa1852100a3ab1c38b423f6 -->
## Crear nuevo cliente

> Example request:

```bash
curl -X POST "http://localhost/api/v1/cliente" \
-H "Accept: application/json" \
    -d "tx_alias"="quam" \
    -d "password"="quam" \
    -d "nombre1"="quam" \
    -d "apellido1"="quam" \
    -d "telefono_celular"="quam" \
    -d "telefono_local"="quam" \
    -d "correo"="guido.jacobson@example.com" \
    -d "sexo"="quam" \
    -d "fecha_nacimiento"="quam" \
    -d "identificacion"="quam" \
    -d "tipo_identificacion"="quam" \
    -d "direccion"="quam" \
    -d "direccion_longitud"="quam" \
    -d "direccion_latitud"="quam" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/cliente",
    "method": "POST",
    "data": {
        "tx_alias": "quam",
        "password": "quam",
        "nombre1": "quam",
        "apellido1": "quam",
        "telefono_celular": "quam",
        "telefono_local": "quam",
        "correo": "guido.jacobson@example.com",
        "sexo": "quam",
        "fecha_nacimiento": "quam",
        "identificacion": "quam",
        "tipo_identificacion": "quam",
        "direccion": "quam",
        "direccion_longitud": "quam",
        "direccion_latitud": "quam"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/cliente`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    tx_alias | string |  required  | 
    password | string |  required  | 
    nombre1 | string |  required  | 
    apellido1 | string |  required  | 
    telefono_celular | string |  required  | 
    telefono_local | string |  required  | 
    correo | email |  required  | 
    sexo | string |  required  | 
    fecha_nacimiento | string |  required  | 
    identificacion | string |  required  | 
    tipo_identificacion | string |  required  | 
    direccion | string |  required  | 
    direccion_longitud | string |  required  | 
    direccion_latitud | string |  required  | 

<!-- END_985c36796aa1852100a3ab1c38b423f6 -->

<!-- START_e9b0c3e949b102d6956248abae6d6fe3 -->
## Cambia el estado de un usuario.

Activa o desactiva un usuario

> Example request:

```bash
curl -X POST "http://localhost/api/v1/estatus/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/estatus/{id}",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/estatus/{id}`


<!-- END_e9b0c3e949b102d6956248abae6d6fe3 -->

<!-- START_c5ca9c22c56d9a70f5fa343569d466a9 -->
## Pre Registro

Debe ser consumido antes de registrar un cliente o despachador, ya que tiene los datos necesarios para dicho registro

> Example request:

```bash
curl -X GET "http://localhost/api/v1/registrar" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/registrar",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "rol": [
        {
            "id": 1,
            "tx_nombre": "Cliente"
        },
        {
            "id": 2,
            "tx_nombre": "Distribuidor"
        },
        {
            "id": 3,
            "tx_nombre": "Super Usuario"
        },
        {
            "id": 4,
            "tx_nombre": "Administrador"
        }
    ],
    "identificacion": [
        {
            "id": 1,
            "tx_nombre": "Cedula"
        },
        {
            "id": 2,
            "tx_nombre": "Pasaporte"
        },
        {
            "id": 3,
            "tx_nombre": "DNI"
        }
    ]
}
```

### HTTP Request
`GET api/v1/registrar`

`HEAD api/v1/registrar`


<!-- END_c5ca9c22c56d9a70f5fa343569d466a9 -->

<!-- START_3fbf6f46beb52637c64c91e891387c39 -->
## Muestra todos los usuarios

> Example request:

```bash
curl -X GET "http://localhost/api/v1/usuario" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/usuario",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
[
    {
        "id": 1,
        "tx_nombre1": "Sistema",
        "tx_nombre2": "",
        "tx_apellido1": "Gestion",
        "tx_apellido2": "",
        "id_tipo_identificacion": 1,
        "nu_identificacion": 87654321,
        "id_rol": 3,
        "tx_sexo": "Hombre",
        "dt_fecha_nacimiento": "2017-10-01",
        "bo_activo": 1,
        "tx_correo": "masgas@masgas.com",
        "tx_telefono_celular": "04141234567",
        "tx_telefono_local": "02121234567",
        "tx_alias": "sistema",
        "tx_password": "123",
        "tx_codigo": "Dg42PR38"
    },
    {
        "id": 2,
        "tx_nombre1": "Otoniel",
        "tx_nombre2": "Enrique",
        "tx_apellido1": "Marquez",
        "tx_apellido2": "Mejias",
        "id_tipo_identificacion": 1,
        "nu_identificacion": 21091361,
        "id_rol": 1,
        "tx_sexo": "Hombre",
        "dt_fecha_nacimiento": "1992-10-05",
        "bo_activo": 1,
        "tx_correo": "otonielmax@hotmail.com",
        "tx_telefono_celular": "04264170396",
        "tx_telefono_local": "02128343743",
        "tx_alias": "otonielmax",
        "tx_password": "123",
        "tx_codigo": "55FjuVEI"
    },
    {
        "id": 3,
        "tx_nombre1": "Distribuidor",
        "tx_nombre2": "",
        "tx_apellido1": "Prueba",
        "tx_apellido2": "",
        "id_tipo_identificacion": 1,
        "nu_identificacion": 123456789,
        "id_rol": 2,
        "tx_sexo": "Hombre",
        "dt_fecha_nacimiento": "2017-10-10",
        "bo_activo": 1,
        "tx_correo": "distribuidor@hotmail.com",
        "tx_telefono_celular": "04241234567",
        "tx_telefono_local": "02121234567",
        "tx_alias": "distribuidor",
        "tx_password": "123",
        "tx_codigo": "jTWz1fR7"
    }
]
```

### HTTP Request
`GET api/v1/usuario`

`HEAD api/v1/usuario`


<!-- END_3fbf6f46beb52637c64c91e891387c39 -->

<!-- START_6fae4109d3d82b76bdde5f82acafa974 -->
## Mostrar un usuario especifico.

> Example request:

```bash
curl -X GET "http://localhost/api/v1/usuario/{usuario}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/usuario/{usuario}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "detalle": true,
    "usuario": {
        "id": 1,
        "tx_nombre1": "Sistema",
        "tx_nombre2": "",
        "tx_apellido1": "Gestion",
        "tx_apellido2": "",
        "id_tipo_identificacion": 1,
        "nu_identificacion": 87654321,
        "id_rol": 3,
        "tx_sexo": "Hombre",
        "dt_fecha_nacimiento": "2017-10-01",
        "bo_activo": 1,
        "tx_correo": "masgas@masgas.com",
        "tx_telefono_celular": "04141234567",
        "tx_telefono_local": "02121234567",
        "tx_alias": "sistema",
        "tx_password": "123",
        "tx_codigo": "Dg42PR38"
    }
}
```

### HTTP Request
`GET api/v1/usuario/{usuario}`

`HEAD api/v1/usuario/{usuario}`


<!-- END_6fae4109d3d82b76bdde5f82acafa974 -->

<!-- START_c02e1a4265c5705efafba684b78f89df -->
## Modificar un usuario.

> Example request:

```bash
curl -X PUT "http://localhost/api/v1/usuario/{usuario}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/usuario/{usuario}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/v1/usuario/{usuario}`

`PATCH api/v1/usuario/{usuario}`


<!-- END_c02e1a4265c5705efafba684b78f89df -->

<!-- START_8b57959c480d69010d1afa2a2175073f -->
## Muestra los usuarios por rol

> Example request:

```bash
curl -X GET "http://localhost/api/v1/usuario/rol/{id}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/usuario/rol/{id}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
[
    {
        "id": 2,
        "tx_nombre1": "Otoniel",
        "tx_nombre2": "Enrique",
        "tx_apellido1": "Marquez",
        "tx_apellido2": "Mejias",
        "id_tipo_identificacion": 1,
        "nu_identificacion": 21091361,
        "id_rol": 1,
        "tx_sexo": "Hombre",
        "dt_fecha_nacimiento": "1992-10-05",
        "bo_activo": 1,
        "tx_correo": "otonielmax@hotmail.com",
        "tx_telefono_celular": "04264170396",
        "tx_telefono_local": "02128343743",
        "tx_alias": "otonielmax",
        "tx_password": "123",
        "tx_codigo": "55FjuVEI"
    }
]
```

### HTTP Request
`GET api/v1/usuario/rol/{id}`

`HEAD api/v1/usuario/rol/{id}`


<!-- END_8b57959c480d69010d1afa2a2175073f -->

