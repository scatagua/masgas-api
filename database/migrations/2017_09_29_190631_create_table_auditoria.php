<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAuditoria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* 
            Modulos
            1- Usuario, 2- Distribuidor, 3- Articulo, 4- Pedido, 5- Direccion, 6- Ruta

            Accion
            1- Crear, 2- Modificar, 3- Eliminar, 4- Consulta, 5- Relacionar
        */

        Schema::create('auditoria', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nu_modulo');
            $table->integer('nu_identificador');
            $table->integer('nu_accion');
            $table->date('dt_fecha_accion');
            $table->integer('id_usuario_accion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('auditoria');
    }
}
