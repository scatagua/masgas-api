<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePedidoGrupo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido_grupo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_direccion')->unsigned();
            $table->foreign('id_direccion')->references('id')->on('direccion');
            $table->date('dt_fecha');
            $table->string('tx_codigo');
            $table->boolean('bo_pagada');
            $table->string('tx_estatus');
            $table->string('tx_motivo')->nullable();
            $table->integer('id_distribuidor')->unsigned();
            $table->date('dt_fecha_asignado')->nullable();
            $table->string('tx_vendido_en');
            $table->string('tx_tipo_pago')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedido_grupo');
    }
}
