<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNotificacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notificacion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_usuario_emisor')->unsigned();
            $table->foreign('id_usuario_emisor')->references('id')->on('usuario');
            $table->integer('id_usuario_receptor')->unsigned();
            $table->foreign('id_usuario_receptor')->references('id')->on('usuario');
            $table->integer('id_mensaje')->unsigned();
            $table->foreign('id_mensaje')->references('id')->on('notificacion_mensaje');
            $table->string('tx_cuerpo');
            $table->date('dt_fecha');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notificacion');
    }
}
