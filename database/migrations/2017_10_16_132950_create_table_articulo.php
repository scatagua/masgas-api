<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableArticulo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tx_nombre');
            $table->float('nu_precio');
            $table->integer('id_articulo_categoria')->unsigned();
            $table->foreign('id_articulo_categoria')->references('id')->on('articulo_categoria');
            $table->integer('nu_inventario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articulo');
    }
}
