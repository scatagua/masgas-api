<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePedido extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pedido_grupo')->unsigned();
            $table->foreign('id_pedido_grupo')->references('id')->on('pedido_grupo');
            $table->integer('id_articulo')->unsigned();
            $table->foreign('id_articulo')->references('id')->on('articulo');
            $table->integer('nu_cantidad');
            $table->double('nu_subtotal', 15, 8);
        });
    }

    /**
     * Reverse the migrations.      
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pedido');
    }
}
