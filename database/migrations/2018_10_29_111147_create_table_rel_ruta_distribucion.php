<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRelRutaDistribucion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rel_ruta_distribuccion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_ruta')->unsigned();
            $table->foreign('id_ruta')->references('id')->on('ruta');
            $table->integer('id_distribuidor')->unsigned();
            $table->foreign('id_distribuidor')->references('id')->on('usuario');
            $table->integer('id_colonia')->unsigned();
            $table->foreign('id_colonia')->references('id')->on('colonia');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rel_ruta_distribuccion');
    }
}
