<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEquivalencia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equivalencia', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_unidad_from')->unsigned();
            $table->foreign('id_unidad_from')->references('id')->on('unidad_metrica');
            $table->integer('id_unidad_to')->unsigned();
            $table->foreign('id_unidad_to')->references('id')->on('unidad_metrica');
            $table->double('nu_valor', 15, 8);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equivalencia');
    }
}
