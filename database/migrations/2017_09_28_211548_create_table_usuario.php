<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tx_nombre1');
            $table->string('tx_nombre2')->nullable();
            $table->string('tx_apellido1');
            $table->string('tx_apellido2')->nullable();
            $table->integer('id_tipo_identificacion')->unsigned();
            $table->foreign('id_tipo_identificacion')->references('id')->on('tipo_identificacion');
            $table->string('nu_identificacion')->nullable();
            $table->integer('id_rol')->unsigned();
            $table->foreign('id_rol')->references('id')->on('rol');
            $table->string('tx_sexo');
            $table->date('dt_fecha_nacimiento');
            $table->boolean('bo_activo');
            $table->string('tx_correo')->unique();
            $table->string('tx_telefono_celular')->nullable();
            $table->string('tx_telefono_local')->nullable();
            $table->string('tx_password')->nullable();
            $table->string('tx_codigo');
            $table->string('tx_cargo')->nullable();
            $table->date('dt_fecha_ingreso')->nullable();
            $table->boolean('bo_impreso');
            $table->string('tx_token')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('usuario');
    }
}
