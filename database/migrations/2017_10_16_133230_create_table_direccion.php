<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDireccion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direccion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tx_nombre');
            $table->double('nu_latitud', 15, 8);
            $table->double('nu_longitud', 15, 8);
            $table->integer('id_usuario')->unsigned();
            $table->foreign('id_usuario')->references('id')->on('usuario');
            $table->string('tx_direccion');
            $table->string('tx_referencia');
            $table->string('tx_codigo_postal')->nullable();
            $table->string('tx_numero_ext')->nullable();
            $table->string('tx_numero_int')->nullable();
            $table->string('tx_colonia')->nullable();
            $table->string('tx_municipio')->nullable();
            $table->string('tx_calle')->nullable();
            $table->string('telefono')->nullable();
            $table->integer('id_sucursal')->unsigned()->nullable();
            $table->foreign('id_sucursal')->references('id')->on('sucursal');
            //$table->integer('id_zona')->unsigned();
            //$table->foreign('id_zona')->references('id')->on('zona');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('direccion');
    }
}
