<?php

use App\Auditoria;
use App\Equivalencia;
use App\UnidadMetrica;
use App\Usuario;
use App\TipoIdentificacion;
use App\Rol;
use App\Pais;
use App\Estado;
use App\Ciudad;
use App\Vehiculo;
use App\VehiculoTipo;
use App\Zona;
use App\Direccion;
use App\Articulo;
use App\ArticuloCategoria;
use App\Inventario;
use App\PedidoGrupo;
use App\Pedido;
use App\Ruta;
use App\Colonia;
use App\RelRutaCliente;
use App\RelRutaDistribuccion;
use App\NotificacionMensaje;
use App\Notificacion;
use App\RelNotificacionRol;
use App\Proveedor;
use App\Monitoreo;
use App\MotivoNoEntrega;

use Illuminate\Database\Seeder;

class GeneralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $usuario = new Usuario();

        /* Venezuela */
        Pais::create(['tx_nombre' => 'Venezuela']);

            Estado::create([
                'tx_nombre' => 'Distrito Capital',
                'id_pais' => 1
            ]);
            Estado::create([
                'tx_nombre' => 'Zulia',
                'id_pais' => 1
            ]);
            Estado::create([
                'tx_nombre' => 'Carabobo',
                'id_pais' => 1
            ]);
            Estado::create([
                'tx_nombre' => 'Lara',
                'id_pais' => 1
            ]);
            Estado::create([
                'tx_nombre' => 'Monagas',
                'id_pais' => 1
            ]);

        /* Mexico */
        Pais::create(['tx_nombre' => 'Mexico']);

            Estado::create([
                'tx_nombre' => 'Morelos',
                'id_pais' => 2
            ]);
                Ciudad::create([
                    'tx_nombre' => 'Cuernavaca',
                    'id_estado' => 6
                ]);

                    Zona::create([
                        'tx_nombre' => '01',
                        'id_ciudad' => 1
                    ]);

                    Zona::create([
                        'tx_nombre' => '02',
                        'id_ciudad' => 1
                    ]);

                    Zona::create([
                        'tx_nombre' => '03',
                        'id_ciudad' => 1
                    ]);

            Estado::create([
                'tx_nombre' => 'Chiapas',
                'id_pais' => 2
            ]);
                Ciudad::create([
                    'tx_nombre' => 'Tuxtla Gutiérrez',
                    'id_estado' => 7
                ]);

                    Zona::create([
                        'tx_nombre' => '01',
                        'id_ciudad' => 2
                    ]);

                    Zona::create([
                        'tx_nombre' => '02',
                        'id_ciudad' => 2
                    ]);

                    Zona::create([
                        'tx_nombre' => '03',
                        'id_ciudad' => 2
                    ]);

            Estado::create([
                'tx_nombre' => 'Chihuahua',
                'id_pais' => 2
            ]);
                Ciudad::create([
                    'tx_nombre' => 'Chihuahua',
                    'id_estado' => 8
                ]);

                    Zona::create([
                        'tx_nombre' => '01',
                        'id_ciudad' => 3
                    ]);

                    Zona::create([
                        'tx_nombre' => '02',
                        'id_ciudad' => 3
                    ]);

                    Zona::create([
                        'tx_nombre' => '03',
                        'id_ciudad' => 3
                    ]);

            Estado::create([
                'tx_nombre' => 'Distrito Federal',
                'id_pais' => 2
            ]);
                Ciudad::create([
                    'tx_nombre' => 'Ciudad de México',
                    'id_estado' => 9
                ]);

                    Zona::create([
                        'tx_nombre' => '01',
                        'id_ciudad' => 4
                    ]);

                    Zona::create([
                        'tx_nombre' => '02',
                        'id_ciudad' => 4
                    ]);

                    Zona::create([
                        'tx_nombre' => '03',
                        'id_ciudad' => 4
                    ]);

            Estado::create([
                'tx_nombre' => 'Durango',
                'id_pais' => 2
            ]);
                Ciudad::create([
                    'tx_nombre' => 'Durango',
                    'id_estado' => 10
                ]);

                    Zona::create([
                        'tx_nombre' => '01',
                        'id_ciudad' => 5
                    ]);

                    Zona::create([
                        'tx_nombre' => '02',
                        'id_ciudad' => 5
                    ]);

                    Zona::create([
                        'tx_nombre' => '03',
                        'id_ciudad' => 5
                    ]);

            Estado::create([
                'tx_nombre' => 'Aguascalientes',
                'id_pais' => 2
            ]);
                Ciudad::create([
                    'tx_nombre' => 'Aguascalientes',
                    'id_estado' => 11
                ]);

                    Zona::create([
                        'tx_nombre' => '01',
                        'id_ciudad' => 6
                    ]);

                    Zona::create([
                        'tx_nombre' => '02',
                        'id_ciudad' => 6
                    ]);

                    Zona::create([
                        'tx_nombre' => '03',
                        'id_ciudad' => 6
                    ]);

            Estado::create([
                'tx_nombre' => 'Guanajuato',
                'id_pais' => 2
            ]);
                Ciudad::create([
                    'tx_nombre' => 'Guanajuato',
                    'id_estado' => 12
                ]);

                    Zona::create([
                        'tx_nombre' => '01',
                        'id_ciudad' => 7
                    ]);

                    Zona::create([
                        'tx_nombre' => '02',
                        'id_ciudad' => 7
                    ]);

                    Zona::create([
                        'tx_nombre' => '03',
                        'id_ciudad' => 7
                    ]);


            Estado::create([
                'tx_nombre' => 'Veracruz',
                'id_pais' => 2
            ]);
                Ciudad::create([
                    'tx_nombre' => 'Xalapa',
                    'id_estado' => 13
                ]);

                    Zona::create([
                        'tx_nombre' => '01',
                        'id_ciudad' => 8
                    ]);

                    Zona::create([
                        'tx_nombre' => '02',
                        'id_ciudad' => 8
                    ]);

                    Zona::create([
                        'tx_nombre' => '03',
                        'id_ciudad' => 8
                    ]);

            Estado::create([
                'tx_nombre' => 'Yucatan',
                'id_pais' => 2
            ]);
                Ciudad::create([
                    'tx_nombre' => 'Mérida',
                    'id_estado' => 14
                ]);

                    Zona::create([
                        'tx_nombre' => '01',
                        'id_ciudad' => 9
                    ]);

                    Zona::create([
                        'tx_nombre' => '02',
                        'id_ciudad' => 9
                    ]);

                    Zona::create([
                        'tx_nombre' => '03',
                        'id_ciudad' => 9
                    ]);

    	TipoIdentificacion::create(['tx_nombre' => 'Cedula']);
    	TipoIdentificacion::create(['tx_nombre' => 'Pasaporte']);
    	TipoIdentificacion::create(['tx_nombre' => 'DNI']);
        TipoIdentificacion::create(['tx_nombre' => 'RFC']);
        TipoIdentificacion::create(['tx_nombre' => 'CURP']);
        TipoIdentificacion::create(['tx_nombre' => 'IFE/INE']);

    	Rol::create(['tx_nombre' => 'Cliente Fisico']);
    	Rol::create(['tx_nombre' => 'Distribuidor']);
    	Rol::create(['tx_nombre' => 'Super Usuario']);
    	Rol::create(['tx_nombre' => 'Administrador']);
        Rol::create(['tx_nombre' => 'Cliente Moral']);
        Rol::create(['tx_nombre' => 'Atención Cliente']);
        Rol::create(['tx_nombre' => 'Supervisor']);
        Rol::create(['tx_nombre' => 'Planta']);
        Rol::create(['tx_nombre' => 'RRHH']);
        Rol::create(['tx_nombre' => 'Operador']);
        Rol::create(['tx_nombre' => 'Staff']);

        Usuario::create([
            'tx_nombre1' => 'Sistema',
            'tx_nombre2' => '',
            'tx_apellido1' => 'Gestion',
            'tx_apellido2' => '',
            'dt_fecha_nacimiento' => '2017-10-01',
            'id_tipo_identificacion' => 1,
            'nu_identificacion' => 87654321,
            'tx_sexo' => 'Masculino',
            'bo_activo' => TRUE,
            'id_rol' => 3,
            'tx_correo' => 'masgas@masgas.com',
            'tx_telefono_celular' => '04141234567',
            'tx_telefono_local' => '02121234567',
            'tx_password' => '12345678',
            'tx_codigo' => $usuario->randomString(8),
            'tx_cargo' => '',
            'dt_fecha_ingreso' => null,
            'bo_impreso' => FALSE
        ]);

        Auditoria::create([
            'nu_modulo' => 1,
            'nu_accion' => 1,
            'nu_identificador' => 1,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        Usuario::create([
        	'tx_nombre1' => 'Otoniel',
        	'tx_nombre2' => 'Enrique',
        	'tx_apellido1' => 'Marquez',
        	'tx_apellido2' => 'Mejias',
        	'dt_fecha_nacimiento' => '1992-10-05',
        	'id_tipo_identificacion' => 1,
        	'nu_identificacion' => 21091361,
        	'tx_sexo' => 'Masculino',
        	'bo_activo' => TRUE,
        	'id_rol' => 1,
        	'tx_correo' => 'otonielmax@hotmail.com',
        	'tx_telefono_celular' => '04264170396',
        	'tx_telefono_local' => '02128343743',
            'tx_password' => '12345678',
            'tx_codigo' => $usuario->randomString(8),
            'tx_cargo' => '',
            'dt_fecha_ingreso' => null,
            'bo_impreso' => FALSE
        ]);

        Auditoria::create([
            'nu_modulo' => 1,
            'nu_accion' => 1,
            'nu_identificador' => 2,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        Usuario::create([
            'tx_nombre1' => 'Distribuidor',
            'tx_nombre2' => '',
            'tx_apellido1' => 'Prueba',
            'tx_apellido2' => '',
            'dt_fecha_nacimiento' => '2017-10-10',
            'id_tipo_identificacion' => 1,
            'nu_identificacion' => 123456789,
            'tx_sexo' => 'Masculino',
            'bo_activo' => TRUE,
            'id_rol' => 2,
            'tx_correo' => 'distribuidor@hotmail.com',
            'tx_telefono_celular' => '04241234567',
            'tx_telefono_local' => '02121234567',
            'tx_password' => '12345678',
            'tx_codigo' => $usuario->randomString(8),
            'tx_cargo' => '',
            'dt_fecha_ingreso' => null,
            'bo_impreso' => FALSE
        ]);

        Auditoria::create([
            'nu_modulo' => 2,
            'nu_accion' => 1,
            'nu_identificador' => 1,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        Usuario::create([
            'tx_nombre1' => 'Distribuidor',
            'tx_nombre2' => '',
            'tx_apellido1' => 'Moto Pipa',
            'tx_apellido2' => '',
            'dt_fecha_nacimiento' => '2017-10-10',
            'id_tipo_identificacion' => 1,
            'nu_identificacion' => 987654321,
            'tx_sexo' => 'Masculino',
            'bo_activo' => TRUE,
            'id_rol' => 2,
            'tx_correo' => 'distribuidorpipa@hotmail.com',
            'tx_telefono_celular' => '04241234567',
            'tx_telefono_local' => '02121234567',
            'tx_password' => '12345678',
            'tx_codigo' => $usuario->randomString(8),
            'tx_cargo' => '',
            'dt_fecha_ingreso' => null,
            'bo_impreso' => FALSE
        ]);

        Auditoria::create([
            'nu_modulo' => 2,
            'nu_accion' => 2,
            'nu_identificador' => 1,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        // Distribuidor
        Usuario::create([            
            'tx_password' => '12345678',
            'tx_nombre1' => 'Distribuidora',
            'tx_nombre2' => '',
            'tx_apellido1' => 'Pruebas',   
            'tx_apellido2' => '',
            'tx_telefono_local' => '02121234567',
            'tx_telefono_celular' => '04261234567',
            'tx_correo' => 'd@d.com',
            'dt_fecha_nacimiento' => date("Y-m-d"),
            'tx_sexo' => '',
            'id_tipo_identificacion' => 1,
            'nu_identificacion' => 'J1234567',
            'id_rol' => 2,
            'bo_activo' => true,
            'bo_impreso' => false,
            'tx_codigo' => $usuario->randomString(8),
        ]);

        Auditoria::create([
            'nu_modulo' => 1,
            'nu_accion' => 1,
            'nu_identificador' => 5,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        /* Rutas */
        Ruta::create([
            'alias' => '001'            
        ]);

        Colonia::create([
            'nombre' => 'Coche'            
        ]);

        Colonia::create([
            'nombre' => 'El Valle'            
        ]);

        RelRutaDistribuccion::create([
            'id_ruta' => 1,
            'id_distribuidor' => 1,
            'id_colonia' => 1
        ]);

        RelRutaDistribuccion::create([
            'id_ruta' => 1,
            'id_distribuidor' => 1,
            'id_colonia' => 2
        ]);

        Auditoria::create([
            'nu_modulo' => 6,
            'nu_accion' => 1,
            'nu_identificador' => 1,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        /* Direccion */
        Direccion::create([
            'tx_nombre' => 'Casa',
            'nu_latitud' => 16.7686469,
            'nu_longitud' => -93.0360495,
            'id_usuario' => 2,
            'tx_direccion' => 'Cochecitos, Caracas, Distrito Capital',
            'tx_referencia' => 'A media cuadra',
            'tx_codigo_postal' => '1090',
            'tx_colonia' => 'Cochecito',
            'tx_calle' => 'Madre Maria',
            'tx_municipio' => 'Libertador'
        ]);

        Auditoria::create([
            'nu_modulo' => 5,
            'nu_accion' => 1,
            'nu_identificador' => 1,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 2
        ]);

        /* RelClienteRuta */
        RelRutaCliente::create([
            'id_direccion' => 1,
            'id_ruta' => 1
        ]);

        /* Proveedores */
        Proveedor::create([
            'tx_nombre' => 'PDVSA',
            'tx_numero_documento' => 'J12345647',
            'tx_telefono' => '04121234567',
            'tx_correo' => 'pruebas',
            'tx_direccion' => 'pruebas',
        ]);

        Proveedor::create([
            'tx_nombre' => 'DIESEL',
            'tx_numero_documento' => 'J43423132',
            'tx_telefono' => '04121234567',
            'tx_correo' => 'pruebas',
            'tx_direccion' => 'pruebas',
        ]);

        /* Articulos e Inventario */
        ArticuloCategoria::create([
            'tx_nombre' => 'Cilindros'
        ]);

        ArticuloCategoria::create([
            'tx_nombre' => 'Gas estacionario'
        ]);

        ArticuloCategoria::create([
            'tx_nombre' => 'Carburación'
        ]);

        Articulo::create([
            'tx_nombre' => 'Cilindro 4 Kg',
            'nu_precio' => 50.0,
            'id_articulo_categoria' => 1,
            'nu_inventario' => 10
        ]);

        Auditoria::create([
            'nu_modulo' => 3,
            'nu_accion' => 1,
            'nu_identificador' => 1,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        Inventario::create([
            'id_articulo' => 1,
            'id_usuario' => 1,
            'nu_cantidad' => 10,
            'dt_fecha' => date('Y-m-d'),
            'nu_solicitado' => 12,
            'nu_merma' => 2,
            'nu_monto' => 500,
            'nu_flete' => 25,
            'id_proveedor' => 2,
            'tx_numero_orden' => 'ORD-006'
        ]);

        Articulo::create([
            'tx_nombre' => 'Cilindro 10 Kg',
            'nu_precio' => 80.0,
            'id_articulo_categoria' => 1,
            'nu_inventario' => 15
        ]);

        Auditoria::create([
            'nu_modulo' => 3,
            'nu_accion' => 1,
            'nu_identificador' => 2,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        Inventario::create([
            'id_articulo' => 2,
            'id_usuario' => 1,
            'nu_cantidad' => 15,
            'dt_fecha' => date('Y-m-d'),
            'nu_solicitado' => 20,
            'nu_merma' => 5,
            'nu_monto' => 1250,
            'nu_flete' => 50,
            'id_proveedor' => 1,
            'tx_numero_orden' => 'ORD-007'
        ]);

        Articulo::create([
            'tx_nombre' => 'Cilindro 20 Kg',
            'nu_precio' => 344.0,
            'id_articulo_categoria' => 1,
            'nu_inventario' => 8
        ]);

        Auditoria::create([
            'nu_modulo' => 3,
            'nu_accion' => 1,
            'nu_identificador' => 3,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        Inventario::create([
            'id_articulo' => 3,
            'id_usuario' => 1,
            'nu_cantidad' => 8,
            'dt_fecha' => date('Y-m-d'),
            'nu_solicitado' => 11,
            'nu_merma' => 3,
            'nu_monto' => 350,
            'nu_flete' => 30,
            'id_proveedor' => 1,
            'tx_numero_orden' => 'ORD-001'
        ]);

        Articulo::create([
            'tx_nombre' => 'Cilindro 30 Kg',
            'nu_precio' => 516.0,
            'id_articulo_categoria' => 1,
            'nu_inventario' => 10
        ]);

        Auditoria::create([
            'nu_modulo' => 3,
            'nu_accion' => 1,
            'nu_identificador' => 4,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        Inventario::create([
            'id_articulo' => 4,
            'id_usuario' => 1,
            'nu_cantidad' => 10,
            'dt_fecha' => date('Y-m-d'),
            'nu_solicitado' => 12,
            'nu_merma' => 2,
            'nu_monto' => 500,
            'nu_flete' => 25,
            'id_proveedor' => 1,
            'tx_numero_orden' => 'ORD-002'
        ]);

        Articulo::create([
            'tx_nombre' => 'Cilindro 45 Kg',
            'nu_precio' => 774.0,
            'id_articulo_categoria' => 1,
            'nu_inventario' => 15
        ]);

        Auditoria::create([
            'nu_modulo' => 3,
            'nu_accion' => 1,
            'nu_identificador' => 5,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        Inventario::create([
            'id_articulo' => 5,
            'id_usuario' => 1,
            'nu_cantidad' => 15,
            'dt_fecha' => date('Y-m-d'),
            'nu_solicitado' => 18,
            'nu_merma' => 8,
            'nu_monto' => 950,
            'nu_flete' => 25,
            'id_proveedor' => 2,
            'tx_numero_orden' => 'ORD-003'
        ]);

        Articulo::create([
            'tx_nombre' => 'Pipa',
            'nu_precio' => 50.0,
            'id_articulo_categoria' => 2,
            'nu_inventario' => 10000
        ]);

        Auditoria::create([
            'nu_modulo' => 3,
            'nu_accion' => 1,
            'nu_identificador' => 6,
            'dt_fecha_accion' => date('Y-m-d'),
            'id_usuario_accion' => 1
        ]);

        Inventario::create([
            'id_articulo' => 6,
            'id_usuario' => 1,
            'nu_cantidad' => 10000,
            'dt_fecha' => date('Y-m-d'),
            'nu_solicitado' => 10500,
            'nu_merma' => 500,
            'nu_monto' => 500,
            'nu_flete' => 100,
            'id_proveedor' => 1,
            'tx_numero_orden' => 'ORD-004'
        ]);

        /* Pedidos */
        PedidoGrupo::create([
            'id_direccion' => 1,
            'dt_fecha' => date('Y-m-d'),
            'tx_codigo' => $usuario->randomString(8),
            'bo_pagada' => FALSE,
            'tx_estatus' => 'Por entregar',
            'id_distribuidor' => 3,
            'dt_fecha_asignado' => date('Y-m-d'),
            'tx_vendido_en' => 'App'
        ]);

        Pedido::create([
            'id_pedido_grupo' => 1,
            'nu_cantidad' => 3,
            'id_articulo' => 1,
            'nu_subtotal' => 150.0
        ]);

        Pedido::create([
            'id_pedido_grupo' => 1,
            'nu_cantidad' => 1,
            'id_articulo' => 2,
            'nu_subtotal' => 160.0
        ]);

        Pedido::create([
            'id_pedido_grupo' => 1,
            'nu_cantidad' => 2,
            'id_articulo' => 3,
            'nu_subtotal' => 240.0
        ]);

        // Notificaciones
        NotificacionMensaje::create([
            'tx_mensaje' => 'El distribuidor no ha entregado el pedido'
        ]);

        NotificacionMensaje::create([
            'tx_mensaje' => 'El pedido recibido no fue el solicitado'
        ]);

        NotificacionMensaje::create([
            'tx_mensaje' => 'El producto vino con una fuga'
        ]);

        NotificacionMensaje::create([
            'tx_mensaje' => 'Hay un retraso por accidente en la vía'
        ]);

        NotificacionMensaje::create([
            'tx_mensaje' => 'Retraso de su pedido por problemas con el vehículo'
        ]);

        NotificacionMensaje::create([
            'tx_mensaje' => 'Otros'
        ]);

        RelNotificacionRol::create([
            'id_rol' => 1,
            'id_notificacion_mensaje' => 1
        ]);

        RelNotificacionRol::create([
            'id_rol' => 5,
            'id_notificacion_mensaje' => 1
        ]);

        RelNotificacionRol::create([
            'id_rol' => 1,
            'id_notificacion_mensaje' => 2
        ]);

        RelNotificacionRol::create([
            'id_rol' => 5,
            'id_notificacion_mensaje' => 2
        ]);

        RelNotificacionRol::create([
            'id_rol' => 1,
            'id_notificacion_mensaje' => 3
        ]);

        RelNotificacionRol::create([
            'id_rol' => 5,
            'id_notificacion_mensaje' => 3
        ]);

        RelNotificacionRol::create([
            'id_rol' => 2,
            'id_notificacion_mensaje' => 4
        ]);

        RelNotificacionRol::create([
            'id_rol' => 2,
            'id_notificacion_mensaje' => 5
        ]);

        RelNotificacionRol::create([
            'id_rol' => 2,
            'id_notificacion_mensaje' => 5
        ]);

        RelNotificacionRol::create([
            'id_rol' => 1,
            'id_notificacion_mensaje' => 6
        ]);

        RelNotificacionRol::create([
            'id_rol' => 2,
            'id_notificacion_mensaje' => 6
        ]);

        RelNotificacionRol::create([
            'id_rol' => 5,
            'id_notificacion_mensaje' => 6
        ]);

        Notificacion::create([
            'id_usuario_emisor' => 2,
            'id_usuario_receptor' => 3,
            'id_mensaje' => 2,
            'dt_fecha' => date('Y-m-d'),
            'tx_cuerpo' => 'Pruebas'
        ]);

        Notificacion::create([
            'id_usuario_emisor' => 2,
            'id_usuario_receptor' => 3,
            'id_mensaje' => 1,
            'dt_fecha' => date('Y-m-d'),
            'tx_cuerpo' => 'Pruebas'
        ]);

        Notificacion::create([
            'id_usuario_emisor' => 3,
            'id_usuario_receptor' => 2,
            'id_mensaje' => 4,
            'dt_fecha' => date('Y-m-d'),
            'tx_cuerpo' => 'Pruebas'
        ]);

        Notificacion::create([
            'id_usuario_emisor' => 3,
            'id_usuario_receptor' => 2,
            'id_mensaje' => 5,
            'dt_fecha' => date('Y-m-d'),
            'tx_cuerpo' => 'Pruebas'
        ]);

        VehiculoTipo::create([
           'tx_nombre' => 'Pipa'
        ]);

        VehiculoTipo::create([
            'tx_nombre' => 'Motocarro'
        ]);

        Vehiculo::create([
           'id_tipo_vehiculo' => 1,
           'tx_numero_identificacion' => 'XYZ123',
           'id_usuario' => 3, 
           'tx_marca' => 'Chevrolet',
           'tx_modelo' => '350'
        ]);

        Vehiculo::create([
           'id_tipo_vehiculo' => 2,
           'tx_numero_identificacion' => '123XYZ',
           'id_usuario' => 4,
           'tx_marca' => 'Yamaha',
           'tx_modelo' => '115 CC'
        ]);

        UnidadMetrica::create([
            'tx_nombre' => 'Volumen',
            'tx_simbolo' => 'Lts'
        ]);

        UnidadMetrica::create([
            'tx_nombre' => 'Masa',
            'tx_simbolo' => 'Kg'
        ]);

        Equivalencia::create([
            'id_unidad_from' => 1,
            'id_unidad_to' => 2,
            'nu_valor' => 1.0
        ]);

        Equivalencia::create([
            'id_unidad_from' => 2,
            'id_unidad_to' => 1,
            'nu_valor' => 1.0
        ]);

        Monitoreo::create([
            'id_usuario' => 3,
            'id_vehiculo' => 1,
            'nu_latitud' => 10.560977, 
            'nu_longitud' => -71.932072,
            'created_at' => '2018-07-30 10:00:00',
            'updated_at' => '2018-07-30 10:00:00'
        ]);

        Monitoreo::create([
            'id_usuario' => 3,
            'id_vehiculo' => 1,
            'nu_latitud' => 10.527204, 
            'nu_longitud' => -71.861331,
            'created_at' => '2018-07-30 10:15:00',
            'updated_at' => '2018-07-30 10:15:00'
        ]);

        Monitoreo::create([
            'id_usuario' => 3,
            'id_vehiculo' => 1,
            'nu_latitud' => 10.534580, 
            'nu_longitud' => -71.706082,
            'created_at' => '2018-07-30 10:30:00',
            'updated_at' => '2018-07-30 10:30:00'
        ]);

        Monitoreo::create([
            'id_usuario' => 4,
            'id_vehiculo' => 2,
            'nu_latitud' => 10.475818, 
            'nu_longitud' => -71.048522,
            'created_at' => '2018-07-30 09:50:00',
            'updated_at' => '2018-07-30 09:50:00'
        ]);

        Monitoreo::create([
            'id_usuario' => 4,
            'id_vehiculo' => 2,
            'nu_latitud' => 10.641628, 
            'nu_longitud' => -71.130385,
            'created_at' => '2018-07-30 10:05:00',
            'updated_at' => '2018-07-30 10:05:00'
        ]);

        Monitoreo::create([
            'id_usuario' => 4,
            'id_vehiculo' => 2,
            'nu_latitud' => 10.701689,  
            'nu_longitud' => -71.061729,
            'created_at' => '2018-07-30 10:20:00',
            'updated_at' => '2018-07-30 10:20:00'
        ]);

        MotivoNoEntrega::create([
            'nombre' => "No se encontro al cliente",
        ]);

        MotivoNoEntrega::create([
            'nombre' => "No se encontro el domicilio",
        ]);

        MotivoNoEntrega::create([
            'nombre' => "No se pudo entrar al domicilio",
        ]);

        MotivoNoEntrega::create([
            'nombre' => "Ya compro a otro proveedor",
        ]);

        MotivoNoEntrega::create([
            'nombre' => "El precio no fue el pautado",
        ]);
    }
}
